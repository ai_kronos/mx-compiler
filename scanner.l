%{ /* -*- C++ -*- */
# include <cerrno>
# include <climits>
# include <cstdlib>
# include <cstring> // strerror
# include <string>
# include "parser.hpp"
# include "wrap.h"
%}
%option noyywrap
%x COMMENT
id    [a-zA-Z][a-zA-Z_0-9]*
int   [0-9]+
str   \"(\\.|[^"\\])*\"
blank [ \t\r]
%%
"//".* {}
"/*" {BEGIN COMMENT;}
<COMMENT>"*/" {BEGIN INITIAL;}
<COMMENT>[\r\n] {}
<COMMENT>.      {}
{blank}+        {}
[\r\n]          {}
"||"            {return yy::parser::make_lor(yytext);}
"&&"            {return yy::parser::make_land(yytext);}
"|"             {return yy::parser::make_bor(yytext);}
"^"             {return yy::parser::make_bxor(yytext);}
"&"             {return yy::parser::make_band(yytext);}
"=="            {return yy::parser::make_equ(yytext);}
"!="            {return yy::parser::make_neq(yytext);}
"<"             {return yy::parser::make_lst(yytext);}
">"             {return yy::parser::make_bgt(yytext);}
"<="            {return yy::parser::make_let(yytext);}
">="            {return yy::parser::make_bet(yytext);}
"<<"            {return yy::parser::make_shl(yytext);}
">>"            {return yy::parser::make_shr(yytext);}
"+"             {return yy::parser::make_add(yytext);}
"-"             {return yy::parser::make_sub(yytext);}
"*"             {return yy::parser::make_mul(yytext);}
"/"             {return yy::parser::make_div(yytext);}
"%"             {return yy::parser::make_mod(yytext);}
"!"             {return yy::parser::make_den(yytext);}
"~"             {return yy::parser::make_neg(yytext);}
"++"            {return yy::parser::make_inc(yytext);}
"--"            {return yy::parser::make_dec(yytext);}
"("             {return yy::parser::make_lrod(yytext);}
")"             {return yy::parser::make_rrod(yytext);}
"["             {return yy::parser::make_lsqu(yytext);}
"]"             {return yy::parser::make_rsqu(yytext);}
"="             {return yy::parser::make_asig(yytext);}
","             {return yy::parser::make_sper(yytext);}
"."             {return yy::parser::make_memb(yytext);}
"{"             {return yy::parser::make_lbra(yytext);}
"}"             {return yy::parser::make_rbra(yytext);}
";"             {return yy::parser::make_semi(yytext);}

"return"        {return yy::parser::make_ret(yytext);}
"break"         {return yy::parser::make_brk(yytext);}
"continue"      {return yy::parser::make_ctn(yytext);}
"while"         {return yy::parser::make__while(yytext);}
"for"           {return yy::parser::make__for(yytext);}
"if"            {return yy::parser::make__if(yytext);}
"else"          {return yy::parser::make__else(yytext);}
"null"          {return yy::parser::make__null(yytext);}
"true"          {return yy::parser::make__true(yytext);}
"false"         {return yy::parser::make__false(yytext);}
"void"          {return yy::parser::make__void(yytext);}
"string"        {return yy::parser::make__string(yytext);}
"int"           {return yy::parser::make__int(yytext);}
"bool"           {return yy::parser::make__bool(yytext);}
"class"         {return yy::parser::make__class(yytext);}
"new"           {return yy::parser::make__new(yytext);}
(\[{blank}*\]{blank}*)+         {return yy::parser::make_creator(yytext);}

{int}           {return yy::parser::make_inte(yytext);}
{id}            {return yy::parser::make_id(yytext);}
{str}           {return yy::parser::make_str(yytext);}
<<EOF>>         {return yy::parser::make_END_OF_FILE();}
%%