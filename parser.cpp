// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.

#line 37 "parser.cpp" // lalr1.cc:404

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "parser.hpp"

// User implementation prologue.

#line 51 "parser.cpp" // lalr1.cc:412


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif



// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)


namespace yy {
#line 118 "parser.cpp" // lalr1.cc:479

  /// Build a parser object.
  parser::parser ()
#if YYDEBUG
     :yydebug_ (false),
      yycdebug_ (&std::cerr)
#endif
  {}

  parser::~parser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/



  // by_state.
  inline
  parser::by_state::by_state ()
    : state (empty_state)
  {}

  inline
  parser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
  parser::by_state::clear ()
  {
    state = empty_state;
  }

  inline
  void
  parser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  inline
  parser::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
  parser::symbol_number_type
  parser::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  inline
  parser::stack_symbol_type::stack_symbol_type ()
  {}


  inline
  parser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s)
  {
      switch (that.type_get ())
    {
      case 61: // PROGRAM
      case 62: // FUNCDEF
      case 63: // PARADEF
      case 64: // PARADEFLIST
      case 65: // STALIST
      case 66: // STATEMENT
      case 67: // RETURN
      case 68: // WHILE
      case 69: // FOR
      case 70: // IF
      case 71: // FOR_EXPR
      case 72: // VARDEF
      case 73: // CLASSDEF
      case 74: // CLASSPARADEF
      case 76: // CREATOR
      case 77: // EXPR
      case 78: // _NEW
      case 79: // _SUBSCRIPT
      case 80: // PARALIST
        value.move< ast_node* > (that.value);
        break;

      case 6: // sper
      case 7: // asig
      case 8: // lor
      case 9: // land
      case 10: // bor
      case 11: // bxor
      case 12: // band
      case 13: // equ
      case 14: // neq
      case 15: // lst
      case 16: // bgt
      case 17: // let
      case 18: // bet
      case 19: // shl
      case 20: // shr
      case 21: // add
      case 22: // sub
      case 23: // mul
      case 24: // div
      case 25: // mod
      case 26: // den
      case 27: // neg
      case 28: // _new
      case 29: // inc
      case 30: // dec
      case 31: // memb
      case 32: // lrod
      case 33: // lsqu
      case 34: // rrod
      case 35: // rsqu
      case 36: // id
      case 37: // inte
      case 38: // str
      case 39: // semi
      case 40: // lbra
      case 41: // rbra
      case 42: // creator
      case 43: // ctn
      case 44: // brk
      case 45: // ret
      case 46: // whi
      case 47: // _while
      case 48: // _for
      case 49: // _if
      case 50: // _null
      case 51: // _true
      case 52: // _false
      case 53: // _void
      case 54: // _int
      case 55: // _string
      case 56: // _class
      case 57: // _bool
      case 58: // _then
      case 59: // _else
      case 75: // TYPEBASE
        value.move< std::string > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty_symbol;
  }

  inline
  parser::stack_symbol_type&
  parser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 61: // PROGRAM
      case 62: // FUNCDEF
      case 63: // PARADEF
      case 64: // PARADEFLIST
      case 65: // STALIST
      case 66: // STATEMENT
      case 67: // RETURN
      case 68: // WHILE
      case 69: // FOR
      case 70: // IF
      case 71: // FOR_EXPR
      case 72: // VARDEF
      case 73: // CLASSDEF
      case 74: // CLASSPARADEF
      case 76: // CREATOR
      case 77: // EXPR
      case 78: // _NEW
      case 79: // _SUBSCRIPT
      case 80: // PARALIST
        value.copy< ast_node* > (that.value);
        break;

      case 6: // sper
      case 7: // asig
      case 8: // lor
      case 9: // land
      case 10: // bor
      case 11: // bxor
      case 12: // band
      case 13: // equ
      case 14: // neq
      case 15: // lst
      case 16: // bgt
      case 17: // let
      case 18: // bet
      case 19: // shl
      case 20: // shr
      case 21: // add
      case 22: // sub
      case 23: // mul
      case 24: // div
      case 25: // mod
      case 26: // den
      case 27: // neg
      case 28: // _new
      case 29: // inc
      case 30: // dec
      case 31: // memb
      case 32: // lrod
      case 33: // lsqu
      case 34: // rrod
      case 35: // rsqu
      case 36: // id
      case 37: // inte
      case 38: // str
      case 39: // semi
      case 40: // lbra
      case 41: // rbra
      case 42: // creator
      case 43: // ctn
      case 44: // brk
      case 45: // ret
      case 46: // whi
      case 47: // _while
      case 48: // _for
      case 49: // _if
      case 50: // _null
      case 51: // _true
      case 52: // _false
      case 53: // _void
      case 54: // _int
      case 55: // _string
      case 56: // _class
      case 57: // _bool
      case 58: // _then
      case 59: // _else
      case 75: // TYPEBASE
        value.copy< std::string > (that.value);
        break;

      default:
        break;
    }

    return *this;
  }


  template <typename Base>
  inline
  void
  parser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
  parser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " (";
    YYUSE (yytype);
    yyo << ')';
  }
#endif

  inline
  void
  parser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
  parser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
  parser::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  parser::debug_level_type
  parser::debug_level () const
  {
    return yydebug_;
  }

  void
  parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  inline parser::state_type
  parser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  inline bool
  parser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
  parser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  parser::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            symbol_type yylookahead (yylex ());
            yyla.move (yylookahead);
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
        switch (yyr1_[yyn])
    {
      case 61: // PROGRAM
      case 62: // FUNCDEF
      case 63: // PARADEF
      case 64: // PARADEFLIST
      case 65: // STALIST
      case 66: // STATEMENT
      case 67: // RETURN
      case 68: // WHILE
      case 69: // FOR
      case 70: // IF
      case 71: // FOR_EXPR
      case 72: // VARDEF
      case 73: // CLASSDEF
      case 74: // CLASSPARADEF
      case 76: // CREATOR
      case 77: // EXPR
      case 78: // _NEW
      case 79: // _SUBSCRIPT
      case 80: // PARALIST
        yylhs.value.build< ast_node* > ();
        break;

      case 6: // sper
      case 7: // asig
      case 8: // lor
      case 9: // land
      case 10: // bor
      case 11: // bxor
      case 12: // band
      case 13: // equ
      case 14: // neq
      case 15: // lst
      case 16: // bgt
      case 17: // let
      case 18: // bet
      case 19: // shl
      case 20: // shr
      case 21: // add
      case 22: // sub
      case 23: // mul
      case 24: // div
      case 25: // mod
      case 26: // den
      case 27: // neg
      case 28: // _new
      case 29: // inc
      case 30: // dec
      case 31: // memb
      case 32: // lrod
      case 33: // lsqu
      case 34: // rrod
      case 35: // rsqu
      case 36: // id
      case 37: // inte
      case 38: // str
      case 39: // semi
      case 40: // lbra
      case 41: // rbra
      case 42: // creator
      case 43: // ctn
      case 44: // brk
      case 45: // ret
      case 46: // whi
      case 47: // _while
      case 48: // _for
      case 49: // _if
      case 50: // _null
      case 51: // _true
      case 52: // _false
      case 53: // _void
      case 54: // _int
      case 55: // _string
      case 56: // _class
      case 57: // _bool
      case 58: // _then
      case 59: // _else
      case 75: // TYPEBASE
        yylhs.value.build< std::string > ();
        break;

      default:
        break;
    }



      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
      try
        {
          switch (yyn)
            {
  case 2:
#line 62 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast_root=ast.create<node_PROGRAM>(); yylhs.value.as< ast_node* > ()->appendChild({yystack_[0].value.as< ast_node* > ()});}
#line 683 "parser.cpp" // lalr1.cc:859
    break;

  case 3:
#line 63 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast_root=ast.create<node_PROGRAM>(); yylhs.value.as< ast_node* > ()->appendChild({yystack_[1].value.as< ast_node* > ()});}
#line 689 "parser.cpp" // lalr1.cc:859
    break;

  case 4:
#line 64 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast_root=ast.create<node_PROGRAM>(); yylhs.value.as< ast_node* > ()->appendChild({yystack_[0].value.as< ast_node* > ()});}
#line 695 "parser.cpp" // lalr1.cc:859
    break;

  case 5:
#line 65 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast_root=yystack_[1].value.as< ast_node* > (); yylhs.value.as< ast_node* > ()->appendChild({yystack_[0].value.as< ast_node* > ()});}
#line 701 "parser.cpp" // lalr1.cc:859
    break;

  case 6:
#line 66 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast_root=yystack_[2].value.as< ast_node* > (); yylhs.value.as< ast_node* > ()->appendChild({yystack_[1].value.as< ast_node* > ()});}
#line 707 "parser.cpp" // lalr1.cc:859
    break;

  case 7:
#line 67 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast_root=yystack_[1].value.as< ast_node* > (); yylhs.value.as< ast_node* > ()->appendChild({yystack_[0].value.as< ast_node* > ()});}
#line 713 "parser.cpp" // lalr1.cc:859
    break;

  case 8:
#line 69 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[3].value.as< std::string > (), "", yystack_[2].value.as< std::string > (), yystack_[1].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ());}
#line 719 "parser.cpp" // lalr1.cc:859
    break;

  case 9:
#line 70 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[3].value.as< std::string > (), "", yystack_[2].value.as< std::string > (), yystack_[1].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ());}
#line 725 "parser.cpp" // lalr1.cc:859
    break;

  case 10:
#line 71 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[4].value.as< std::string > (), yystack_[3].value.as< std::string > (), yystack_[2].value.as< std::string > (), yystack_[1].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ());}
#line 731 "parser.cpp" // lalr1.cc:859
    break;

  case 11:
#line 72 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[4].value.as< std::string > (), yystack_[3].value.as< std::string > (), yystack_[2].value.as< std::string > (), yystack_[1].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ());}
#line 737 "parser.cpp" // lalr1.cc:859
    break;

  case 12:
#line 73 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)yylhs.value.as< ast_node* > ())->init("", "", yystack_[2].value.as< std::string > (), yystack_[1].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ());}
#line 743 "parser.cpp" // lalr1.cc:859
    break;

  case 13:
#line 75 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_PARADEF>(); yylhs.value.as< ast_node* > ()->merge(yystack_[1].value.as< ast_node* > ());}
#line 749 "parser.cpp" // lalr1.cc:859
    break;

  case 14:
#line 77 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_PARADEFLIST>();}
#line 755 "parser.cpp" // lalr1.cc:859
    break;

  case 15:
#line 78 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_PARADEFLIST>(); ((node_PARADEFLIST*)yylhs.value.as< ast_node* > ())->init(yystack_[0].value.as< ast_node* > ());}
#line 761 "parser.cpp" // lalr1.cc:859
    break;

  case 16:
#line 79 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_PARADEFLIST>(); ((node_PARADEFLIST*)yylhs.value.as< ast_node* > ())->init(yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ());}
#line 767 "parser.cpp" // lalr1.cc:859
    break;

  case 17:
#line 81 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_STATEMENT>(); ((node_STATEMENT*)yylhs.value.as< ast_node* > ())->init(false, yystack_[0].value.as< ast_node* > ());}
#line 773 "parser.cpp" // lalr1.cc:859
    break;

  case 18:
#line 82 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[1].value.as< ast_node* > (); yylhs.value.as< ast_node* > ()->appendChild({yystack_[0].value.as< ast_node* > ()});}
#line 779 "parser.cpp" // lalr1.cc:859
    break;

  case 19:
#line 84 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[1].value.as< ast_node* > ();}
#line 785 "parser.cpp" // lalr1.cc:859
    break;

  case 20:
#line 85 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[1].value.as< ast_node* > ();}
#line 791 "parser.cpp" // lalr1.cc:859
    break;

  case 21:
#line 86 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[0].value.as< ast_node* > ();}
#line 797 "parser.cpp" // lalr1.cc:859
    break;

  case 22:
#line 87 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[0].value.as< ast_node* > ();}
#line 803 "parser.cpp" // lalr1.cc:859
    break;

  case 23:
#line 88 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[0].value.as< ast_node* > ();}
#line 809 "parser.cpp" // lalr1.cc:859
    break;

  case 24:
#line 89 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[1].value.as< ast_node* > ();}
#line 815 "parser.cpp" // lalr1.cc:859
    break;

  case 25:
#line 90 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_CONTINUE>();}
#line 821 "parser.cpp" // lalr1.cc:859
    break;

  case 26:
#line 91 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_BREAK>();}
#line 827 "parser.cpp" // lalr1.cc:859
    break;

  case 27:
#line 92 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_STATEMENT>();}
#line 833 "parser.cpp" // lalr1.cc:859
    break;

  case 28:
#line 93 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_STATEMENT>();}
#line 839 "parser.cpp" // lalr1.cc:859
    break;

  case 29:
#line 94 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_STATEMENT>(); ((node_STATEMENT*)yylhs.value.as< ast_node* > ())->init(true, yystack_[1].value.as< ast_node* > ());}
#line 845 "parser.cpp" // lalr1.cc:859
    break;

  case 30:
#line 96 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_RETURN>(); yylhs.value.as< ast_node* > ()->appendChild({yystack_[0].value.as< ast_node* > ()});}
#line 851 "parser.cpp" // lalr1.cc:859
    break;

  case 31:
#line 97 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_RETURN>();}
#line 857 "parser.cpp" // lalr1.cc:859
    break;

  case 32:
#line 98 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_WHILE>(); yylhs.value.as< ast_node* > ()->appendChild({yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ()});}
#line 863 "parser.cpp" // lalr1.cc:859
    break;

  case 33:
#line 99 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_FOR>(); yylhs.value.as< ast_node* > ()->appendChild({yystack_[6].value.as< ast_node* > (), yystack_[4].value.as< ast_node* > (), yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ()});}
#line 869 "parser.cpp" // lalr1.cc:859
    break;

  case 34:
#line 101 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_IF>(); yylhs.value.as< ast_node* > ()->appendChild({yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ()});}
#line 875 "parser.cpp" // lalr1.cc:859
    break;

  case 35:
#line 102 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_IF>(); yylhs.value.as< ast_node* > ()->appendChild({yystack_[4].value.as< ast_node* > (), yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ()});}
#line 881 "parser.cpp" // lalr1.cc:859
    break;

  case 36:
#line 104 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[0].value.as< ast_node* > ();}
#line 887 "parser.cpp" // lalr1.cc:859
    break;

  case 37:
#line 105 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>();}
#line 893 "parser.cpp" // lalr1.cc:859
    break;

  case 38:
#line 107 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_VARDEF>(); ((node_VARDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[1].value.as< std::string > (), "", yystack_[0].value.as< std::string > (), NULL);}
#line 899 "parser.cpp" // lalr1.cc:859
    break;

  case 39:
#line 108 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_VARDEF>(); ((node_VARDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[3].value.as< std::string > (), "", yystack_[2].value.as< std::string > (), yystack_[0].value.as< ast_node* > ());}
#line 905 "parser.cpp" // lalr1.cc:859
    break;

  case 40:
#line 109 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_VARDEF>(); ((node_VARDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[2].value.as< std::string > (), yystack_[1].value.as< std::string > (), yystack_[0].value.as< std::string > (), NULL);}
#line 911 "parser.cpp" // lalr1.cc:859
    break;

  case 41:
#line 110 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_VARDEF>(); ((node_VARDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[4].value.as< std::string > (), yystack_[3].value.as< std::string > (), yystack_[2].value.as< std::string > (), yystack_[0].value.as< ast_node* > ());}
#line 917 "parser.cpp" // lalr1.cc:859
    break;

  case 42:
#line 111 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_VARDEF>(); ((node_VARDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[3].value.as< std::string > (), "", yystack_[2].value.as< std::string > (), yystack_[0].value.as< ast_node* > ());}
#line 923 "parser.cpp" // lalr1.cc:859
    break;

  case 43:
#line 112 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_VARDEF>(); ((node_VARDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[4].value.as< std::string > (), yystack_[3].value.as< std::string > (), yystack_[2].value.as< std::string > (), yystack_[0].value.as< ast_node* > ());}
#line 929 "parser.cpp" // lalr1.cc:859
    break;

  case 44:
#line 113 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_VARDEF>(); ((node_VARDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[1].value.as< std::string > (), "", yystack_[0].value.as< std::string > (), NULL);}
#line 935 "parser.cpp" // lalr1.cc:859
    break;

  case 45:
#line 114 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_VARDEF>(); ((node_VARDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[2].value.as< std::string > (), yystack_[1].value.as< std::string > (), yystack_[0].value.as< std::string > (), NULL);}
#line 941 "parser.cpp" // lalr1.cc:859
    break;

  case 46:
#line 116 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_CLASSDEF>(); ((node_CLASSDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[3].value.as< std::string > (), yystack_[1].value.as< ast_node* > ());}
#line 947 "parser.cpp" // lalr1.cc:859
    break;

  case 47:
#line 117 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_CLASSDEF>(); ((node_CLASSDEF*)yylhs.value.as< ast_node* > ())->init(yystack_[2].value.as< std::string > (), NULL);}
#line 953 "parser.cpp" // lalr1.cc:859
    break;

  case 48:
#line 119 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_CLASSPARADEF>(); ((node_CLASSPARADEF*)yylhs.value.as< ast_node* > ())->init(yystack_[1].value.as< ast_node* > ());}
#line 959 "parser.cpp" // lalr1.cc:859
    break;

  case 49:
#line 120 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_CLASSPARADEF>(); ((node_CLASSPARADEF*)yylhs.value.as< ast_node* > ())->init(yystack_[0].value.as< ast_node* > ());}
#line 965 "parser.cpp" // lalr1.cc:859
    break;

  case 50:
#line 121 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_CLASSPARADEF>(); ((node_CLASSPARADEF*)yylhs.value.as< ast_node* > ())->init(yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ());}
#line 971 "parser.cpp" // lalr1.cc:859
    break;

  case 51:
#line 122 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_CLASSPARADEF>(); ((node_CLASSPARADEF*)yylhs.value.as< ast_node* > ())->init(yystack_[1].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > ());}
#line 977 "parser.cpp" // lalr1.cc:859
    break;

  case 52:
#line 124 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< std::string > ()=yystack_[0].value.as< std::string > ();}
#line 983 "parser.cpp" // lalr1.cc:859
    break;

  case 53:
#line 125 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< std::string > ()=yystack_[0].value.as< std::string > ();}
#line 989 "parser.cpp" // lalr1.cc:859
    break;

  case 54:
#line 126 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< std::string > ()=yystack_[0].value.as< std::string > ();}
#line 995 "parser.cpp" // lalr1.cc:859
    break;

  case 55:
#line 127 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< std::string > ()=yystack_[0].value.as< std::string > ();}
#line 1001 "parser.cpp" // lalr1.cc:859
    break;

  case 56:
#line 129 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_CREATOR>(); yylhs.value.as< ast_node* > ()->appendChild({yystack_[1].value.as< ast_node* > ()});}
#line 1007 "parser.cpp" // lalr1.cc:859
    break;

  case 57:
#line 130 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[3].value.as< ast_node* > (); yylhs.value.as< ast_node* > ()->appendChild({yystack_[1].value.as< ast_node* > ()});}
#line 1013 "parser.cpp" // lalr1.cc:859
    break;

  case 58:
#line 132 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(_ID, NULL, NULL, yystack_[0].value.as< std::string > ());}
#line 1019 "parser.cpp" // lalr1.cc:859
    break;

  case 59:
#line 133 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(_INT, NULL, NULL, yystack_[0].value.as< std::string > ());}
#line 1025 "parser.cpp" // lalr1.cc:859
    break;

  case 60:
#line 134 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(_NULL, NULL, NULL, yystack_[0].value.as< std::string > ());}
#line 1031 "parser.cpp" // lalr1.cc:859
    break;

  case 61:
#line 135 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(_BOOL, NULL, NULL, yystack_[0].value.as< std::string > ());}
#line 1037 "parser.cpp" // lalr1.cc:859
    break;

  case 62:
#line 136 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(_BOOL, NULL, NULL, yystack_[0].value.as< std::string > ());}
#line 1043 "parser.cpp" // lalr1.cc:859
    break;

  case 63:
#line 137 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(_STRING, NULL, NULL, yystack_[0].value.as< std::string > ());}
#line 1049 "parser.cpp" // lalr1.cc:859
    break;

  case 64:
#line 138 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(SPER, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1055 "parser.cpp" // lalr1.cc:859
    break;

  case 65:
#line 139 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(ASIG, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1061 "parser.cpp" // lalr1.cc:859
    break;

  case 66:
#line 140 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(LOR, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1067 "parser.cpp" // lalr1.cc:859
    break;

  case 67:
#line 141 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(LAND, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1073 "parser.cpp" // lalr1.cc:859
    break;

  case 68:
#line 142 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(BOR, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1079 "parser.cpp" // lalr1.cc:859
    break;

  case 69:
#line 143 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(BXOR, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1085 "parser.cpp" // lalr1.cc:859
    break;

  case 70:
#line 144 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(BAND, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1091 "parser.cpp" // lalr1.cc:859
    break;

  case 71:
#line 145 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(EQU, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1097 "parser.cpp" // lalr1.cc:859
    break;

  case 72:
#line 146 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(NEQ, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1103 "parser.cpp" // lalr1.cc:859
    break;

  case 73:
#line 147 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(LST, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1109 "parser.cpp" // lalr1.cc:859
    break;

  case 74:
#line 148 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(BGT, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1115 "parser.cpp" // lalr1.cc:859
    break;

  case 75:
#line 149 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(LET, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1121 "parser.cpp" // lalr1.cc:859
    break;

  case 76:
#line 150 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(SHL, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1127 "parser.cpp" // lalr1.cc:859
    break;

  case 77:
#line 151 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(SHR, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1133 "parser.cpp" // lalr1.cc:859
    break;

  case 78:
#line 152 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(BET, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1139 "parser.cpp" // lalr1.cc:859
    break;

  case 79:
#line 153 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(ADD, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1145 "parser.cpp" // lalr1.cc:859
    break;

  case 80:
#line 154 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(SUB, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1151 "parser.cpp" // lalr1.cc:859
    break;

  case 81:
#line 155 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(MUL, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1157 "parser.cpp" // lalr1.cc:859
    break;

  case 82:
#line 156 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(DIV, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1163 "parser.cpp" // lalr1.cc:859
    break;

  case 83:
#line 157 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(MOD, yystack_[2].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1169 "parser.cpp" // lalr1.cc:859
    break;

  case 84:
#line 158 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(DEN, yystack_[0].value.as< ast_node* > (), NULL, "");}
#line 1175 "parser.cpp" // lalr1.cc:859
    break;

  case 85:
#line 159 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(F_SUB, yystack_[0].value.as< ast_node* > (), NULL, "");}
#line 1181 "parser.cpp" // lalr1.cc:859
    break;

  case 86:
#line 160 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(F_ADD, yystack_[0].value.as< ast_node* > (), NULL, "");}
#line 1187 "parser.cpp" // lalr1.cc:859
    break;

  case 87:
#line 161 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(NEG, yystack_[0].value.as< ast_node* > (), NULL, "");}
#line 1193 "parser.cpp" // lalr1.cc:859
    break;

  case 88:
#line 162 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(F_INC, yystack_[0].value.as< ast_node* > (), NULL, "");}
#line 1199 "parser.cpp" // lalr1.cc:859
    break;

  case 89:
#line 163 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(F_DEC, yystack_[0].value.as< ast_node* > (), NULL, "");}
#line 1205 "parser.cpp" // lalr1.cc:859
    break;

  case 90:
#line 164 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(E_INC, yystack_[1].value.as< ast_node* > (), NULL, "");}
#line 1211 "parser.cpp" // lalr1.cc:859
    break;

  case 91:
#line 165 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(E_DEC, yystack_[1].value.as< ast_node* > (), NULL, "");}
#line 1217 "parser.cpp" // lalr1.cc:859
    break;

  case 92:
#line 166 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(SUBSCRIPT, yystack_[1].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1223 "parser.cpp" // lalr1.cc:859
    break;

  case 93:
#line 167 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(CALL, yystack_[1].value.as< ast_node* > (), yystack_[0].value.as< ast_node* > (), "");}
#line 1229 "parser.cpp" // lalr1.cc:859
    break;

  case 94:
#line 168 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(MEMB, yystack_[2].value.as< ast_node* > (), NULL, yystack_[0].value.as< std::string > ());}
#line 1235 "parser.cpp" // lalr1.cc:859
    break;

  case 95:
#line 169 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[1].value.as< ast_node* > ();}
#line 1241 "parser.cpp" // lalr1.cc:859
    break;

  case 96:
#line 170 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_EXPR>(); ((node_EXPR*)yylhs.value.as< ast_node* > ())->init(NEW, yystack_[0].value.as< ast_node* > (), NULL, "");}
#line 1247 "parser.cpp" // lalr1.cc:859
    break;

  case 97:
#line 172 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_NEW>(); ((node_NEW*)yylhs.value.as< ast_node* > ())->init(yystack_[0].value.as< std::string > (), "", NULL);}
#line 1253 "parser.cpp" // lalr1.cc:859
    break;

  case 98:
#line 173 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_NEW>(); ((node_NEW*)yylhs.value.as< ast_node* > ())->init(yystack_[1].value.as< std::string > (), "", yystack_[0].value.as< ast_node* > ());}
#line 1259 "parser.cpp" // lalr1.cc:859
    break;

  case 99:
#line 174 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_NEW>(); ((node_NEW*)yylhs.value.as< ast_node* > ())->init(yystack_[2].value.as< std::string > (), yystack_[0].value.as< std::string > (), yystack_[1].value.as< ast_node* > ());}
#line 1265 "parser.cpp" // lalr1.cc:859
    break;

  case 100:
#line 175 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_NEW>(); ((node_NEW*)yylhs.value.as< ast_node* > ())->init(yystack_[1].value.as< std::string > (), "", yystack_[0].value.as< ast_node* > ());}
#line 1271 "parser.cpp" // lalr1.cc:859
    break;

  case 101:
#line 176 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_NEW>(); ((node_NEW*)yylhs.value.as< ast_node* > ())->init(yystack_[0].value.as< std::string > (), "", NULL);}
#line 1277 "parser.cpp" // lalr1.cc:859
    break;

  case 102:
#line 177 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_NEW>(); ((node_NEW*)yylhs.value.as< ast_node* > ())->init(yystack_[2].value.as< std::string > (), yystack_[0].value.as< std::string > (), yystack_[1].value.as< ast_node* > ());}
#line 1283 "parser.cpp" // lalr1.cc:859
    break;

  case 103:
#line 178 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_NEW>(); ((node_NEW*)yylhs.value.as< ast_node* > ())->init(yystack_[1].value.as< std::string > (), "", NULL, yystack_[0].value.as< ast_node* > ());}
#line 1289 "parser.cpp" // lalr1.cc:859
    break;

  case 104:
#line 180 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=yystack_[1].value.as< ast_node* > ();}
#line 1295 "parser.cpp" // lalr1.cc:859
    break;

  case 105:
#line 182 "parser.yy" // lalr1.cc:859
    {yylhs.value.as< ast_node* > ()=ast.create<node_PARALIST>();}
#line 1301 "parser.cpp" // lalr1.cc:859
    break;

  case 106:
#line 183 "parser.yy" // lalr1.cc:859
    {
                yylhs.value.as< ast_node* > ()=ast.create<node_PARALIST>();
                if(((node_EXPR*)yystack_[1].value.as< ast_node* > ())->op_info == SPER)
                        yylhs.value.as< ast_node* > ()->merge(yystack_[1].value.as< ast_node* > ());
                else
                        yylhs.value.as< ast_node* > ()->appendChild({yystack_[1].value.as< ast_node* > ()});
        }
#line 1313 "parser.cpp" // lalr1.cc:859
    break;


#line 1317 "parser.cpp" // lalr1.cc:859
            default:
              break;
            }
        }
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yysyntax_error_ (yystack_[0].state, yyla));
      }


    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }


      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
  parser::error (const syntax_error& yyexc)
  {
    error (yyexc.what());
  }

  // Generate an error message.
  std::string
  parser::yysyntax_error_ (state_type, const symbol_type&) const
  {
    return YY_("syntax error");
  }


  const short int parser::yypact_ninf_ = -178;

  const signed char parser::yytable_ninf_ = -1;

  const short int
  parser::yypact_[] =
  {
     190,    77,  -178,  -178,  -178,   -25,  -178,    10,  -178,   -30,
    -178,    32,    99,    11,   -21,   232,   -15,  -178,  -178,   -12,
    -178,  -178,    12,    -3,    72,     8,    44,    87,     2,   232,
      13,     2,     2,     2,     2,   635,     2,     2,     2,    72,
    -178,  -178,  -178,    49,    16,    20,     2,    40,    48,    50,
    -178,  -178,  -178,  -178,    34,  -178,  -178,  -178,    45,   284,
    -178,   643,  -178,     2,   232,    15,    76,    55,  -178,    99,
      88,    71,  -178,   551,  -178,     2,   232,   209,   209,   209,
     209,    25,    78,   209,   209,   408,  -178,   180,  -178,  -178,
    -178,   524,     2,     2,     2,  -178,  -178,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,  -178,  -178,    74,
     112,     2,  -178,  -178,  -178,  -178,   649,    85,    90,   551,
    -178,     2,   232,   118,  -178,   121,   551,  -178,     2,   -28,
    -178,    -7,  -178,  -178,  -178,   437,    93,   524,   466,   551,
     551,   576,   600,   623,   645,   694,   713,   713,   728,   728,
     728,   728,   741,   741,   752,   752,   209,   209,   209,  -178,
    -178,   495,   318,  -178,   649,  -178,   551,  -178,   348,     2,
    -178,  -178,   232,     2,   232,  -178,  -178,  -178,  -178,   378,
    -178,    97,    86,  -178,     2,   232,   103,  -178,   232,  -178
  };

  const unsigned char
  parser::yydefact_[] =
  {
       0,     0,    52,    53,    54,     0,    55,     0,     2,     0,
       4,     0,    14,    44,     0,     0,     0,     1,     5,     0,
       7,     3,    38,     0,     0,     0,    15,     0,     0,     0,
      45,     0,     0,     0,     0,     0,     0,     0,     0,    58,
      59,    63,    27,     0,     0,     0,    31,     0,     0,     0,
      60,    61,    62,    12,     0,    22,    23,    21,     0,     0,
      96,     0,     6,     0,     0,    40,    44,     0,    13,    14,
      38,     0,    58,    42,     9,     0,     0,    86,    85,    84,
      87,   101,    97,    88,    89,     0,    28,     0,    17,    25,
      26,    30,     0,    37,     0,    24,    20,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    90,    91,     0,
       0,     0,    19,    92,    93,    47,    49,     0,     0,    39,
       8,     0,     0,    45,    16,    40,    43,    11,     0,   100,
     103,    98,    95,    29,    18,     0,     0,    36,     0,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    78,    76,    77,    79,    80,    81,    82,    83,    94,
     105,     0,     0,    51,    48,    46,    41,    10,     0,     0,
     102,    99,     0,    37,     0,   106,   104,    50,    56,     0,
      32,     0,    34,    57,    37,     0,     0,    35,     0,    33
  };

  const short int
  parser::yypgoto_[] =
  {
    -178,  -178,    41,    -9,    82,  -178,   -27,  -178,  -178,  -178,
    -178,  -177,     0,   136,  -123,     1,    79,    84,  -178,  -178,
      89
  };

  const short int
  parser::yydefgoto_[] =
  {
      -1,     7,   126,    15,    25,    87,    53,    54,    55,    56,
      57,   146,    58,    10,   128,    27,   139,    59,    60,   123,
     124
  };

  const unsigned char
  parser::yytable_[] =
  {
       9,    11,    74,   173,    29,   179,   191,    19,    11,    21,
      17,    16,    26,    64,   180,    30,    88,   196,    28,    63,
      75,    76,   131,    31,    32,    61,   179,    62,    33,    34,
      35,    36,    37,    65,    38,   181,    82,   130,    72,    40,
      41,     8,    68,    12,    12,    12,     1,    12,    18,   137,
      69,   187,    50,    51,    52,    89,   132,   120,   138,    90,
     144,   127,    11,     2,     3,     4,     5,     6,    22,    26,
      31,    32,    92,    95,    23,    33,    34,    35,    36,    37,
      93,    38,    94,    28,    96,    39,    40,    41,    42,    43,
      86,   133,    44,    45,    46,    63,    47,    48,    49,    50,
      51,    52,     2,     3,     4,   177,     6,   135,    66,    12,
     169,   138,    73,    13,    67,    77,    78,    79,    80,    14,
      83,    84,    85,    70,   174,    75,   127,    11,   131,    71,
      91,   175,   183,    31,    32,    24,   194,   198,    33,    34,
      35,    36,    37,    20,    38,   195,   170,   129,    72,    40,
      41,   134,     2,     3,     4,   190,     6,   192,     0,   136,
       0,   141,    50,    51,    52,     0,     0,     0,   197,     0,
     140,   199,     0,     0,   127,    11,   145,   147,   148,     0,
       0,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,    31,    32,     0,   171,   172,    33,    34,    35,    36,
      37,     0,    38,     0,     0,   176,    39,    40,    41,    42,
      43,   143,   178,    44,    45,    46,     1,    47,    48,    49,
      50,    51,    52,     2,     3,     4,     0,     6,   117,   118,
     119,   120,   121,     2,     3,     4,     5,     6,     0,     0,
       0,     0,     0,    31,    32,     0,     0,     0,    33,    34,
      35,    36,    37,   189,    38,     0,     0,   147,    39,    40,
      41,    42,    43,     0,     0,    44,    45,    46,   147,    47,
      48,    49,    50,    51,    52,     2,     3,     4,     0,     6,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
       0,     0,     0,   117,   118,   119,   120,   121,     0,     0,
       0,     0,     0,   122,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,     0,     0,     0,   117,   118,   119,
     120,   121,     0,   186,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,     0,     0,     0,   117,   118,   119,
     120,   121,     0,   188,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,     0,     0,     0,   117,   118,   119,
     120,   121,     0,   193,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,     0,     0,     0,   117,   118,   119,
     120,   121,   142,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,     0,     0,     0,   117,   118,   119,   120,
     121,   182,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,     0,     0,     0,   117,   118,   119,   120,   121,
     184,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,     0,     0,     0,   117,   118,   119,   120,   121,   185,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
       0,     0,     0,   117,   118,   119,   120,   121,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,     0,     0,     0,
     117,   118,   119,   120,   121,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,     0,     0,     0,   117,   118,   119,   120,   121,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,     0,     0,     0,   117,
     118,   119,   120,   121,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,     0,
       0,     0,   117,   118,   119,   120,   121,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,    81,     0,     0,   117,   118,   119,   120,   121,     1,
       0,     0,     0,     0,   125,     1,     0,     0,     2,     3,
       4,     0,     6,     0,     0,     0,     2,     3,     4,     0,
       6,     0,     2,     3,     4,     0,     6,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
       0,     0,     0,   117,   118,   119,   120,   121,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,     0,
       0,     0,   117,   118,   119,   120,   121,   110,   111,   112,
     113,   114,   115,   116,     0,     0,     0,   117,   118,   119,
     120,   121,   112,   113,   114,   115,   116,     0,     0,     0,
     117,   118,   119,   120,   121,   114,   115,   116,     0,     0,
       0,   117,   118,   119,   120,   121
  };

  const short int
  parser::yycheck_[] =
  {
       0,     0,    29,   126,    13,    33,   183,     7,     7,    39,
       0,    36,    12,    22,    42,    36,    43,   194,     7,     7,
       7,    30,     7,    21,    22,    40,    33,    39,    26,    27,
      28,    29,    30,    36,    32,    42,    35,    64,    36,    37,
      38,     0,    34,    32,    32,    32,    36,    32,     7,    76,
       6,   174,    50,    51,    52,    39,    65,    32,    33,    39,
      87,    61,    61,    53,    54,    55,    56,    57,    36,    69,
      21,    22,    32,    39,    42,    26,    27,    28,    29,    30,
      32,    32,    32,     7,    39,    36,    37,    38,    39,    40,
      41,    36,    43,    44,    45,     7,    47,    48,    49,    50,
      51,    52,    53,    54,    55,   132,    57,    36,    36,    32,
      36,    33,    28,    36,    42,    31,    32,    33,    34,    42,
      36,    37,    38,    36,    39,     7,   126,   126,     7,    42,
      46,    41,    39,    21,    22,    36,    39,    34,    26,    27,
      28,    29,    30,     7,    32,    59,    34,    63,    36,    37,
      38,    69,    53,    54,    55,   182,    57,   184,    -1,    75,
      -1,    82,    50,    51,    52,    -1,    -1,    -1,   195,    -1,
      81,   198,    -1,    -1,   174,   174,    92,    93,    94,    -1,
      -1,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,    21,    22,    -1,   120,   121,    26,    27,    28,    29,
      30,    -1,    32,    -1,    -1,   131,    36,    37,    38,    39,
      40,    41,   138,    43,    44,    45,    36,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    -1,    57,    29,    30,
      31,    32,    33,    53,    54,    55,    56,    57,    -1,    -1,
      -1,    -1,    -1,    21,    22,    -1,    -1,    -1,    26,    27,
      28,    29,    30,   179,    32,    -1,    -1,   183,    36,    37,
      38,    39,    40,    -1,    -1,    43,    44,    45,   194,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    -1,    57,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    -1,    -1,
      -1,    -1,    -1,    39,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    -1,    -1,    -1,    29,    30,    31,
      32,    33,    -1,    35,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    -1,    -1,    -1,    29,    30,    31,
      32,    33,    -1,    35,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    -1,    -1,    -1,    29,    30,    31,
      32,    33,    -1,    35,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    -1,    -1,    -1,    29,    30,    31,
      32,    33,    34,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    -1,    -1,    -1,    29,    30,    31,    32,
      33,    34,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    -1,    -1,    -1,    29,    30,    31,    32,    33,
      34,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    -1,    -1,    -1,    29,    30,    31,    32,    33,    34,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      -1,    -1,    -1,    29,    30,    31,    32,    33,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    -1,    -1,    -1,
      29,    30,    31,    32,    33,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    -1,    -1,    -1,    29,    30,    31,    32,    33,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    -1,    -1,    -1,    29,
      30,    31,    32,    33,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    -1,
      -1,    -1,    29,    30,    31,    32,    33,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    36,    -1,    -1,    29,    30,    31,    32,    33,    36,
      -1,    -1,    -1,    -1,    41,    36,    -1,    -1,    53,    54,
      55,    -1,    57,    -1,    -1,    -1,    53,    54,    55,    -1,
      57,    -1,    53,    54,    55,    -1,    57,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    -1,
      -1,    -1,    29,    30,    31,    32,    33,    19,    20,    21,
      22,    23,    24,    25,    -1,    -1,    -1,    29,    30,    31,
      32,    33,    21,    22,    23,    24,    25,    -1,    -1,    -1,
      29,    30,    31,    32,    33,    23,    24,    25,    -1,    -1,
      -1,    29,    30,    31,    32,    33
  };

  const unsigned char
  parser::yystos_[] =
  {
       0,    36,    53,    54,    55,    56,    57,    61,    62,    72,
      73,    75,    32,    36,    42,    63,    36,     0,    62,    72,
      73,    39,    36,    42,    36,    64,    72,    75,     7,    63,
      36,    21,    22,    26,    27,    28,    29,    30,    32,    36,
      37,    38,    39,    40,    43,    44,    45,    47,    48,    49,
      50,    51,    52,    66,    67,    68,    69,    70,    72,    77,
      78,    40,    39,     7,    63,    36,    36,    42,    34,     6,
      36,    42,    36,    77,    66,     7,    63,    77,    77,    77,
      77,    36,    75,    77,    77,    77,    41,    65,    66,    39,
      39,    77,    32,    32,    32,    39,    39,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    29,    30,    31,
      32,    33,    39,    79,    80,    41,    62,    72,    74,    77,
      66,     7,    63,    36,    64,    36,    77,    66,    33,    76,
      80,    76,    34,    41,    66,    77,    71,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    36,
      34,    77,    77,    74,    39,    41,    77,    66,    77,    33,
      42,    42,    34,    39,    34,    34,    35,    74,    35,    77,
      66,    71,    66,    35,    39,    59,    71,    66,    34,    66
  };

  const unsigned char
  parser::yyr1_[] =
  {
       0,    60,    61,    61,    61,    61,    61,    61,    62,    62,
      62,    62,    62,    63,    64,    64,    64,    65,    65,    66,
      66,    66,    66,    66,    66,    66,    66,    66,    66,    66,
      67,    67,    68,    69,    70,    70,    71,    71,    72,    72,
      72,    72,    72,    72,    72,    72,    73,    73,    74,    74,
      74,    74,    75,    75,    75,    75,    76,    76,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    78,    78,    78,
      78,    78,    78,    78,    79,    80,    80
  };

  const unsigned char
  parser::yyr2_[] =
  {
       0,     2,     1,     2,     1,     2,     3,     2,     4,     4,
       5,     5,     3,     3,     0,     1,     3,     1,     2,     2,
       2,     1,     1,     1,     2,     2,     2,     1,     2,     3,
       2,     1,     5,     9,     5,     7,     1,     0,     2,     4,
       3,     5,     4,     5,     2,     3,     5,     4,     2,     1,
       3,     2,     1,     1,     1,     1,     3,     4,     1,     1,
       1,     1,     1,     1,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     3,     3,     1,     2,     3,     4,
       3,     2,     4,     3,     3,     2,     3
  };


#if YYDEBUG
  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const parser::yytname_[] =
  {
  "END_OF_FILE", "error", "$undefined", "vir", "virc1", "virc2", "sper",
  "asig", "lor", "land", "bor", "bxor", "band", "equ", "neq", "lst", "bgt",
  "let", "bet", "shl", "shr", "add", "sub", "mul", "div", "mod", "den",
  "neg", "_new", "inc", "dec", "memb", "lrod", "lsqu", "rrod", "rsqu",
  "id", "inte", "str", "semi", "lbra", "rbra", "creator", "ctn", "brk",
  "ret", "whi", "_while", "_for", "_if", "_null", "_true", "_false",
  "_void", "_int", "_string", "_class", "_bool", "_then", "_else",
  "$accept", "PROGRAM", "FUNCDEF", "PARADEF", "PARADEFLIST", "STALIST",
  "STATEMENT", "RETURN", "WHILE", "FOR", "IF", "FOR_EXPR", "VARDEF",
  "CLASSDEF", "CLASSPARADEF", "TYPEBASE", "CREATOR", "EXPR", "_NEW",
  "_SUBSCRIPT", "PARALIST", YY_NULLPTR
  };


  const unsigned char
  parser::yyrline_[] =
  {
       0,    62,    62,    63,    64,    65,    66,    67,    69,    70,
      71,    72,    73,    75,    77,    78,    79,    81,    82,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      96,    97,    98,    99,   101,   102,   104,   105,   107,   108,
     109,   110,   111,   112,   113,   114,   116,   117,   119,   120,
     121,   122,   124,   125,   126,   127,   129,   130,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   172,   173,   174,
     175,   176,   177,   178,   180,   182,   183
  };

  // Print the state stack on the debug stream.
  void
  parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG



} // yy
#line 1842 "parser.cpp" // lalr1.cc:1167
#line 191 "parser.yy" // lalr1.cc:1168

void yy::parser::error (const std::string& m)
{
        printf("error:%s\n", m.c_str());
        throw("parser failed.");
}
