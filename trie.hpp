#ifndef _trie_hpp
#define _trie_hpp

#include<iostream>
#include<string>
#include<list>

using namespace std;

template<class DATA>
class trie
{
  private:
    bool bind;
    string (*trans)(const string &);

    struct trie_node
    {
        trie_node **son;
        DATA data;
        int time_stamp;
        bool valid;
        bool is_copy;
        trie_node *cp_from = NULL;
        list<trie_node*> notify_list;

        trie_node() = delete;
        trie_node(const trie_node &) = delete;
        trie_node(trie_node &ob, int siz):data(ob.data), time_stamp(ob.time_stamp), valid(ob.valid), is_copy(ob.valid)
        {
            cp_from = &ob;
            ob.notify_list.push_back(this);
            son = new trie_node* [siz];
            for(int i = 0; i < siz; ++i)
                son[i] = ob.son[i];
        }
        trie_node(int siz, int time = 0):time_stamp(time), valid(0), is_copy(false)
        {
            son = new trie_node* [siz];
            for(int i = 0; i < siz; ++i)
                son[i] = NULL;
        }
        void set_son(int id, trie_node *v)
        {
            son[id] = v;
            typename list<trie_node*>::iterator it;
            for(it = notify_list.begin(); it != notify_list.end(); ++it)
            {
                if((*it)->son[id] == NULL || (*it)->son[id]->time_stamp < son[id]->time_stamp)
                    (*it)->set_son(id, v);
            }
        }
        void set_data(const DATA &d, bool cp = false)
        {
            data = d;
            valid = 1;
            is_copy = cp;
            typename list<trie_node*>::iterator it;
            for(it = notify_list.begin(); it != notify_list.end(); ++it)
            {
                if(!((*it)->valid) || (*it)->is_copy)
                    (*it)->set_data(d, true);
            }
        }
        ~trie_node()
        {
            delete [] son;
        }
    };

    trie_node *root;
    int time_stamp;
    int _Max;

  public:
    trie() = delete;
    trie(const trie &ob):_Max(ob._Max), time_stamp(ob.time_stamp + 1), bind(ob.bind), trans(ob.trans)
    {
        root = new trie_node(*ob.root, _Max);
        root->time_stamp = time_stamp;
    }
    trie(int siz, int time = 0):time_stamp(time), _Max(siz), bind(false), trans(NULL)
    {
        root = new trie_node(siz, time);
    }
    ~trie()
    {
        delete_dfs(root);
    }
    void bind_trans_func(string (*fun)(const string &))
    {
        bind = true;
        trans = fun;
    }

    void insert(const string &arg, const DATA &data, bool over = false)
    {
        string st;
        if(bind)
            st = trans(arg);
        else
            st = arg;
        trie_node *now = root;
        int len(st.length());
        int p = 0;
        bool flag = 0;
        while(p < len)
        {
            if(now->son[st[p]] == NULL)
                now->set_son(st[p], new trie_node(_Max, time_stamp));
            if(now->son[st[p]]->time_stamp != time_stamp)
            {
                trie_node *tmp = new trie_node(*(now->son[st[p]]), _Max);
                tmp->time_stamp = time_stamp;
                now->set_son(st[p], tmp);
            }
            now = now->son[st[p]];
            ++p;
        }
        if(now->valid && !over && !now->is_copy)
        {
            cout << "Wrong: " << arg << endl;
            throw("This id has been defined!");
        }
        now->set_data(data);
    }

    bool is_defined(const string &arg, bool limit = false)
    {
        trie_node *tmp(find_in(arg, limit));
        return (tmp != NULL && tmp->valid);
    }

    DATA &find(const string &arg, bool limit = false)
    {
        trie_node *tmp(find_in(arg, limit));
        if(tmp == NULL || !tmp->valid)
        {
            cout << arg << endl;
            throw("This id hasn't been defined");
        }
        return tmp->data;
    }

  private:
    void delete_dfs(trie_node *now)
    {
        if(now->time_stamp != time_stamp)
            return;
        for(int i = 0; i < _Max; ++i)
            if(now->son[i] != NULL)
                delete_dfs(now->son[i]);
        delete now;
    }

    trie_node *find_in(const string &arg, bool limit = false)
    {
        string st;
        if(bind)
            st = trans(arg);
        else
            st = arg;
        trie_node *now = root;
        int len(st.length());
        int p(0);
        while(p < len)
        {
            if(now->son[st[p]] == NULL)
                return NULL;
            now = now->son[st[p]];
            ++p;
        }
        if(limit && now->time_stamp != time_stamp)
            return NULL;
        return now;
    }
};

#endif