#include"tools.h"
#include<map>

map<string, string> sta_label;

bool assign_valid(string a, string b)
{
    if(a != b)
    {
        if(b == "null")
            swap(a, b);
        if(a != "null" || b == "int" || b == "bool" || b == "void" || b == "string")
            return false;
    }
    return true;
}

string cut_class_name(string &s)
{
    if(s.length() == 0)
        return "";
    int i;
    for(i = 0; s[i] != ']'; ++i);
    string tmp = s.substr(1, i - 1);
    s = s.substr(i + 1);
    return tmp;
}

string cut_reg_name(const string s)
{
    int siz = s.length() - 1;
    for(; s[siz] != '['; --siz);
    return s.substr(0, siz);
}

void decode_esc(string &s)
{
    int len = s.length();
    for(int i = 1; i < len - 1; ++i)
    {
        if(s[i] == '\\')
        {
            switch (s[i + 1])
            {
            case 'n':
                s[i] = '\n';
                break;
            case '\\':
                s[i] = '\\';
                break;
            case '\"':
                s[i] = '\"';
            default:
                break;
            }
            --len;
            s.erase(i + 1, 1);
        }
    }
}

string color2reg(phyreg r)
{
    switch (r)
    {
    case NONE:
        return "mem";
    case RAX:
        return "rax";
    case RDI:
        return "rdi";
    case RSI:
        return "rsi";
    case RDX:
        return "rdx";
    case RCX:
        return "rcx";
    case R8:
        return "r8";
    case R9:
        return "r9";
    case R10:
        return "r10";
    case R11:
        return "r11";
    case RBX:
        return "rbx";
    case RBP:
        return "rbp";
    case R12:
        return "r12";
    case R13:
        return "r13";
    case R14:
        return "r14";
    case R15:
        return "r15";
    case RSP:
        return "rsp";
    default:
        break;
    }
}
string qword2byte(const string s)
{
    if(s == "rax")
        return "al";
    if(s == "rbx")
        return "bl";
    if(s == "rcx")
        return "cl";
    if(s == "rdx")
        return "dl";
    if(s == "rsi")
        return "sil";
    if(s == "rdi")
        return "dil";
    if(s == "rbp")
        return "bpl";
    if(s == "rsp")
        return "spl";
    if(s == "r8")
        return "r8b";
    if(s == "r9")
        return "r9b";
    if(s == "r10")
        return "r10b";
    if(s == "r11")
        return "r11b";
    if(s == "r12")
        return "r12b";
    if(s == "r13")
        return "r13b";
    if(s == "r14")
        return "r14b";
    if(s == "r15")
        return "r15b";
}
string qword2dword(const string s)
{
    if(s == "rax")
        return "eax";
    if(s == "rbx")
        return "ebx";
    if(s == "rcx")
        return "ecx";
    if(s == "rdx")
        return "edx";
    if(s == "rsi")
        return "esi";
    if(s == "rdi")
        return "edi";
    if(s == "rbp")
        return "ebp";
    if(s == "rsp")
        return "esp";
    if(s == "r8")
        return "r8d";
    if(s == "r9")
        return "r9d";
    if(s == "r10")
        return "r10d";
    if(s == "r11")
        return "r11d";
    if(s == "r12")
        return "r12d";
    if(s == "r13")
        return "r13d";
    if(s == "r14")
        return "r14d";
    if(s == "r15")
        return "r15d";
}
string get_label_name(const string s)
{
    static int name_cnt = 0;
    if(sta_label.count(s))
        return sta_label[s];
//    printf("[      ] %s\n", s.c_str());
    if(s == "main")
        return "main";
    if(s == "]strcpy")
        return "strcpy";
    if(s == "]strcat")
        return "strcat";
    if(s == "]strcmp")
        return "strcmp";
    if(s == "]strlen")
        return "strlen";
    if(s == "]scanf")
        return "scanf";
    if(s == "]printf")
        return "printf";
    if(s == "]sprintf")
        return "sprintf";
    if(s == "]puts")
        return "puts";
    if(s == "]malloc")
        return "malloc";
    if(s == "]memcpy")
        return "memcpy";
    if(s == "]atoi")
        return "atoi";
    sta_label[s] = "L_" + to_string(++name_cnt);
    return sta_label[s];
}

bool is_power2(int a)
{
    return (a ^ (a & (-a))) == 0;
}

int power2(int a)
{
    int t = 0;
    while(!(a & 1))
    {
        ++t;
        a >>= 1;
    }
    return t;
}