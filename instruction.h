#ifndef _instruction_h
#define _instruction_h

#include "ir_enum.h"
#include "scope.h"
#include "vir_reg.h"
#include "trie.hpp"
#include "graph.h"
#include<list>
#include<set>
#include<map>

class instruction;
class Basic_block;
class prog_with_ir;
class ir;

class instruction: public flow_node
{
  public:
    Basic_block* bb = NULL;

  public:
    virtual bool is_call();

  public:
    ir* func = NULL;
    ir_ins_type type = IR_INS_NULL;
    double loop_cnt = 0;
    string label;
    set<vir_reg*> _export;
    list<instruction*>::iterator it;

    int in_cnt = 0, time = 0;

  public:
    void clean_flow_info();

  public:
    virtual ~instruction();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void sim();
    virtual void print();
    virtual void init_ins_flow();
    virtual bool def_reg(vir_reg*);
    virtual bool use_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);

    virtual vir_reg* get_def();
    virtual list<vir_reg*> get_use();

    Basic_block* get_bb();

    virtual vir_reg *constant_pro();
    virtual void copy_pro();
    virtual void eval_cost(double);

    virtual void print_x86();
    virtual void convert_to_2op();
    virtual void gvn(trie<vir_reg*>*);

    void self_remove();

  protected:
    void rename_single_reg(prog_with_ir*, trie<int>*, vir_reg *&, bool = false);
};

class ir_ins_glb: public instruction
{
  private:
    vir_reg *op;
    string v;
    bool is_string;

  public:
    ir_ins_glb(vir_reg*);
    ir_ins_glb(vir_reg*, const string);
    virtual ~ir_ins_glb();
    virtual void sim();
    virtual void print();
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual void print_x86();
};

class ir_ins_mov: public instruction
{
public:
    vir_reg *op0, *op1;

public:
    bool is_sx;

  public:
    ir_ins_mov(vir_reg*, vir_reg*, bool = false);
    virtual ~ir_ins_mov();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void sim();
    virtual void print();
    virtual bool def_reg(vir_reg*);
    virtual bool use_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual vir_reg *constant_pro();
    virtual void copy_pro();
    virtual vir_reg* get_def();
    virtual list<vir_reg*> get_use();
    virtual void eval_cost(double);
    virtual void print_x86();
};

class ir_ins_lea: public instruction
{
  public:
    vir_reg *base, *index, *target;
    long long offset, scale;

    ir_ins_lea(vir_reg*);
    ~ir_ins_lea();
    void set_base(vir_reg*);
    void set_index(vir_reg*);

    virtual void sim();
    virtual void print();
    virtual vir_reg* get_def();
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual bool def_reg(vir_reg*);
    virtual list<vir_reg*> get_use();
    virtual void eval_cost(double);
    virtual void print_x86();
};

class ir_ins_load: public instruction
{
    friend class ir;

  private:
    vir_reg *op0, *op1;
    vir_reg *base = NULL, *index = NULL;
    long long offset, scale;
    bool sx;

  public:
    ir_ins_load(vir_reg*, vir_reg*, bool = false);
    virtual ~ir_ins_load();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void sim();
    virtual void print();
    virtual bool def_reg(vir_reg*);
    virtual bool use_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual vir_reg* get_def();
    virtual list<vir_reg*> get_use();
    virtual void eval_cost(double);
    virtual void print_x86();
    void merge_lea(ir_ins_lea *);
};

class ir_ins_store: public instruction
{
    friend class ir;

  private:
    vir_reg *op0, *op1;
    vir_reg *base = NULL, *index = NULL;
    long long offset, scale;

  public:
    ir_ins_store(vir_reg*, vir_reg*);
    virtual ~ir_ins_store();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void sim();
    virtual void print();
    virtual bool use_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual list<vir_reg*> get_use();
    virtual void eval_cost(double);
    virtual void print_x86();
    void merge_lea(ir_ins_lea *);
};

class ir_ins_thr_op: public instruction
{
  friend class ir;
  protected:
    vir_reg *op0, *op1, *op2;
  
  public:
    ir_ins_thr_op(vir_reg*, vir_reg*, vir_reg*);
    ~ir_ins_thr_op();
    virtual instruction *clone(prog_with_ir *, int) = 0;
    virtual bool use_reg(vir_reg*);
    virtual bool def_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual vir_reg *constant_pro();
    virtual vir_reg* get_def();
    virtual list<vir_reg*> get_use();
    virtual void eval_cost(double);
    virtual void convert_to_2op();
};

class ir_ins_or: public ir_ins_thr_op
{
  public:
    ir_ins_or(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_or();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
};

class ir_ins_and: public ir_ins_thr_op
{
  public:
    ir_ins_and(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_and();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
};

class ir_ins_xor: public ir_ins_thr_op
{
  public:
    ir_ins_xor(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_xor();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
};

class ir_ins_equ: public ir_ins_thr_op
{
  public:
    ir_ins_equ(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_equ();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void convert_to_2op();
    virtual void print_x86();
};

class ir_ins_jequ: public instruction
{
  private:
    string label;
    vir_reg *op0, *op1;

  public:
    ir_ins_jequ(vir_reg*, vir_reg*, string);
    ~ir_ins_jequ();

    virtual void init_ins_flow();
    virtual void sim();
    virtual void print();
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual list<vir_reg*> get_use();
    virtual void eval_cost(double);
    virtual void print_x86();
};

class ir_ins_neq: public ir_ins_thr_op
{
  public:
    ir_ins_neq(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_neq();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void convert_to_2op();
    virtual void print_x86();
};

class ir_ins_jneq: public instruction
{
  private:
    string label;
    vir_reg *op0, *op1;

  public:
    ir_ins_jneq(vir_reg*, vir_reg*, string);
    ~ir_ins_jneq();

    virtual void init_ins_flow();
    virtual void sim();
    virtual void print();
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual list<vir_reg*> get_use();
    virtual void eval_cost(double);
    virtual void print_x86();
};

class ir_ins_lst: public ir_ins_thr_op
{
  public:
    ir_ins_lst(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_lst();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void convert_to_2op();
    virtual void print_x86();
};

class ir_ins_jlst: public instruction
{
  private:
    string label;
    vir_reg *op0, *op1;

  public:
    ir_ins_jlst(vir_reg*, vir_reg*, string);
    ~ir_ins_jlst();

    virtual void init_ins_flow();
    virtual void sim();
    virtual void print();
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual list<vir_reg*> get_use();
    virtual void eval_cost(double);
    virtual void print_x86();
};

class ir_ins_let: public ir_ins_thr_op
{
  public:
    ir_ins_let(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_let();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void convert_to_2op();
    virtual void print_x86();
};

class ir_ins_jlet: public instruction
{
  private:
    string label;
    vir_reg *op0, *op1;

  public:
    ir_ins_jlet(vir_reg*, vir_reg*, string);
    ~ir_ins_jlet();

    virtual void init_ins_flow();
    virtual void sim();
    virtual void print();
    virtual void replace_reg(vir_reg* , vir_reg*);
    virtual list<vir_reg*> get_use();
    virtual void eval_cost(double);
    virtual void print_x86();
};

class ir_ins_shl: public ir_ins_thr_op
{
  public:
    ir_ins_shl(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_shl();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
};

class ir_ins_shr: public ir_ins_thr_op
{
  public:
    ir_ins_shr(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_shr();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
};

class ir_ins_add: public ir_ins_thr_op
{
  public:
    ir_ins_add(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_add();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
    virtual void gvn(trie<vir_reg*>*);
};

class ir_ins_sub: public ir_ins_thr_op
{
  public:
    ir_ins_sub(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_sub();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
    virtual void gvn(trie<vir_reg*>*);
};

class ir_ins_mul: public ir_ins_thr_op
{
  public:
    ir_ins_mul(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_mul();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
    virtual void gvn(trie<vir_reg*>*);
};

class ir_ins_div: public ir_ins_thr_op
{
  public:
    ir_ins_div(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_div();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
    virtual void gvn(trie<vir_reg*>*);
};

class ir_ins_mod: public ir_ins_thr_op
{
  public:
    ir_ins_mod(vir_reg*, vir_reg*, vir_reg*);
    virtual ~ir_ins_mod();
    virtual void sim();
    virtual void print();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void print_x86();
    virtual void gvn(trie<vir_reg*>*);
};

class ir_ins_call: public instruction
{
  friend class prog_with_ir;
  friend class ir;

  public:
    virtual bool is_call();

  private:
    string name;
    list<vir_reg*> para_list;
    map<ir*, vir_reg*> phi;
    vir_reg* re;
  
  public:
    ir_ins_call(const string, vir_reg*);
    ir_ins_call(const string);
    void append_para(vir_reg*);
    string get_name();
    virtual ~ir_ins_call();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void sim();
    virtual void print();
    virtual bool use_reg(vir_reg*);
    virtual bool def_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg*, vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual vir_reg* get_def();

    const list<vir_reg*> get_para_list();
    virtual list<vir_reg*> get_use();

    virtual void print_x86();
};

class ir_ins_not: public instruction
{
  private:
    vir_reg *op0, *op1;

  public:
    ir_ins_not(vir_reg*, vir_reg*);
    virtual instruction *clone(prog_with_ir *, int);
    virtual ~ir_ins_not();
    virtual void sim();
    virtual void print();
    virtual bool use_reg(vir_reg*);
    virtual bool def_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg*, vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual vir_reg *constant_pro();
    virtual vir_reg* get_def();
    virtual list<vir_reg*> get_use();
    virtual void print_x86();
    virtual void convert_to_2op();
};

class ir_ins_neg: public instruction
{
  private:
    vir_reg *op0, *op1;

  public:
    ir_ins_neg(vir_reg*, vir_reg*);
    virtual instruction *clone(prog_with_ir *, int);
    virtual ~ir_ins_neg();
    virtual void sim();
    virtual void print();
    virtual bool use_reg(vir_reg*);
    virtual bool def_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg*, vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual vir_reg *constant_pro();
    virtual vir_reg* get_def();
    virtual list<vir_reg*> get_use();
    virtual void print_x86();
    virtual void convert_to_2op();
};

class ir_ins_jmp: public instruction
{
  friend class prog_with_ir;

  private:
    string label;

  public:
    ir_ins_jmp(const string);
    virtual ~ir_ins_jmp();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void sim();
    virtual void print();
    virtual void init_ins_flow();
    virtual void print_x86();
    string get_label();
};

class ir_ins_cjmp: public instruction
{
  friend class prog_with_ir;
  friend class ir;

  private:
    string label;
    vir_reg *op;
    bool taken;
  
  public:
    ir_ins_cjmp(const string, vir_reg*, bool = true);
    virtual ~ir_ins_cjmp();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void sim();
    virtual void print();
    virtual void init_ins_flow();
    virtual bool use_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg*, vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual list<vir_reg*> get_use();
    virtual void print_x86();
};

class ir_ins_ret: public instruction
{
  friend class prog_with_ir;
  public:
    vir_reg *op;

  public:
    ir_ins_ret(vir_reg*);
    ir_ins_ret();
    virtual ~ir_ins_ret();
    virtual instruction *clone(prog_with_ir *, int);
    virtual void sim();
    virtual void print();
    virtual bool use_reg(vir_reg*);
    virtual void set_bb(Basic_block*);
    virtual void replace_reg(vir_reg*, vir_reg*);
    virtual void rename_reg(trie<int>*, prog_with_ir*);
    virtual list<vir_reg*> get_use();
};

#endif