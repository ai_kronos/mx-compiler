#include"frontend.h"

scope *arr_sub;

void frontend_init()
{
    scope *sp = new scope;
    ast_root->sp = sp;
    type_struct t;
    sp->get_tTrie().insert("this", t);
    t.init("int", "", true);
    sp->get_tTrie().insert("[4][int][int]", t);
    sp->get_tTrie().insert("[5][int][int]", t);
    sp->get_tTrie().insert("[6][int][int]", t);
    sp->get_tTrie().insert("[12][int][int]", t);
    sp->get_tTrie().insert("[13][int][int]", t);
    sp->get_tTrie().insert("[15][int][int]", t);
    sp->get_tTrie().insert("[16][int][int]", t);
    sp->get_tTrie().insert("[17][int][int]", t);
    sp->get_tTrie().insert("[18][int][int]", t);
    sp->get_tTrie().insert("[19][int][int]", t);
    sp->get_tTrie().insert("[21][int]", t);
    sp->get_tTrie().insert("[22][int]", t);
    sp->get_tTrie().insert("[23][int]", t);
    sp->get_tTrie().insert("[31][getInt]", t);
    sp->get_fun_trie().insert("[getInt]", "");
    sp->get_tTrie().insert("[29][int]", t);
    sp->get_tTrie().insert("[30][int]", t);
    t.init("int", "", false);
    sp->get_tTrie().insert("[24][int]", t);
    sp->get_tTrie().insert("[25][int]", t);
    t.init("bool", "", true);
    sp->get_tTrie().insert("[2][bool][bool]", t);
    sp->get_tTrie().insert("[3][bool][bool]", t);
    sp->get_tTrie().insert("[7][bool][bool]", t);
    sp->get_tTrie().insert("[8][bool][bool]", t);
    sp->get_tTrie().insert("[9][int][int]", t);
    sp->get_tTrie().insert("[10][int][int]", t);
    sp->get_tTrie().insert("[11][int][int]", t);
    sp->get_tTrie().insert("[14][int][int]", t);
    sp->get_tTrie().insert("[9][string][string]", t);
    sp->get_tTrie().insert("[10][string][string]", t);
    sp->get_tTrie().insert("[11][string][string]", t);
    sp->get_tTrie().insert("[14][string][string]", t);
    sp->get_tTrie().insert("[20][bool]", t);
    sp->get_tTrie().insert("[7][string][string]", t);
    sp->get_tTrie().insert("[8][string][string]", t);
    sp->get_tTrie().insert("[7][int][int]", t);
    sp->get_tTrie().insert("[8][int][int]", t);

    t.init("string", "", true);
    sp->get_tTrie().insert("[15][string][string]", t);
    sp->get_tTrie().insert("[31][getString]", t);
    sp->get_fun_trie().insert("[getString]", "");
    sp->get_tTrie().insert("[31][toString][int]", t);
    sp->get_fun_trie().insert("[toString]", "[int]");
    t.init("void", "", true);
    sp->get_tTrie().insert("[31][print][string]", t);
    sp->get_fun_trie().insert("[print]", "[string]");
    sp->get_tTrie().insert("[31][println][string]", t);
    sp->get_fun_trie().insert("[println]", "[string]");

    t.init_func("getInt", "");
    sp->get_tTrie().insert("getInt", t);
    t.init_func("getString", "");
    sp->get_tTrie().insert("getString", t);
    t.init_func("toString", "");
    sp->get_tTrie().insert("toString", t);
    t.init_func("print", "");
    sp->get_tTrie().insert("print", t);
    t.init_func("println", "");
    sp->get_tTrie().insert("println", t);

    scope *str_sp = new scope(*sp);
    sp->get_sTrie().insert("string", str_sp);
    t.init("int", "", true);
    str_sp->get_tTrie().insert("[31][length]", t);
    sp->get_fun_trie().insert("[length]", "");
    str_sp->get_tTrie().insert("[31][parseInt]", t);
    sp->get_fun_trie().insert("[parseInt]", "");
    str_sp->get_tTrie().insert("[31][ord][int]", t);
    sp->get_fun_trie().insert("[ord]", "[int]");
    t.init("string", "", true);
    str_sp->get_tTrie().insert("[31][substring][int][int]", t);
    sp->get_fun_trie().insert("[substring]", "[int][int]");

    t.init_func("substring", "");
    str_sp->get_tTrie().insert("substring", t);
    t.init_func("length", "");
    str_sp->get_tTrie().insert("length", t);
    t.init_func("parseInt", "");
    str_sp->get_tTrie().insert("parseInt", t);
    t.init_func("ord", "");
    str_sp->get_tTrie().insert("ord", t);

    arr_sub = new scope(*sp);
    t.init_func("size", "");
    arr_sub->get_tTrie().insert("size", t);
    t.init("int", "", true);
    arr_sub->get_tTrie().insert("[31][size]", t);
    sp->get_fun_trie().insert("[size]", "");
}

void semantic()
{
    frontend_init();
    cout << "init completed." << endl;
    ast_root->build();
    cout << "build completed." << endl;
    ast_root->type_check();
    cout << "type check completed." << endl;

    scope *sp = ast_root->sp;
    type_struct t;
    t.init("int", "", true);
    if(!(sp->get_tTrie()).is_defined("main", ""))
        throw("main!!!");
    if(!(t == (sp->get_tTrie()).find("[31][main]", "")))
        throw("main!!!");
}