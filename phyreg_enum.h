#ifndef _phyreg_enum_h
#define _phyreg_enum_h

enum phyreg
{
    NONE,
    RAX,
    RDI,
    RSI,
    RDX,
    RCX,
    R8,
    R9,
    R10,
    R11,
    RBX,
    RBP,
    R12,
    R13,
    R14,
    R15,
    RSP
};

#endif