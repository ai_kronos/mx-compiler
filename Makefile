all: parser.hpp scanner.c
	g++ *.cpp *.c *.hpp -o code -O3 -std=c++14
parser.hpp parser.cpp stack.hh: parser.yy
	bison parser.yy -o parser.cpp
scanner.c: scanner.l
	flex -o scanner.c scanner.l
clean:
	rm parser.cpp parser.hpp stack.hh scanner.c *.o code
