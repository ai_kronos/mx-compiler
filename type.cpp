#include"type.h"

type_struct::type_struct(): type_info(PH), level(0), has_name(0), is_global(0), is_memb_func(0) {};

void type_struct::init(const string &t, const string &arr, bool con, int _new)
{
    is_const = con;
    int siz = arr.length();
    level = 0;
    for(int i = 0; i < siz; ++i)
        if(arr[i] == '[')
            ++level;
    if (t == "void")
        type_info = n_void;
    else if (t == "int")
        type_info = n_int;
    else if (t == "bool")
        type_info = n_bool;
    else if (t == "string")
        type_info = n_string;
    else if (t == "null")
        type_info = n_null;
    else if (t == "[]")
        type_info = PH;
    else
    {
        type_info = n_class;
        class_name = t;
    }
    level += _new;
}

void type_struct::init_func(const string &t, const string &arr, bool con, int _new)
{
    is_const = false;
    type_info = n_func;
    class_name = t;
    level = (arr.length() >> 1);
    level += _new;
}

bool type_struct::operator ==(const type_struct &ob)
{
    if (level != ob.level)
        return false;
    if (type_info == ob.type_info)
    {
        if (type_info != n_null && type_info != n_class && type_info != n_func)
            return true;
        if (type_info == n_class || type_info == n_func)
            return class_name == ob.class_name;
        return false;
    }
    else
    {
        return false;
    }
}

string type_struct::getStr()
{
    string re;
    switch (type_info)
    {
        case n_void:
            re = "[void";
            break;
        case n_int:
            re = "[int";
            break;
        case n_bool:
            re = "[bool";
            break;
        case n_string:
            re = "[string";
            break;
        case n_unknow:
        case n_null:
            re = "[null";
            break;
        case n_func:
        case n_class:
            re = "[" + class_name;
            break;
        default:
            re = "[";
            break;
    }
    for(int i = 0; i < level; ++i)
        re = re + "*";
    re = re + "]";
    return re;
}

void type_struct::dec_level(int v)
{
    level -= v;
}

int type_struct::get_level()
{
    return level;
}

bool type_struct::is_ref()
{
    if(type_info == n_int || type_info == n_bool)
    {
        return level != 0;
    }
    return 1;
}

bool type_struct::is_class()
{
    return type_info == n_class || type_info == n_string;
}

bool type_struct::is_func()
{
    return type_info == n_func;
}

string type_struct::getClassname()
{
    if(type_info == n_string)
        return "string";
    return class_name;
}

bool type_struct::is_set_reg()
{
    return has_name;
}

void type_struct::set_reg(const string s)
{
    has_name = true;
    reg_name = s;
}

string type_struct::get_reg_name()
{
    return reg_name;
}