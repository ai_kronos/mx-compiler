#ifndef _ir_h
#define _ir_h

#include "ir_enum.h"
#include "scope.h"
#include "vir_reg.h"
#include "trie.hpp"
#include "allocation.h"
#include "graph.h"
#include "instruction.h"
#include<list>
#include<set>
#include<map>

using namespace std;

class instruction;
class Basic_block;
class prog_with_ir;
class ir;

class ir: public flow_node
{
  friend class ir_ins_call;
  friend class ir_ins_jmp;
  friend class ir_ins_cjmp;
  friend class ir_ins_ret;
  friend class prog_with_ir;

  public:
    static string trans(const string&);

  public:
    class iterator
    {
        friend class ir;

      private:
        list<instruction*>::iterator p;
        list<instruction*> *ins_list;

      public:
        iterator();
        iterator(const iterator&);
        iterator(list<instruction *>::iterator, list<instruction *> *);

      public:
        iterator& operator ++();
        iterator& operator --();
        instruction* operator *();

        bool operator ==(const iterator &);
        bool operator !=(const iterator &);

        void cut();
        void insert(instruction *);
        void append(instruction *);
    };

    iterator begin();
    iterator end();

  private:
    Basic_block *cfg;
    trie<iterator> label_set;
    list<vir_reg*> para_list;
    set<vir_reg*> global_set;

  public:
    prog_with_ir* prog;
    int frame_cnt = 0;

    vir_reg *ret_reg;
    long long ret_val;
  
  public:
    string label;
    list<instruction*> ins_list;
    bool jmp, call, ret, cjmp;
    string name_flag;

  public:
    ir() = delete;
    ir(prog_with_ir*);
    ~ir();

    void tmp_func_label();
    void init_pcg();
    void replace_global();
    void remove_useless_call();

    void clean_flow_info();

    void insert_ins_in_bb(list<instruction*>::iterator, instruction *);
    void append_inst(instruction *);
    void push_front_inst(instruction *);
    void append_para(vir_reg *);
    long long sim(bool = false);
    void print();
    void clean_ins_flag();

    void init_label_set();
    void build_ins_flow_graph();
    void rebuild_ins_flow_graph();
    void build_cfg();
    void place_phi();
    void add_phi();
    void liveness_analysis();
    void renaming();

    void mark_phi_v();

    void constant_pro();
    void copy_pro();
    void ins_simplification();

    void add_constraint(map<string, vir_reg*>&);
    void ssa_deconstruct();

    void eval_cost();
    void allocate();

    void print_x86();
    void gvn();

  private:
    Basic_block* get_bb(instruction*);

    bool gen_lea();
    void cjmp2jmp();
    void cjmp2jcc();
    void convert_to_2op();
    void useless_lea();
    void useless_jmp();

    void rewrite_program(graph_coloring&);
};

class Basic_block: public dom_node
{
  private:
    instruction *_begin, *_end;
    set<vir_reg*> def, use;

  public:
    set<vir_reg*> phi;
    map<vir_reg*, list<vir_reg*> > phi_table;
    trie<int> *rename_table;
    trie<vir_reg*> *vn_table;

  public:
    void add_use(vir_reg*);
    void add_def(vir_reg*);
    void set_begin(instruction*);
    void set_end(instruction*);
    instruction* begin();
    instruction* end();
    void renaming(prog_with_ir*, trie<int>*);
    void mark_phi_v(prog_with_ir*);
    void print();
    void clean_flow_info();
    void init_inter_graph(graph_coloring&);
    void gvn(prog_with_ir*, trie<vir_reg*>*);
  
};

class prog_with_ir
{
  friend void ir_ins_call::sim();
  friend void Basic_block::mark_phi_v(prog_with_ir*);
  friend class ir;
  public:
    static string trans(const string&);

  private:
    list<ir*> func_list;
    trie<ir*> func_index;
    list<vir_reg*> reg_list;
    trie<vir_reg*> reg_index;
    vir_reg *string_format = NULL;
    vir_reg *int_format = NULL;
    vir_reg *buff = NULL;

  public:
    map<string, vir_reg*> pre_color_reg;

  public:
    trie<string> string_const;
    list<trie<int>*> rename_table;
    list<trie<vir_reg*>*> vn_table_list;

  public:
    prog_with_ir();
    ~prog_with_ir();
    ir* get_last_func();
    ir* get_first_func();

    void build_func(const string);
    void add_builtin_func();
    void move_init_ins();
    void init_label_set();
    void clean_flow_info();
    void remove_useless_call();

    vir_reg* build_reg(const string);
    vir_reg* find_reg(const string);
    void sim(bool = false);
    void print();

    void replace_global();
    void opt_inline(int);
    void convert_to_ssa();
    void liveness_analysis();

    void mark_phi_v();

    void constant_pro();
    void copy_pro();
    void ins_simplification();
    void gvn();
    void add_constraint();
    void ssa_deconstruct();
    void eval_cost();
    void allocate();
    void useless_jmp();

    void print_x86();

  private:
    void add_strcat();
    void add_strlst();
    void add_strlet();
    void add_strequ();
    void add_strneq();
    void add_getInt();
    void add_getString();
    void add_print();
    void add_println();
    void add_toString();
    void add_arr_size();
    void add_str_length();
    void add_str_substring();
    void add_str_parseInt();
    void add_str_ord();
};

#endif