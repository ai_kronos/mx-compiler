#ifndef _allocation_h
#define _allocation_h

#include<set>
#include<map>
#include<stack>
#include<list>
#include<cstdio>
#include<string>
#include"vir_reg.h"
#include"instruction.h"

using namespace std;

class graph_coloring
{
private:
    int K = 15;
    set<vir_reg*> precolored;
    set<vir_reg*> initial;

public:
    map<vir_reg*, set<ir_ins_mov*> > moveList;

    set<ir_ins_mov*> worklistMoves;
private:
    set<ir_ins_mov*> frozenMoves;
    set<ir_ins_mov*> activeMoves;

    set<ir_ins_mov*> coalescedMoves;
    set<ir_ins_mov*> constrainedMoves;

private:
    set<vir_reg*> simplifyWorklist;
    set<vir_reg*> freezeWorklist;
    set<vir_reg*> spillWorklist;

    set<vir_reg*> coalescedNodes;
    set<vir_reg*> coloredNodes;

    set<vir_reg*> selectStack_set;
    list<vir_reg*> selectStack;
    map<vir_reg*, vir_reg*> alias;


private:
    set<pair<vir_reg *, vir_reg *> > adjSet;
    map<vir_reg *, set<vir_reg*> > adjList;
    map<vir_reg *, int> degree;

public:
    set<vir_reg*> spilledNodes;


    void AddEdge(vir_reg*, vir_reg*);

    void add_precolored_node(vir_reg*);
    void add_initial_node(vir_reg*);

    void re_addin();

    void run();

private:
    void MakeWorklist();
    bool MoveRelated(vir_reg *);
    set<vir_reg*> Adjacent(vir_reg*);
    set<ir_ins_mov*> NodeMoves(vir_reg*);
    void Simplify();
    void DecrementDegree(vir_reg*);
    void EnableMoves(set<vir_reg*>);
    void AddWorkList(vir_reg*);
    bool OK(vir_reg*, vir_reg*);
    bool Conservative(set<vir_reg*>);
    void Coalesce();
    vir_reg *GetAlias(vir_reg*);
    void Combine(vir_reg*, vir_reg*);
    void Freeze();
    void FreezeMoves(vir_reg*);
    void SelectSpill();
    void AssignColors();

    bool judge(vir_reg*, vir_reg*);
};

#endif