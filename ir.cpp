#include "ir.h"
#include "tools.h"
#include "graph.h"
#include<algorithm>
#include<cstring>

extern int name_cnt;
extern int _debug;
extern bool x86_reg_out;
extern bool _sim;

void flow_node::add_out(flow_node* p)
{
    nxt.insert(p);
    p->pre.insert(this);
}

void flow_node::erase_out(flow_node* p)
{
    nxt.erase(p);
    p->pre.erase(this);
}

void flow_node::clean_edge()
{
    nxt.clear();
    pre.clear();
}

vir_reg::vir_reg(const string s):name(s), is_imm(0), is_global(0), del_flag(false), pre_color(false), color(NONE) {}
vir_reg::vir_reg(const string s, phyreg reg):name(s), is_imm(0), is_global(0), del_flag(false), pre_color(true), color(reg) {}
vir_reg::vir_reg(const int v):name(to_string(v)), imm(v), is_imm(1), val(v), is_global(0), del_flag(true) {}

bool vir_reg::is_interable()
{
    return !(is_global || frame_add != -1 || is_imm);
}

vir_reg *vir_reg::get_phi_coalesce_reg()
{
    if(phi_coalesce == this)
        return this;
    return phi_coalesce = phi_coalesce->get_phi_coalesce_reg();
}

void vir_reg::replace(vir_reg *p)
{
    set<instruction*>::iterator it = use.begin();
    for(; it != use.end(); )
    {
        instruction *ins = *it;
        ++it;
        ins->replace_reg(this, p);
    }
    for(it = def.begin(); it != def.end(); )
    {
        instruction *ins = *it;
        ++it;
        ins->replace_reg(this, p);
    }
}

void vir_reg::set_imme()
{
    imm = val;
    is_imm = true;
}

bool vir_reg::is_imme()
{
    return is_imm;
}

long long vir_reg::get_v()
{
    return val;
}

int vir_reg::get_use_size()
{
    return use.size() + (phi_v? 1 : 0);
}

vir_reg *vir_reg::clone(prog_with_ir *prog, int inx)
{
    if(is_imm)
    {
        return new vir_reg(imm);
    }
    if(is_global || is_str)
    {
        return this;
    }
    return prog->find_reg(name + "][" + to_string(inx) + "]");
}

void vir_reg::set_v(const long long v)
{
    val = v;
}

void vir_reg::set_def(instruction* ins)
{
    def.insert(ins);
}

void vir_reg::remove_def(instruction *ins)
{
    def.erase(ins);
    if(def.size() == 0 && use.size() == 0 && del_flag)
    {
        delete this;
    }
}

void vir_reg::set_use(instruction* ins)
{
    use.insert(ins);
}

void vir_reg::remove_use(instruction *ins)
{
    use.erase(ins);
    if(def.size() == 0 && use.size() == 0 && del_flag)
    {
        delete this;
    }
}

const set<instruction*> &vir_reg::get_use()
{
    return use;
}

const set<instruction*> &vir_reg::get_def()
{
    return def;
}

string vir_reg::get_name(bool is_x86)
{
    if(is_imm)
        return to_string(imm);
    if(is_x86)
        return color2reg(color);
    return name;
}

instruction::~instruction() {}
void instruction::sim() {}
void instruction::init_ins_flow() {}
void instruction::copy_pro() {}

void instruction::convert_to_2op() {}
void instruction::gvn(trie<vir_reg*> *t) {}

list<vir_reg*> instruction::get_use()
{
    list<vir_reg*> empty;
    return empty;
}

void instruction::clean_flow_info()
{
    _export.clear();
    pre.clear();
    nxt.clear();
    flag = false;
}

void instruction::eval_cost(double n)
{
    loop_cnt = n;
}

vir_reg *instruction::constant_pro()
{
    return NULL;
}

vir_reg *instruction::get_def()
{
    return NULL;
}

void instruction::print()
{
    if(label.size())
        printf("%s:\n", label.c_str());
}

bool instruction::def_reg(vir_reg *p)
{
    return false;
}

bool instruction::use_reg(vir_reg *p)
{
    return false;
}

bool instruction::is_call()
{
    return false;
}

void instruction::set_bb(Basic_block *p)
{
    bb = p;
}

instruction *instruction::clone(prog_with_ir *prog, int inx)
{
    instruction *tmp = new instruction(*this);
    if(label.size() > 0)
        tmp->label = label + "[" + to_string(inx) + "]";
    return tmp;
}

Basic_block *instruction::get_bb()
{
    return bb;
}

void instruction::print_x86()
{
    if(label.size())
        printf("%s: ;%s\n", get_label_name(label).c_str(), label.c_str());
}

ir_ins_lea::ir_ins_lea(vir_reg* t):base(NULL), index(NULL), target(t), offset(0), scale(0) 
{
    type = IR_INS_LEA;
    t->set_def(this);
}
ir_ins_lea::~ir_ins_lea()
{
    if(base != NULL)
        base->remove_use(this);
    if(index != NULL)
        index->remove_use(this);

    target->remove_def(this);
}

vir_reg *ir_ins_lea::get_def()
{
    return target;
}
list<vir_reg*> ir_ins_lea::get_use()
{
    list<vir_reg*> empty;
    if(base != NULL)
        empty.push_back(base);
    if(index != NULL)
        empty.push_back(index);
    return empty;
}
void ir_ins_lea::print_x86()
{
    printf("\tlea\t%s, [%s+%s*%d+%d]\n", 
        target->get_name(1).c_str(),
        base? base->get_name(1).c_str() : "0",
        index? index->get_name(1).c_str() : "0",
        scale,
        offset
    );
}
void ir_ins_lea::set_base(vir_reg *p)
{
    base = p;
    p->set_use(this);
}
bool ir_ins_lea::def_reg(vir_reg *p)
{
    return target == p;
}

void ir_ins_lea::eval_cost(double n)
{
    target->eval += n;
    if(base != NULL)
        base->eval += n;
    if(index != NULL)
        index->eval += n;
}

void ir_ins_lea::set_index(vir_reg *p)
{
    index = p;
    p->set_use(this);
}

void ir_ins_lea::sim()
{
    long long v = 0;
    if(base != NULL)
        v += base->get_v();
    if(index != NULL)
        v += index->get_v() * scale;
    v += offset;
    target->set_v(v);
    if(_debug)
    {
        printf("[debug] %lld  = %lld + %lld * %lld + %lld\n",
        v,
        base? base->get_v() : 0,
        index? index->get_v() : 0,
        scale,
        offset
        );
    }
}

void ir_ins_lea::print()
{
    printf("\t\tlea %s = %s + %s * %d + %d\n", 
        target->get_name(x86_reg_out).c_str(),
        base? base->get_name(x86_reg_out).c_str() : "NULL",
        index? index->get_name(x86_reg_out).c_str() : "NULL",
        scale,
        offset
    );
}

void ir_ins_lea::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(target == _old)
    {
        target->remove_def(this);
        target = _new;
        target->set_def(this);
    }
    if(base == _old)
    {
        base->remove_use(this);
        base = _new;
        base->set_use(this);
    }
    if(index == _old)
    {
        index->remove_use(this);
        index = _new;
        index->set_use(this);
    }
}

ir_ins_jequ::ir_ins_jequ(vir_reg* p0, vir_reg *p1, string s): op0(p0), op1(p1), label(s)
{
    type = IR_INS_JEQU;
    op0->set_use(this);
    op1->set_use(this);
}
ir_ins_jequ::~ir_ins_jequ()
{
    op0->remove_use(this);
    op1->remove_use(this);
}
void ir_ins_jequ::sim()
{
    if(op0->get_v() == op1->get_v())
    {
        func->jmp = true;
        func->name_flag = label;
    }
}
void ir_ins_jequ::print()
{
    printf("\t\tjequ %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), label.c_str());
}
void ir_ins_jequ::print_x86()
{ 
    if(op0->is_imme())
        printf("\tcmp\t %s, %s\n", op1->get_name(1).c_str(), op0->get_name(1).c_str());
    else
        printf("\tcmp\t %s, %s\n", op0->get_name(1).c_str(), op1->get_name(1).c_str());
    printf("\tje\t %s\n", get_label_name(label).c_str());
}
void ir_ins_jequ::init_ins_flow()
{
    func->cjmp = true;
    func->name_flag = label;
}
void ir_ins_jequ::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_use(this);
        op0 = _new;
        op0->set_use(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
}
list<vir_reg*> ir_ins_jequ::get_use()
{
    list<vir_reg*> empty;
    empty.push_back(op1);
    empty.push_back(op0);
    return empty;
}
void ir_ins_jequ::eval_cost(double n)
{
    op0->eval += n;
    op1->eval += n;
}

ir_ins_jneq::ir_ins_jneq(vir_reg* p0, vir_reg *p1, string s): op0(p0), op1(p1), label(s)
{
    type = IR_INS_JNEQ;
    op0->set_use(this);
    op1->set_use(this);
}
ir_ins_jneq::~ir_ins_jneq()
{
    op0->remove_use(this);
    op1->remove_use(this);
}
void ir_ins_jneq::sim()
{
    if(op0->get_v() != op1->get_v())
    {
        func->jmp = true;
        func->name_flag = label;
    }
}
void ir_ins_jneq::print()
{
    printf("\t\tjneq %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), label.c_str());
}
void ir_ins_jneq::print_x86()
{ 
    if(op0->is_imme())
        printf("\tcmp\t %s, %s\n", op1->get_name(1).c_str(), op0->get_name(1).c_str());
    else
        printf("\tcmp\t %s, %s\n", op0->get_name(1).c_str(), op1->get_name(1).c_str());
    printf("\tjne\t %s\n", get_label_name(label).c_str());
}
void ir_ins_jneq::init_ins_flow()
{
    func->cjmp = true;
    func->name_flag = label;
}
void ir_ins_jneq::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_use(this);
        op0 = _new;
        op0->set_use(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
}
list<vir_reg*> ir_ins_jneq::get_use()
{
    list<vir_reg*> empty;
    empty.push_back(op1);
    empty.push_back(op0);
    return empty;
}
void ir_ins_jneq::eval_cost(double n)
{
    op0->eval += n;
    op1->eval += n;
}

ir_ins_jlst::ir_ins_jlst(vir_reg* p0, vir_reg *p1, string s): op0(p0), op1(p1), label(s)
{
    type = IR_INS_JLST;
    op0->set_use(this);
    op1->set_use(this);
}
ir_ins_jlst::~ir_ins_jlst()
{
    op0->remove_use(this);
    op1->remove_use(this);
}
void ir_ins_jlst::sim()
{
    if(op0->get_v() < op1->get_v())
    {
        func->jmp = true;
        func->name_flag = label;
    }
}
void ir_ins_jlst::print()
{
    printf("\t\tjlst %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), label.c_str());
}
void ir_ins_jlst::print_x86()
{ 
    if(!op0->is_imme())
    {
        printf("\tcmp\t %s, %s\n", op0->get_name(1).c_str(), op1->get_name(1).c_str());
        printf("\tjl\t %s\n", get_label_name(label).c_str());
    }
    else
    {
        printf("\tcmp\t %s, %s\n", op1->get_name(1).c_str(), op0->get_name(1).c_str());
        printf("\tjg\t %s\n", get_label_name(label).c_str());
    }
    
}
void ir_ins_jlst::init_ins_flow()
{
    func->cjmp = true;
    func->name_flag = label;
}
void ir_ins_jlst::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_use(this);
        op0 = _new;
        op0->set_use(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
}
list<vir_reg*> ir_ins_jlst::get_use()
{
    list<vir_reg*> empty;
    empty.push_back(op1);
    empty.push_back(op0);
    return empty;
}
void ir_ins_jlst::eval_cost(double n)
{
    op0->eval += n;
    op1->eval += n;
}

ir_ins_jlet::ir_ins_jlet(vir_reg* p0, vir_reg *p1, string s): op0(p0), op1(p1), label(s)
{
    type = IR_INS_JLET;
    op0->set_use(this);
    op1->set_use(this);
}
ir_ins_jlet::~ir_ins_jlet()
{
    op0->remove_use(this);
    op1->remove_use(this);
}
void ir_ins_jlet::sim()
{
    if(op0->get_v() <= op1->get_v())
    {
        func->jmp = true;
        func->name_flag = label;
    }
}
void ir_ins_jlet::print()
{
    printf("\t\tjlet %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), label.c_str());
}
void ir_ins_jlet::print_x86()
{ 
    if(!op0->is_imme())
    {
        printf("\tcmp\t %s, %s\n", op0->get_name(1).c_str(), op1->get_name(1).c_str());
        printf("\tjle\t %s\n", get_label_name(label).c_str());
    }
    else
    {
        printf("\tcmp\t %s, %s\n", op1->get_name(1).c_str(), op0->get_name(1).c_str());
        printf("\tjge\t %s\n", get_label_name(label).c_str());
    }
}
void ir_ins_jlet::init_ins_flow()
{
    func->cjmp = true;
    func->name_flag = label;
}
void ir_ins_jlet::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_use(this);
        op0 = _new;
        op0->set_use(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
}
list<vir_reg*> ir_ins_jlet::get_use()
{
    list<vir_reg*> empty;
    empty.push_back(op1);
    empty.push_back(op0);
    return empty;
}
void ir_ins_jlet::eval_cost(double n)
{
    op0->eval += n;
    op1->eval += n;
}

ir_ins_thr_op::ir_ins_thr_op(vir_reg *p0, vir_reg *p1, vir_reg *p2): op0(p0), op1(p1), op2(p2)
{
    op0->set_def(this);
    op1->set_use(this);
    op2->set_use(this);
}
ir_ins_thr_op::~ir_ins_thr_op()
{
    op0->remove_def(this);
    op1->remove_use(this);
    op2->remove_use(this);
}

void ir_ins_thr_op::convert_to_2op()
{
    if(op0 != op1)
    {
        instruction *nins = new ir_ins_mov(op0, op1);
        func->insert_ins_in_bb(it, nins);
        replace_reg(op1, op0);
    }
}

vir_reg *ir_ins_thr_op::get_def()
{
    return op0;
}

list<vir_reg*> ir_ins_thr_op::get_use()
{
    list<vir_reg*> empty;
    empty.push_back(op1);
    empty.push_back(op2);
    return empty;
}

void ir_ins_thr_op::eval_cost(double n)
{
    op0->eval += n;
    op1->eval += n;
    op2->eval += n;
}
vir_reg *ir_ins_thr_op::constant_pro()
{
    if(op1->is_imme() && op2->is_imme())
    {
        if(op2->get_v() == 0 && (type == IR_INS_DIV || type == IR_INS_MOD))
        {
            self_remove();
            return NULL;
        }
        sim();
        op0->set_imme();
        vir_reg *ret_val = op0;
        if(op0->get_use().size() == 0 && op0->get_def().size() == 1)
            ret_val = NULL;
//        if(!op0->phi_v)
            self_remove();
        return ret_val;
    }
    return NULL;
}

ir_ins_glb::ir_ins_glb(vir_reg *p0):op(p0), is_string(0) 
{
    op->is_global = 1;
    type = IR_INS_GLB;
}
ir_ins_glb::ir_ins_glb(vir_reg *p0, const string s):op(p0), v(s), is_string(1)
{
    op->is_global = 1;
    op->is_str = 1;
    type = IR_INS_GLB;
}
ir_ins_glb::~ir_ins_glb() {}

void ir_ins_glb::print_x86()
{
    printf("%s:\n", get_label_name(op->get_name()).c_str());
    if(is_string)
    {
        printf("\tdb ");
        int siz = v.length();
        for(int i = 0; i < siz; ++i)
            printf("%d, ", v[i]);
        printf("00H\n\n");
    }
    else
    {
        printf("\tdq 00H\n\n");
    }
}

ir_ins_mov::ir_ins_mov(vir_reg *p0, vir_reg *p1, bool sx):op0(p0), op1(p1), is_sx(sx)
{
    type = IR_INS_MOV;
    op0->set_def(this);
    op1->set_use(this);
}
instruction *ir_ins_mov::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx);
    return new ir_ins_mov(nop0, nop1);
}
ir_ins_mov::~ir_ins_mov()
{
    op0->remove_def(this);
    op1->remove_use(this);
}
void ir_ins_mov::eval_cost(double n)
{
    op0->eval += n;
    op1->eval += n;
}
vir_reg *ir_ins_mov::get_def()
{
    return op0;
}
list<vir_reg*> ir_ins_mov::get_use()
{
    list<vir_reg*> empty;
    empty.push_back(op1);
    return empty;
}
void ir_ins_mov::print_x86()
{
    if(is_sx)
    {
        printf("\tmovsx\t%s ,%s\n", op0->get_name(1).c_str(), qword2dword(op1->get_name(1)).c_str());
        return;
    }
    if(op0->color == op1->color)
        return;
    if(op1->is_global)
        printf("\tlea\t%s, [%s]\n", op0->get_name(1).c_str(), get_label_name(op1->get_name()).c_str());
    else
        printf("\tmov\t%s ,%s\n", op0->get_name(1).c_str(), op1->get_name(1).c_str());
}

ir_ins_load::ir_ins_load(vir_reg *p0, vir_reg *p1, bool is_sx) : op0(p0), op1(p1), sx(is_sx)
{
    type = IR_INS_LOAD;
    op0->set_def(this);
    op1->set_use(this);
}
instruction *ir_ins_load::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx);
    return new ir_ins_load(nop0, nop1, sx);
}
ir_ins_load::~ir_ins_load()
{
    op0->remove_def(this);
    if(op1)
        op1->remove_use(this);
    if(base)
        base->remove_use(this);
    if(index)
        index->remove_use(this);
}
void ir_ins_load::merge_lea(ir_ins_lea *ins)
{
    op1->remove_use(this);
    op1 = NULL;
    base = ins->base;
    index = ins->index;
    scale = ins->scale;
    offset = ins->offset;
    if(base)
        base->set_use(this);
    if(index)
        index->set_use(this);
    ins->self_remove();
}
void ir_ins_load::print_x86()
{
    if(op1 != NULL && op1->is_imme())
        printf("\tmov\t%s, qword [rsp+%d]\n", op0->get_name(1).c_str(), (op1->get_v() + func->frame_cnt + 6) * 8);
    else if(sx)
    {
        string s1;
        if(op1 == NULL)
        {
            s1 = "[" +
                (base? (base->is_global? get_label_name(base->get_name()) : base->get_name(1)) : "0") + "+" +
                (index? index->get_name(1) : "0") + "*" +
                to_string(scale) + "+" +
                to_string(offset) + "]";
        }
        else
            s1 = "[" + op1->get_name(1) + "]";
        
        printf("\tmovsx\t%s ,byte %s\n", op0->get_name(1).c_str(), s1.c_str());
    }
    else if(op1 != NULL && op1->is_global)
        printf("\tmov\t%s ,qword [%s]\n", op0->get_name(1).c_str(), get_label_name(op1->get_name()).c_str());
    else
    {
        string s0, s1;

        if(op0->frame_add != -1)
            s0 = "qword [rsp+" + to_string(op0->frame_add * 8) +"]";
        else
            s0 = op0->get_name(1);

        if(op1 == NULL)
        {
            s1 = "qword [" +
                (base? (base->is_global? get_label_name(base->get_name()) : base->get_name(1)) : "0") + "+" +
                (index? index->get_name(1) : "0") + "*" +
                to_string(scale) + "+" +
                to_string(offset) + "]";
        }
        else if(op1->frame_add != -1)
            s1 = "qword [rsp+" + to_string(op1->frame_add * 8) +"]";
        else
            s1 = "qword [" + op1->get_name(1) + "]";
        
        printf("\tmov\t%s, %s\n", s0.c_str(), s1.c_str());
        /*
        if(op1->frame_add != -1)
            printf("\tmov\t%s ,qword [rsp+%d]\n", op0->get_name(1).c_str(), op1->frame_add * 8);
        else
            printf("\tmov\t%s ,qword [%s]\n", op0->get_name(1).c_str(), op1->get_name(1).c_str());
        */
    }
}
vir_reg *ir_ins_load::get_def()
{
    return op0;
}
list<vir_reg*> ir_ins_load::get_use()
{
    list<vir_reg*> empty;
    if(op1)
        empty.push_back(op1);
    else
    {
        if(base)
            empty.push_back(base);
        if(index)
            empty.push_back(index);
    }
    
    return empty;
}
void ir_ins_load::eval_cost(double n)
{
    op0->eval += n;
    if(op1)
        op1->eval += n;
    else
    {
        if(base)
            base->eval += n;
        if(index)
            index->eval += n;
    }
    
}

ir_ins_store::ir_ins_store(vir_reg *p0, vir_reg *p1) : op0(p0), op1(p1)
{
    type = IR_INS_STORE;
    op0->set_use(this);
    op1->set_use(this);
}
instruction *ir_ins_store::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx);
    return new ir_ins_store(nop0, nop1);
}
ir_ins_store::~ir_ins_store()
{
    if(base)
        base->remove_use(this);
    if(index)
        index->remove_use(this);
    if(op0)
        op0->remove_use(this);
    op1->remove_use(this);
}
void ir_ins_store::merge_lea(ir_ins_lea *ins)
{
    op0->remove_use(this);
    op0 = NULL;
    base = ins->base;
    index = ins->index;
    scale = ins->scale;
    offset = ins->offset;
    if(base)
        base->set_use(this);
    if(index)
        index->set_use(this);
    ins->self_remove();
}
list<vir_reg*> ir_ins_store::get_use()
{
    list<vir_reg*> empty;
    if(op0)
        empty.push_back(op0);
    else
    {
        if(base)
            empty.push_back(base);
        if(index)
            empty.push_back(index);
    }
    
    empty.push_back(op1);
    return empty;
}
void ir_ins_store::eval_cost(double n)
{
    if(op0 != NULL)
        op0->eval += n;
    else
    {
        if(base)
            base->eval += n;
        if(index)
            index->eval += n;
    }
    
    op1->eval += n;
}
void ir_ins_store::print_x86()
{
    if(op0 != NULL && op0->is_imme())
        printf("\tpush\t%s\n", op1->get_name(1).c_str());
    else if(op0 != NULL && op0->is_global)
        printf("\tmov\tqword [%s] ,%s\n", get_label_name(op0->get_name()).c_str(), op1->get_name(1).c_str());
    else
    {
        string s0, s1;

        if(op0 == NULL)
        {
            s0 = "qword [" +
                (base? (base->is_global? get_label_name(base->get_name()) : base->get_name(1)) : "0") + "+" +
                (index? index->get_name(1) : "0") + "*" +
                to_string(scale) + "+" +
                to_string(offset) + "]";
        }
        else if(op0->frame_add != -1)
            s0 = "qword [rsp+" + to_string(op0->frame_add * 8) +"]";
        else
            s0 = "qword [" + op0->get_name(1) + "]";

        if(op1->frame_add != -1)
            s1 = "qword [rsp+" + to_string(op1->frame_add * 8) +"]";
        else
            s1 = op1->get_name(1);
        
        printf("\tmov\t%s, %s\n", s0.c_str(), s1.c_str());
        /*
        if(op0->frame_add != -1)
            printf("\tmov\tqword [rsp+%d] ,%s\n", op0->frame_add * 8, op1->get_name(1).c_str());
        else
            printf("\tmov\tqword [%s] ,%s\n", op0->get_name(1).c_str(), op1->get_name(1).c_str());
        */
    }
} 

ir_ins_or::ir_ins_or(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_OR;}
ir_ins_or::~ir_ins_or() {}
instruction *ir_ins_or::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_or(nop0, nop1, nop2);
}
ir_ins_and::ir_ins_and(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_AND;}
ir_ins_and::~ir_ins_and() {}
instruction *ir_ins_and::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_and(nop0, nop1, nop2);
}
ir_ins_xor::ir_ins_xor(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_XOR;}
instruction *ir_ins_xor::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_xor(nop0, nop1, nop2);
}
ir_ins_xor::~ir_ins_xor() {}
ir_ins_equ::ir_ins_equ(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_EQU;}
ir_ins_equ::~ir_ins_equ() {}
instruction *ir_ins_equ::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_equ(nop0, nop1, nop2);
}
ir_ins_neq::ir_ins_neq(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_NEQ;}
ir_ins_neq::~ir_ins_neq() {}
instruction *ir_ins_neq::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_neq(nop0, nop1, nop2);
}
ir_ins_lst::ir_ins_lst(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_LST;}
ir_ins_lst::~ir_ins_lst() {}
instruction *ir_ins_lst::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_lst(nop0, nop1, nop2);
}
ir_ins_let::ir_ins_let(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_LET;}
ir_ins_let::~ir_ins_let() {}
instruction *ir_ins_let::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_let(nop0, nop1, nop2);
}
ir_ins_shl::ir_ins_shl(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_SHL;}
ir_ins_shl::~ir_ins_shl() {}
instruction *ir_ins_shl::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_shl(nop0, nop1, nop2);
}
ir_ins_shr::ir_ins_shr(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_SHR;}
ir_ins_shr::~ir_ins_shr() {}
instruction *ir_ins_shr::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_shr(nop0, nop1, nop2);
}
ir_ins_add::ir_ins_add(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_ADD;}
ir_ins_add::~ir_ins_add() {}
instruction *ir_ins_add::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_add(nop0, nop1, nop2);
}
ir_ins_sub::ir_ins_sub(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_SUB;}
ir_ins_sub::~ir_ins_sub() {}
instruction *ir_ins_sub::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_sub(nop0, nop1, nop2);
}
ir_ins_mul::ir_ins_mul(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_MUL;}
ir_ins_mul::~ir_ins_mul() {}
instruction *ir_ins_mul::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_mul(nop0, nop1, nop2);
}
ir_ins_div::ir_ins_div(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_DIV;}
ir_ins_div::~ir_ins_div() {}
instruction *ir_ins_div::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_div(nop0, nop1, nop2);
}
ir_ins_mod::ir_ins_mod(vir_reg *p0, vir_reg *p1, vir_reg *p2):ir_ins_thr_op(p0, p1, p2) {type = IR_INS_MOD;}
ir_ins_mod::~ir_ins_mod() {}
instruction *ir_ins_mod::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx), *nop2 = op2->clone(prog, inx);
    return new ir_ins_mod(nop0, nop1, nop2);
}

void ir_ins_equ::convert_to_2op() {}
void ir_ins_neq::convert_to_2op() {}
void ir_ins_lst::convert_to_2op() {}
void ir_ins_let::convert_to_2op() {}

ir_ins_call::ir_ins_call(const string s, vir_reg *v) : name(s), re(v)
{
    type = IR_INS_CALL;
    re->set_def(this);
}
ir_ins_call::ir_ins_call(const string s) : name(s), re(NULL) 
{
    type = IR_INS_CALL;
}
ir_ins_call::~ir_ins_call()
{
    if (re != NULL)
        re->remove_def(this);
    for(auto reg: para_list)
        reg->remove_use(this);
}
const list<vir_reg*> ir_ins_call::get_para_list()
{
    return para_list;
}
list<vir_reg*> ir_ins_call::get_use()
{
    return para_list;
}
vir_reg *ir_ins_call::get_def()
{
    return re;
}
instruction *ir_ins_call::clone(prog_with_ir *prog, int inx)
{
    ir_ins_call *ret;
    if(re == NULL)
        ret = new ir_ins_call(name);
    else
    {
        vir_reg *nre = re->clone(prog, inx);
        ret = new ir_ins_call(name, nre);
    }

    for(auto para:para_list)
    {
        vir_reg *npara = para->clone(prog, inx);
        ret->append_para(npara);
    }
    return ret;
}

ir_ins_not::ir_ins_not(vir_reg *p0, vir_reg *p1) : op0(p0), op1(p1)
{
    type = IR_INS_NOT;
    op0->set_def(this);
    op1->set_use(this);
}
instruction *ir_ins_not::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx);
    return new ir_ins_not(nop0, nop1);
}
ir_ins_not::~ir_ins_not()
{
    op0->remove_def(this);
    op1->remove_use(this);
}
vir_reg *ir_ins_not::get_def()
{
    return op0;
}
list<vir_reg*> ir_ins_not::get_use()
{
    list<vir_reg*> empty;
    empty.push_back(op1);
    return empty;
}
void ir_ins_not::convert_to_2op()
{
    if(op0 != op1)
    {
        instruction *nins = new ir_ins_mov(op0, op1);
        func->insert_ins_in_bb(it, nins);
        replace_reg(op1, op0);
    }
}

ir_ins_neg::ir_ins_neg(vir_reg *p0, vir_reg *p1) : op0(p0), op1(p1)
{
    type = IR_INS_NEG;
    op0->set_def(this);
    op1->set_use(this);
}
instruction *ir_ins_neg::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop0 = op0->clone(prog, inx), *nop1 = op1->clone(prog, inx);
    return new ir_ins_neg(nop0, nop1);
}
ir_ins_neg::~ir_ins_neg()
{
    op0->remove_def(this);
    op1->remove_use(this);
}
vir_reg *ir_ins_neg::get_def()
{
    return op0;
}
list<vir_reg*> ir_ins_neg::get_use()
{
    list<vir_reg*> empty;
    empty.push_back(op1);
    return empty;
}
void ir_ins_neg::convert_to_2op()
{
    if(op0 != op1)
    {
        instruction *nins = new ir_ins_mov(op0, op1);
        func->insert_ins_in_bb(it, nins);
        replace_reg(op1, op0);
    }
}

ir_ins_jmp::ir_ins_jmp(const string s): label(s) {type = IR_INS_JMP;}
instruction *ir_ins_jmp::clone(prog_with_ir *prog, int inx)
{
    return new ir_ins_jmp(label + "[" + to_string(inx) + "]");
}
ir_ins_jmp::~ir_ins_jmp() {}

ir_ins_cjmp::ir_ins_cjmp(const string s, vir_reg *v, bool t) : label(s), op(v), taken(t)
{
    type = IR_INS_CJMP;
    op->set_use(this);
}
instruction *ir_ins_cjmp::clone(prog_with_ir *prog, int inx)
{
    vir_reg *nop = op->clone(prog, inx);
    return new ir_ins_cjmp(label + "[" + to_string(inx) + "]", nop, taken);
}
ir_ins_cjmp::~ir_ins_cjmp()
{
    op->remove_use(this);
}
list<vir_reg*> ir_ins_cjmp::get_use()
{
    list<vir_reg*> empty;
    empty.push_back(op);
    return empty;
}

ir_ins_ret::ir_ins_ret(vir_reg *v): op(v)
{
    type = IR_INS_RET;
    op->set_use(this);
}
ir_ins_ret::ir_ins_ret(): op(NULL) { type = IR_INS_RET; }
instruction *ir_ins_ret::clone(prog_with_ir *prog, int inx)
{
    if(op == NULL)
        return new ir_ins_ret();
    else
    {
        vir_reg *nop;
        string op_name = op->get_name() + "][" + to_string(inx) + "]";
        nop = prog->find_reg(op_name);
        return new ir_ins_ret(nop);
    }
}
ir_ins_ret::~ir_ins_ret()
{
    if(op != NULL)
        op->remove_use(this);
}
list<vir_reg*> ir_ins_ret::get_use()
{
    list<vir_reg*> empty;
    if (op != NULL)
        empty.push_back(op);
    return empty;
}

extern void *mem;

void ir_ins_glb::sim()
{
    if(is_string)
    {
        int len = v.length();
        char *s = (char*)mem;
        mem = mem + (len + 1);
        for(int i = 0; i < len; ++i)
            s[i] = v[i];
        s[len] = 0;
        op->set_v((long long)s);
    }
    else
    {
        void *p = mem;
        mem = mem + 8;
        op->set_v((long long)p);
    }
    
}

void ir_ins_glb::print()
{
    printf("\t\t.global %s", op->get_name().c_str());
    if(is_string)
        printf(" %s", v.c_str());
    printf("\n");
}

void ir_ins_mov::sim()
{
    op0->set_v(op1->get_v());
    if(_debug)
        printf("[debug] %lld %lld\n", op0->get_v(), op1->get_v());
}

void ir_ins_mov::copy_pro()
{
    if(op1->is_imme())
        return;
    set<instruction*>::iterator ite = op0->get_use().begin();
    for(;ite != op0->get_use().end();)
    {
        instruction *ins = *ite;
        ++ite;
        ins->replace_reg(op0, op1);
    }
    op0->phi_coalesce = op1;
    op1->phi_v |= op0->phi_v;
//    if(op0->phi_v)
//        op0->phi_coalesce = op1;
    self_remove();
}

vir_reg *ir_ins_mov::constant_pro()
{
    if(op1->is_imme())
    {
        sim();
        op0->set_imme();
        vir_reg *ret_val = op0;
        if(op0->get_use().size() == 0 && op0->get_def().size() == 1)
            ret_val = NULL;
//        if(!op0->phi_v)
            self_remove();
        return ret_val;
    }
    return NULL;
}


void ir_ins_mov::print()
{
    printf("\t\tmov %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str());
}

bool ir_ins_mov::def_reg(vir_reg *p)
{
    return op0 == p;
}

bool ir_ins_mov::use_reg(vir_reg *p)
{
    return op1 == p;
}

void ir_ins_mov::set_bb(Basic_block *p)
{
    bb = p;
    p->add_def(op0);
    p->add_use(op1);
}

void ir_ins_load::sim()
{
    if(sx)
        op0->set_v(*((char*)op1->get_v()));
    else
        op0->set_v(*((long long*)op1->get_v()));
    if(_debug)
        printf("[debug] %lld %lld\n", op0->get_v(), op1->get_v());
}

void ir_ins_load::print()
{
    printf("\t\tload %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str());
}

bool ir_ins_load::def_reg(vir_reg *p)
{
    return op0 == p;
}

bool ir_ins_load::use_reg(vir_reg *p)
{
    return op1 == p;
}

void ir_ins_load::set_bb(Basic_block *p)
{
    bb = p;
    p->add_def(op0);
    p->add_use(op1);
}

void ir_ins_store::sim()
{
    *((long long*)op0->get_v()) = op1->get_v();
    if (_debug)
        printf("[debug] %lld %lld\n", op0->get_v(), op1->get_v());
}

void ir_ins_store::print()
{
    printf("\t\tstore %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str());
}

bool ir_ins_store::use_reg(vir_reg *p)
{
    return p == op1 || p == op0;
}

void ir_ins_store::set_bb(Basic_block *p)
{
    bb = p;
    p->add_use(op0);
    p->add_use(op1);
}

bool ir_ins_thr_op::use_reg(vir_reg *p)
{
    return op1 == p || op2 == p;
}

bool ir_ins_thr_op::def_reg(vir_reg *p)
{
    return op0 == p;
}

void ir_ins_thr_op::set_bb(Basic_block *p)
{
    bb = p;
    p->add_use(op2);
    p->add_use(op1);
    p->add_def(op0);
}

void ir_ins_or::sim()
{
    op0->set_v(op1->get_v() | op2->get_v());
}

void ir_ins_or::print()
{
    printf("\t\tor %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_or::print_x86()
{
    printf("\tor\t%s, %s\n", op0->get_name(1).c_str(), op2->get_name(1).c_str());
}

void ir_ins_and::sim()
{
    op0->set_v(op1->get_v() & op2->get_v());
}

void ir_ins_and::print()
{
    printf("\t\tand %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_and::print_x86()
{
    printf("\tand\t%s, %s\n", op0->get_name(1).c_str(), op2->get_name(1).c_str());
}

void ir_ins_xor::sim()
{
    op0->set_v(op1->get_v() ^ op2->get_v());
}

void ir_ins_xor::print()
{
    printf("\t\txor %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_xor::print_x86()
{
    printf("\txor\t%s, %s\n", op0->get_name(1).c_str(), op2->get_name(1).c_str());
}

void ir_ins_equ::sim()
{
    op0->set_v(op1->get_v() == op2->get_v());
}

void ir_ins_equ::print()
{
    printf("\t\tequ %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_equ::print_x86()
{
    if(op1->is_imme())
        printf("\tcmp\t %s, %s\n", op2->get_name(1).c_str(), op1->get_name(1).c_str());
    else
        printf("\tcmp\t %s, %s\n", op1->get_name(1).c_str(), op2->get_name(1).c_str());
    printf("\tsete\t%s\n", qword2byte(op0->get_name(1)).c_str());
    printf("\tmovsx\t%s, %s\n",op0->get_name(1).c_str() ,qword2byte(op0->get_name(1)).c_str());
}

void ir_ins_neq::sim()
{
    op0->set_v(op1->get_v() != op2->get_v());
}

void ir_ins_neq::print_x86()
{
    if(op1->is_imme())
        printf("\tcmp\t %s, %s\n", op2->get_name(1).c_str(), op1->get_name(1).c_str());
    else
        printf("\tcmp\t %s, %s\n", op1->get_name(1).c_str(), op2->get_name(1).c_str());
    printf("\tsetne\t%s\n", qword2byte(op0->get_name(1)).c_str());
    printf("\tmovsx\t%s, %s\n",op0->get_name(1).c_str() ,qword2byte(op0->get_name(1)).c_str());
}

void ir_ins_neq::print()
{
    printf("\t\tneq %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_lst::sim()
{
    op0->set_v(op1->get_v() < op2->get_v());
    if(_debug)
        printf("[debug] %lld %lld %lld\n", op0->get_v(), op1->get_v(), op2->get_v());
}

void ir_ins_lst::print()
{
    printf("\t\tlst %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_lst::print_x86()
{
    if(!op1->is_imme())
    {
        printf("\tcmp\t %s, %s\n", op1->get_name(1).c_str(), op2->get_name(1).c_str());
        printf("\tsetl\t %s\n", qword2byte(op0->get_name(1)).c_str());
    }
    else
    {
        printf("\tcmp\t %s, %s\n", op2->get_name(1).c_str(), op1->get_name(1).c_str());
        printf("\tsetg\t %s\n", qword2byte(op0->get_name(1)).c_str());
    }
    printf("\tmovsx\t%s, %s\n",op0->get_name(1).c_str() ,qword2byte(op0->get_name(1)).c_str());
}

void ir_ins_let::sim()
{
    op0->set_v(op1->get_v() <= op2->get_v());
}

void ir_ins_let::print()
{
    printf("\t\tlst %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_let::print_x86()
{
    if(!op1->is_imme())
    {
        printf("\tcmp\t %s, %s\n", op1->get_name(1).c_str(), op2->get_name(1).c_str());
        printf("\tsetle\t %s\n", qword2byte(op0->get_name(1)).c_str());
    }
    else
    {
        printf("\tcmp\t %s, %s\n", op2->get_name(1).c_str(), op1->get_name(1).c_str());
        printf("\tsetge\t %s\n", qword2byte(op0->get_name(1)).c_str());
    }
    printf("\tmovsx\t%s, %s\n",op0->get_name(1).c_str() ,qword2byte(op0->get_name(1)).c_str());
}

void ir_ins_shl::sim()
{
    op0->set_v(op1->get_v() << op2->get_v());
}

void ir_ins_shl::print()
{
    printf("\t\tshl %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_shl::print_x86()
{
    if(op2->is_imme())
        printf("\tsal\t %s, %d\n", op0->get_name(1).c_str(), op2->get_v());
    else
        printf("\tsal\t %s, cl\n", op0->get_name(1).c_str());
}

void ir_ins_shr::sim()
{
    op0->set_v(op1->get_v() >> op2->get_v());
}

void ir_ins_shr::print()
{
    printf("\t\tshl %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_shr::print_x86()
{
    if(op2->is_imme())
        printf("\tsar\t %s, %d\n", op0->get_name(1).c_str(), op2->get_v());
    else
        printf("\tsar\t %s, cl\n", op0->get_name(1).c_str());
}

void ir_ins_add::sim()
{
    op0->set_v(op1->get_v() + op2->get_v());
    if (_debug)
        printf("[debug] %lld %lld %lld\n", op0->get_v(), op1->get_v(), op2->get_v());
}

void ir_ins_add::gvn(trie<vir_reg*> *vn_table)
{
    if(vn_table->is_defined(op1->get_name()))
        replace_reg(op1, vn_table->find(op1->get_name()));
    if(vn_table->is_defined(op2->get_name()))
        replace_reg(op2, vn_table->find(op2->get_name()));
    string s = op1->get_name() + "[[add]]" + op2->get_name();
    if(vn_table->is_defined(s))
    {
        instruction *nins = new ir_ins_mov(op0, vn_table->find(s));
        func->insert_ins_in_bb(it, nins);
        vn_table->insert(op0->get_name(), vn_table->find(s));
        self_remove();
    }
    else
    {
        vn_table->insert(s, op0);
    }
}

void ir_ins_add::print()
{
    printf("\t\tadd %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_add::print_x86()
{
    printf("\tadd\t%s, %s\n", op0->get_name(1).c_str(), op2->get_name(1).c_str());
}

void ir_ins_sub::sim()
{
    op0->set_v(op1->get_v() - op2->get_v());
}

void ir_ins_sub::gvn(trie<vir_reg*> *vn_table)
{
    if(vn_table->is_defined(op1->get_name()))
        replace_reg(op1, vn_table->find(op1->get_name()));
    if(vn_table->is_defined(op2->get_name()))
        replace_reg(op2, vn_table->find(op2->get_name()));
    string s = op1->get_name() + "[[sub]]" + op2->get_name();
    if(vn_table->is_defined(s))
    {
        instruction *nins = new ir_ins_mov(op0, vn_table->find(s));
        func->insert_ins_in_bb(it, nins);
        vn_table->insert(op0->get_name(), vn_table->find(s));
        self_remove();
    }
    else
    {
        vn_table->insert(s, op0);
    }
}

void ir_ins_sub::print()
{
    printf("\t\tsub %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_sub::print_x86()
{
    printf("\tsub\t%s, %s\n", op0->get_name(1).c_str(), op2->get_name(1).c_str());
}

void ir_ins_mul::sim()
{
    op0->set_v(op1->get_v() * op2->get_v());
}

void ir_ins_mul::gvn(trie<vir_reg*> *vn_table)
{
    if(vn_table->is_defined(op1->get_name()))
        replace_reg(op1, vn_table->find(op1->get_name()));
    if(vn_table->is_defined(op2->get_name()))
        replace_reg(op2, vn_table->find(op2->get_name()));
    string s = op1->get_name() + "[[mul]]" + op2->get_name();
    if(vn_table->is_defined(s))
    {
        instruction *nins = new ir_ins_mov(op0, vn_table->find(s));
        func->insert_ins_in_bb(it, nins);
        vn_table->insert(op0->get_name(), vn_table->find(s));
        self_remove();
    }
    else
    {
        vn_table->insert(s, op0);
    }
}

void ir_ins_mul::print()
{
    printf("\t\tmul %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_mul::print_x86()
{
    printf("\timul\t%s, %s\n", op0->get_name(1).c_str(), op2->get_name(1).c_str());
}

void ir_ins_div::sim()
{
    op0->set_v(op1->get_v() / op2->get_v());
}

void ir_ins_div::gvn(trie<vir_reg*> *vn_table)
{
    if(vn_table->is_defined(op1->get_name()))
        replace_reg(op1, vn_table->find(op1->get_name()));
    if(vn_table->is_defined(op2->get_name()))
        replace_reg(op2, vn_table->find(op2->get_name()));
    string s = op1->get_name() + "[[div]]" + op2->get_name();
    if(vn_table->is_defined(s))
    {
        instruction *nins = new ir_ins_mov(op0, vn_table->find(s));
        func->insert_ins_in_bb(it, nins);
        vn_table->insert(op0->get_name(), vn_table->find(s));
        self_remove();
    }
    else
    {
        vn_table->insert(s, op0);
    }
}

void ir_ins_div::print()
{
    printf("\t\tdiv %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_div::print_x86()
{
    printf("\tcdq\n");
    printf("\tidiv\t%s\n", qword2dword(op2->get_name(1)).c_str());
}

void ir_ins_mod::sim()
{
    op0->set_v(op1->get_v() % op2->get_v());
    /*
    print();
    printf("[debug] %d = %d mod %d\n", op0->get_v(), op1->get_v(), op2->get_v());
    */
}

void ir_ins_mod::gvn(trie<vir_reg*> *vn_table)
{
    if(vn_table->is_defined(op1->get_name()))
        replace_reg(op1, vn_table->find(op1->get_name()));
    if(vn_table->is_defined(op2->get_name()))
        replace_reg(op2, vn_table->find(op2->get_name()));
    string s = op1->get_name() + "[[mod]]" + op2->get_name();
    if(vn_table->is_defined(s))
    {
        instruction *nins = new ir_ins_mov(op0, vn_table->find(s));
        func->insert_ins_in_bb(it, nins);
        vn_table->insert(op0->get_name(), vn_table->find(s));
        self_remove();
    }
    else
    {
        vn_table->insert(s, op0);
    }
}

void ir_ins_mod::print()
{
    printf("\t\tmod %s %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str(), op2->get_name(x86_reg_out).c_str());
}

void ir_ins_mod::print_x86()
{
    printf("\tcdq\n");
    printf("\tidiv\t%s\n", qword2dword(op2->get_name(1)).c_str());
}

void ir_ins_call::append_para(vir_reg *t)
{
    para_list.push_back(t);
    t->set_use(this);
}

string ir_ins_call::get_name()
{
    return name;
}

bool ir_ins_call::use_reg(vir_reg *p)
{
    list<vir_reg*>::iterator it = find(para_list.begin(), para_list.end(), p);
    if(it == para_list.end())
        return false;
    else
        return true;
}

bool ir_ins_call::def_reg(vir_reg *p)
{
    return p == re;
}

bool ir_ins_call::is_call()
{
    return true;
}

void ir_ins_call::set_bb(Basic_block* p)
{
    bb = p;
    if(re != NULL)
        bb->add_def(re);
    for(auto np:para_list)
        bb->add_use(np);
}

void ir_ins_call::sim()
{
    if(name == "]strcpy")
    {
        char *dest = (char*)(para_list.front()->get_v());
        char *src = (char*)(para_list.back()->get_v());
        strcpy(dest, src);
        return;
    }

    if(name == "]strcat")
    {
        char *dest = (char*)(para_list.front()->get_v());
        char *src = (char*)(para_list.back()->get_v());
        strcat(dest, src);
        return;
    }

    if(name == "]strcmp")
    {
        char *dest = (char*)(para_list.front()->get_v());
        char *src = (char*)(para_list.back()->get_v());
        int a = strcmp(dest, src);
        re->set_v(a);
        return;
    }

    if(name == "]strlen")
    {
        char *ad = (char*)(para_list.front()->get_v());
        int a;
        a = strlen(ad);
        re->set_v(a);
        return;
    }

    if(name == "]scanf")
    {
        char *ad1 = (char*)(para_list.front()->get_v());
        void *ad2 = (char*)(para_list.back()->get_v());
        scanf(ad1, ad2);
        return;
    }

    if(name == "]printf")
    {
        char *ad1 = (char*)(para_list.front()->get_v());
        void *ad2 = (char*)(para_list.back()->get_v());
        printf(ad1, ad2);
        return;
    }

    if(name == "]sprintf")
    {
        vir_reg *ad[3];
        int i = 0;
        for(auto reg:para_list)
            ad[i++] = reg;
        sprintf((char*)(ad[0]->get_v()), (char*)(ad[1]->get_v()), ad[2]->get_v());
        if(_debug)
        {
            printf("------\n");
            printf("[debug] %lld %lld %lld\n",ad[0]->get_v(), ad[1]->get_v(), ad[2]->get_v());
            char s[20];
            sprintf(s, "%lld", ad[2]->get_v());
            printf("%s\n", s);
            printf("------\n");
        }
        return;
    }

    if(name == "]puts")
    {
        char *ad = (char*)(para_list.front()->get_v());
        puts(ad);
        return;
    }

    if(name == "]malloc")
    {
        int a = para_list.front()->get_v();
        unsigned long long p = (unsigned long long)malloc(a);
        re->set_v(p);
        return;
    }

    if(name == "]memcpy")
    {
        vir_reg *ad[3];
        int i = 0;
        for(auto reg:para_list)
            ad[i++] = reg;
        memcpy((char*)(ad[0]->get_v()), (char*)(ad[1]->get_v()), ad[2]->get_v());
        return;
    }

    prog_with_ir *prog = func->prog;
    ir* fun = prog->func_index.find(name);
    int t_siz(para_list.size());
    if(t_siz != fun->para_list.size())
        throw("unmatched para");
    list<vir_reg*>::iterator it0(para_list.begin()), it1(fun->para_list.begin());
    list<long long> buff;

    for(auto p:para_list)
        buff.push_back(p->get_v());
    if (_debug)
        printf("[debug] ");
    for(auto p:fun->para_list)
    {
        p->set_v(buff.front());
        buff.pop_front();
        if (_debug)
            printf("%lld ", p->get_v());
    }
    if (_debug)
        printf("\n");

    func->ret_reg = re;
    func->call = true;
    func->name_flag = name;
}

void ir_ins_call::print()
{
    printf("\t\tcall %s", name.c_str());
    for(auto p:para_list)
        printf(" %s", p->get_name(x86_reg_out).c_str());
    printf("\n");
}

void ir_ins_call::print_x86()
{
    printf("\tcall\t%s ;%s\n", get_label_name(name).c_str(), name.c_str());
}

void ir_ins_not::sim()
{
    op0->set_v(~op1->get_v());
}

vir_reg* ir_ins_not::constant_pro()
{
    return NULL;
    if(op1->is_imme())
    {
        sim();
        op0->set_imme();
        vir_reg *ret_val = op0;
        if(op0->get_use().size() == 0 && op0->get_def().size() == 1)
            ret_val = NULL;
//        if(!op0->phi_v)
            self_remove();
        return ret_val;
    }
    return NULL;
}

void ir_ins_not::print()
{
    printf("\tnot\t%s, %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str());
}

bool ir_ins_not::use_reg(vir_reg *p)
{
    return p == op1;
}

bool ir_ins_not::def_reg(vir_reg *p)
{
    return p == op0;
}

void ir_ins_not::set_bb(Basic_block *p)
{
    bb = p;
    p->add_use(op1);
    p->add_def(op0);
}

void ir_ins_not::print_x86()
{
    printf("\txor\t%s, 1\n", op0->get_name(1).c_str());
}

void ir_ins_neg::sim()
{
    op0->set_v(-op1->get_v());
}

vir_reg* ir_ins_neg::constant_pro()
{
    return NULL;
    if(op1->is_imme())
    {
        sim();
        op0->set_imme();
        vir_reg *ret_val = op0;
        if(op0->get_use().size() == 0 && op0->get_def().size() == 1)
            ret_val = NULL;
//        if(!op0->phi_v)
            self_remove();
        return ret_val;
    }
    return NULL;
}

void ir_ins_neg::print()
{
    printf("\t\tneg %s %s\n", op0->get_name(x86_reg_out).c_str(), op1->get_name(x86_reg_out).c_str());
}

bool ir_ins_neg::use_reg(vir_reg *p)
{
    return p == op1;
}

bool ir_ins_neg::def_reg(vir_reg *p)
{
    return p == op0;
}

void ir_ins_neg::set_bb(Basic_block *p)
{
    bb = p;
    p->add_use(op1);
    p->add_def(op0);
}

void ir_ins_neg::print_x86()
{
    printf("\tneg\t%s\n", op0->get_name(1).c_str());
}

void ir_ins_jmp::sim()
{
    func->jmp = true;
    func->name_flag = label;
}

void ir_ins_jmp::print()
{
    printf("\t\tjmp %s\n", label.c_str());
}

void ir_ins_jmp::print_x86()
{
    printf("\tjmp\t%s\n", get_label_name(label).c_str());
}

bool ir_ins_cjmp::use_reg(vir_reg *p)
{
    return p == op;
}

void ir_ins_cjmp::set_bb(Basic_block *p)
{
    bb = p;
    p->add_use(op);
}

void ir_ins_cjmp::sim()
{
    if((op->get_v() && taken) || (op->get_v() == 0 && !taken))
    {
        func->jmp = true;
        func->name_flag = label;
    }
}

void ir_ins_cjmp::print()
{
    printf("\t\tcjmp %s %s\n", op->get_name(x86_reg_out).c_str(), label.c_str());
}

void ir_ins_cjmp::print_x86()
{
    if(taken)
        printf("\tcmp\t%s, 1\n", op->get_name(1).c_str());
    else
        printf("\tcmp\t%s, 0\n", op->get_name(1).c_str());
    printf("\tje\t%s\n", get_label_name(label).c_str());
}

bool ir_ins_ret::use_reg(vir_reg *p)
{
    return op == p;
}

void ir_ins_ret::sim()
{
    func->ret = true;
    if (op != NULL)
    {
        func->ret_val = op->get_v();
        if(_debug)
            printf("[debug] %lld\n", op->get_v());
    }
}

void ir_ins_ret::print()
{
    printf("\t\tret");
    if(op != NULL)
        printf("%s", op->get_name(x86_reg_out).c_str());
    printf("\n");
}

void ir_ins_ret::set_bb(Basic_block *p)
{
    bb = p;
    if (op != NULL)
        p->add_use(op);
}

string ir::trans(const string &str)
{
    string s;
    int siz = str.length();
    for(int i = 0; i < siz; ++i)
    {
        if('0' <= str[i] && str[i] <= '9')
            s.push_back(str[i] - 48);
        else if('a' <= str[i] && str[i] <= 'z')
            s.push_back(str[i] - 87);
        else if('A' <= str[i] && str[i] <= 'Z')
            s.push_back(str[i] - 29);
        else if(str[i] == '[')
            s.push_back(62);
        else if(str[i] == ']')
            s.push_back(63);
        else
            s.push_back(64);
    }
    return s;
}

void ir::convert_to_2op()
{
    list<instruction*>::iterator it = ins_list.begin();
    for(; it != ins_list.end(); ++it)
    {
        if((*it)->get_bb() == NULL)
            continue;
        (*it)->convert_to_2op();
    }
}

void ir::insert_ins_in_bb(list<instruction*>::iterator it, instruction *ins)
{
    ins_list.insert(it, ins);
    ins->func = this;
    ins->it = it;
    --ins->it;
    ins->set_bb((*it)->get_bb());

    instruction *top_ins = *it;
    if(ins->type == IR_INS_NULL)
        throw("Invalid insert.");
    instruction *pre_ins = (instruction*)(*(top_ins->pre.begin()));
    pre_ins->erase_out(top_ins);
    pre_ins->add_out(ins);

    cjmp = jmp = false;
    ins->init_ins_flow();
    if(!jmp)
        ins->add_out(*it);
    if(jmp || cjmp)
        ins->add_out(*(label_set.find(name_flag)));
}

void ir::append_inst(instruction *ins)
{
    ins_list.push_back(ins);
}

void ir::push_front_inst(instruction *ins)
{
    ins_list.push_front(ins);
}

void ir::append_para(vir_reg *v)
{
    para_list.push_back(v);
}

int tmp_cnt = 0;
int debug_cnt = 0;

long long ir::sim(bool ssa)
{
    list<long long> bak;

    iterator it = begin();
    instruction *last = NULL;
    vir_reg *bak0;
    while(it != end())
    {
        jmp = call = ret = false;

        if(ssa)
        {
            if(*it == (*it)->get_bb()->begin())
            {
                Basic_block *new_bb = (*it)->get_bb();
                if(last != NULL)
                {
                    Basic_block *old_bb = last->get_bb();
                    for(auto reg:new_bb->phi)
                    {
                        string s = cut_reg_name(reg->get_name());
                        int val = 0;
                        if(old_bb->rename_table->is_defined(s))
                            val = old_bb->rename_table->find(s);
                        vir_reg *old_reg = prog->reg_index.find(s + "[" + to_string(val) + "]");
                        old_reg = old_reg->get_phi_coalesce_reg();
                        reg->set_v(old_reg->get_v());
                    }
                }
            }
        }

        if((*it)->is_call())
        {
            tmp_cnt = 0;
            for(auto q:prog->reg_list)
            {
                bak.push_back(q->get_v());
            }
        }
        ++debug_cnt;
        /*
        if(ssa && debug_cnt > 3060300)
        {
            printf("%d ", debug_cnt);
            (*it)->print();
        }
        */
        if(_debug)
        {
            printf("debug_cnt: %lld\n", debug_cnt);
            (*it)->print();
        }
        (*it)->sim();
        bak0 = ret_reg;

        last = *it;

        if(jmp)
            it = label_set.find(name_flag);
        else
            ++it;

        if(call)
        {
            long long t = prog->func_index.find(name_flag)->sim(ssa);

            list<long long>::iterator it2 = bak.begin();

            ret = false;
            ret_reg = bak0;
            for(auto q:prog->reg_list)
            {
                q->set_v(*it2);
                ++it2;
            }

            ret_reg->set_v(t);
        }

        while(!bak.empty())
            bak.pop_back();
        
        if(ret)
            break;
    }
    return ret_val;
}

ir::ir(prog_with_ir *lir):cfg(NULL), label_set(65), prog(lir)
{
    label_set.bind_trans_func(trans);
}

ir::iterator::iterator() {}
ir::iterator::iterator(const iterator &it):p(it.p), ins_list(it.ins_list) {}
ir::iterator::iterator(list<instruction*>::iterator ob, list<instruction *> *li):p(ob), ins_list(li) {}

ir::iterator& ir::iterator::operator ++()
{
    ++p;
    return *this;
}

ir::iterator& ir::iterator::operator --()
{
    --p;
    return *this;
}

instruction* ir::iterator::operator *()
{
    return *p;
}

bool ir::iterator::operator ==(const ir::iterator& ob)
{
    return p == ob.p;
}

bool ir::iterator::operator !=(const ir::iterator& ob)
{
    return p != ob.p;
}

ir::iterator ir::begin()
{
    return iterator(ins_list.begin(), &ins_list);
}

ir::iterator ir::end()
{
    return iterator(ins_list.end(), &ins_list);
}

void ir::print()
{
    printf("%s\n", label.c_str());
    for(auto c:ins_list)
    {
        if(c->get_bb() != NULL && c->get_bb()->begin() == c)
        {
            printf("----------phi-----------\n");
            for(auto reg: c->get_bb()->phi)
                printf("%s ", reg->get_name().c_str());
            printf("\n--\n");
        }
        c->print();
        /*
        printf("{");
        for(auto reg: c->_export)
            printf("%s ", reg->get_name(x86_reg_out).c_str());
        printf("} %d %d\n", c->pre.size(), c->nxt.size());
        */
    }
}

void ir::print_x86()
{
    if(frame_cnt % 2 == 0)
        ++frame_cnt; 
    if(label != "[static]")
        printf("%s: ;(%s)\n", get_label_name(label).c_str(), label.c_str());
    if(label != "main" && label != "[static]")
    {
        printf("\tpush\trbx\n");
        printf("\tpush\trbp\n");
        printf("\tpush\tr12\n");
        printf("\tpush\tr13\n");
        printf("\tpush\tr14\n");
        printf("\tpush\tr15\n");
    }
    if(label != "[static]")
        printf("\tsub\trsp, %d\n", 8 * frame_cnt);
    for(auto c:ins_list)
    {
        if(c->get_bb() == NULL)
            continue;
        c->print_x86();
    }
    if(label != "[static]")
        printf("\tadd\trsp, %d\n", 8 * frame_cnt);
    if(label != "main" && label != "[static]")
    {
        printf("\tpop\tr15\n");
        printf("\tpop\tr14\n");
        printf("\tpop\tr13\n");
        printf("\tpop\tr12\n");
        printf("\tpop\trbp\n");
        printf("\tpop\trbx\n");
    }
    if(label != "[static]")
        printf("\tret\n\n");
}

ir::~ir()
{
    for(auto v:ins_list)
        delete v;
    //tbc
}

void Basic_block::add_use(vir_reg *p)
{
    use.insert(p);
}

void Basic_block::add_def(vir_reg *p)
{
    def.insert(p);
}

void Basic_block::print()
{
    instruction *ins = begin();
    printf("%llu\n{\n", this);
    while(ins != end())
    {
        ins->print();
        printf("%d %d\n", ins->pre.size(), ins->nxt.size());
        ins = (instruction*)(*(ins->nxt.begin()));
    }
    ins->print();
    printf("%d %d\n", ins->pre.size(), ins->nxt.size());
    printf("}\n");
    for(auto reg:phi)
        printf("%s ", reg->get_name().c_str());
    printf("\n-----------------------\n");
}

instruction *Basic_block::begin()
{
    return _begin;
}

instruction *Basic_block::end()
{
    return _end;
}

void Basic_block::set_begin(instruction *p)
{
    _begin = p;
}

void Basic_block::set_end(instruction *p)
{
    _end = p;
}

string prog_with_ir::trans(const string &str)
{
    string s;
    int siz = str.length();
    for(int i = 0; i < siz; ++i)
    {
        if('0' <= str[i] && str[i] <= '9')
            s.push_back(str[i] - 48);
        else if('a' <= str[i] && str[i] <= 'z')
            s.push_back(str[i] - 87);
        else if('A' <= str[i] && str[i] <= 'Z')
            s.push_back(str[i] - 29);
        else if(str[i] == '[')
            s.push_back(62);
        else if(str[i] == ']')
            s.push_back(63);
        else
            s.push_back(64);
    }
    return s;
}

prog_with_ir::prog_with_ir():func_index(65), reg_index(65), string_const(256)
{
    func_index.bind_trans_func(trans);
    build_func("[static]");
    reg_index.bind_trans_func(trans);
}

void prog_with_ir::clean_flow_info()
{
    list<trie<int>*>::reverse_iterator it = rename_table.rbegin();
    for(; it != rename_table.rend(); ++it)
        delete *it;
    rename_table.clear();
    for(auto func: func_list)
        func->clean_flow_info();
}

void prog_with_ir::remove_useless_call()
{
    for(auto func: func_list)
        func->remove_useless_call();
}

void ir::remove_useless_call()
{
    list<instruction*>::iterator it = ins_list.begin();
    for(; it != ins_list.end(); )
    {
        list<instruction*>::iterator now = it++;
        if((*now)->type == IR_INS_CALL)
        {
            ir_ins_call *ins = (ir_ins_call*)(*now);
            if(ins->name[0] != ']' && !prog->func_index.is_defined(ins->name))
                ins_list.erase(now);
        }
    }
}

void ir::clean_flow_info()
{
//    cfg->clean_flow_info();
//    cfg = NULL;
    for(auto ins: ins_list)
        ins->clean_flow_info();
}

void Basic_block::clean_flow_info()
{
    for(auto bb: son)
    {
        ((Basic_block*)bb)->clean_flow_info();
    }
    delete this;
}

ir* prog_with_ir::get_last_func()
{
    return func_list.back();
}

ir* prog_with_ir::get_first_func()
{
    return func_list.front();
}

void prog_with_ir::build_func(const string s)
{
    ir* t(new ir(this));
    t->label = s;
    func_list.push_back(t);
    func_index.insert(s, t);
}

vir_reg* prog_with_ir::build_reg(const string s)
{
    vir_reg* t(new vir_reg(s));
    reg_list.push_back(t);
    reg_index.insert(s, t);
    return t;
}

vir_reg* prog_with_ir::find_reg(const string s)
{
    if (!reg_index.is_defined(s))
    {
        if(s[0] >= '0' && s[0] <= '9')
            return new vir_reg(atoi(s.c_str()));
        build_reg(s);
    }
    return reg_index.find(s);
}

prog_with_ir::~prog_with_ir()
{
    /*
    for(auto reg: reg_list)
    {
        printf("%d %d %s\n", reg->get_def().size(), reg->get_use().size(), reg->get_name().c_str());
    }
    */
    for(auto v:func_list)
        delete v;
    for(auto reg: reg_list)
    {
        delete reg;
    }
}

void prog_with_ir::sim(bool ssa)
{
    ir* func = func_index.find("[static]");
    func->sim(ssa);
    func = func_index.find("main");
    debug_cnt = 0;
    func->sim(ssa);
    printf("com sum: %d\n", debug_cnt);
}

void prog_with_ir::print()
{
    for(auto f:func_list)
        f->print();
}

void prog_with_ir::print_x86()
{
    printf("global\tmain\n");
    printf("extern\tprintf\n");
    printf("extern\tsprintf\n");
    printf("extern\tputs\n");
    printf("extern\tscanf\n");
    printf("extern\tmalloc\n");
    printf("extern\tmemcpy\n");
    printf("extern\tstrcpy\n");
    printf("extern\tstrcat\n");
    printf("extern\tstrcmp\n");
    printf("extern\tstrlen\n");
    printf("extern\tatoi\n\n");
    ir* func = func_index.find("[static]");
    printf("SECTION .data\n");
    func->print_x86();
    printf("\n");
    printf("SECTION .text\n");
    for(auto func: func_list)
    {
        if(func->label == "[static]")
            continue;
        func->print_x86();
    }
}

void ir::init_label_set()
{
    ins_list.push_front(new instruction);
    list<instruction*>::iterator it = ins_list.begin();
    while(it != ins_list.end())
    {
        (*it)->func = this;
        (*it)->it = it;
        if((*it)->label.size() > 0)
            label_set.insert((*it)->label, iterator(it, &ins_list));
        ++it;
    }
    ins_list.push_back(new instruction);
}

void prog_with_ir::init_label_set()
{
    for(auto func:func_list)
        func->init_label_set();
}

void ir::build_ins_flow_graph()
{
    iterator p = begin();
    while(p != end())
    {
        cjmp = jmp = false;
        (*p)->init_ins_flow();
        if(!jmp)
        {
            iterator t(p);
            ++t;
            if(t != end())
                (*p)->add_out(*t);
        }
        if(jmp || cjmp)
            (*p)->add_out(*(label_set.find(name_flag)));
        ++p;
    }
}

void ir::rebuild_ins_flow_graph()
{
    iterator p = begin();

    while(p != end())
    {
        cjmp = jmp = false;
        (*p)->init_ins_flow();
        if(!jmp)
        {
            iterator t(p);
            ++t;
            if(t != end())
                (*p)->add_out(*t);
        }
        if(jmp || cjmp)
            (*p)->add_out(*(label_set.find(name_flag)));
        ++p;
    }
}

Basic_block* ir::get_bb(instruction* it)
{
    Basic_block* bb = new Basic_block;
    bb->set_begin(it);
    it->set_bb(bb);
    while (it->nxt.size() == 1)
    {
        instruction *it2 = (instruction*)(*(it->nxt.begin()));
        if(it2->pre.size() > 1)
            break;
        it = it2;
        it->set_bb(bb);
    }
    bb->set_end(it);
    return bb;
}

void ir::build_cfg()
{
    if(begin() == end())
        return;
    cfg = get_bb(*begin());
    list<Basic_block*> working_list;
    working_list.push_back(cfg);
    while(!working_list.empty())
    {
        Basic_block *p = working_list.front();
        working_list.pop_front();
        instruction *ins = p->end();
        for(auto t_ins:ins->nxt)
        {
            if(((instruction*)t_ins)->get_bb() == NULL)
            {
                Basic_block* new_block = get_bb((instruction*)t_ins);
                p->add_out(new_block);
                working_list.push_back(new_block);
            }
            else
                p->add_out(((instruction*)t_ins)->get_bb());
        }
    }
}

void ir_ins_jmp::init_ins_flow()
{
    func->jmp = true;
    func->name_flag = label;
}

void ir_ins_cjmp::init_ins_flow()
{
    func->cjmp = true;
    func->name_flag = label;
}

void ir::liveness_analysis()
{
    clean_ins_flag();
    list<instruction*> working_list;
    for(auto reg:prog->reg_list)
    {
        if(!reg->is_interable())
            continue;
        if(reg->is_imme())
            continue;

        for(auto ins:reg->get_use())
        {
            if(ins->func == this)
            {
                ins->flag = 1;
                ins->_export.insert(reg);
                working_list.push_back(ins);
            }
        }
        
        while(!working_list.empty())
        {
            instruction *now = working_list.front();
            for(auto f_node:now->pre)
            {
                instruction *pre = (instruction*)f_node;
                if(!pre->def_reg(reg) && pre->_export.count(reg) == 0)
                {
                    pre->_export.insert(reg);
                    if(!pre->flag)
                    {
                        pre->flag = 1;
                        working_list.push_back(pre);
                    }
                }
            }
            working_list.pop_front();
            now->flag = 0;
        }
    }
}

void dom_node::dfs(int &_clock, list<dom_node*> &wk)
{
    time_stamp = _clock++;
    wk.push_back(this);
    for(auto np:nxt)
    {
        dom_node *p = (dom_node*)np;
        if(p->time_stamp == -1)
            p->dfs(_clock, wk);
    }
}

void dom_node::set_fa(dom_node *p)
{
    fa = p;
}

dom_node* dom_node::get_fa()
{
    if(fa == this)
        return this;
    dom_node *p = fa->get_fa();
    if(fa->val->sdom->time_stamp < val->sdom->time_stamp)
        val = fa->val;
    return fa = p;
}

void dom_node::cal_sdom()
{
    int _time = 1e9;
    for(auto np:pre)
    {
        dom_node *p = (dom_node*)(np);
        if(p->time_stamp > time_stamp)
        {
            p->get_fa();
            dom_node *k = p->val; 
            if(k->sdom->time_stamp < _time)
            {
                _time = k->sdom->time_stamp;
                sdom = k->sdom;
            }
        }
        else
        {
            if(p->time_stamp < _time)
            {
                _time = p->time_stamp;
                sdom = p;
            }
        }
    }
    if(sdom != NULL)
        sdom->cal_list.push_back(this);
    else
        sdom = this;
    for(auto p:cal_list)
    {
        p->get_fa();
        p->u = p->val;
    }
    for(auto np:nxt)
    {
        dom_node *p = (dom_node*)(np);
        if(p->time_stamp > time_stamp)
            p->set_fa(this);
    }
}

void dom_node::add_son(dom_node *p)
{
    son.insert(p);
}

void dom_node::cal_idom()
{
    if(sdom != this)
    {
        if(u->sdom->time_stamp < sdom->time_stamp)
            idom = u->idom;
        else
            idom = sdom;
    }

    if(idom != NULL)
        idom->add_son(this);
}

void dom_node::cal_df()
{
    if(pre.size() > 1)
    {
        for(auto np:pre)
        {
            dom_node *p = (dom_node*)np;
            while(p != idom)
            {
                p->df.insert(this);
                p = p->idom;
            }
        }
    }
    for(auto np:son)
    {
        dom_node *p = (dom_node*)np;
        p->cal_df();
    }
}

void dom_node::build_dom_tree()
{
    int _clock = 0;
    list<dom_node*> working_list;
    dfs(_clock, working_list);
    list<dom_node*>::reverse_iterator it = working_list.rbegin();
    for(; it != working_list.rend(); ++it)
        (*it)->cal_sdom();
    list<dom_node*>::iterator it2 = working_list.begin();
    for(; it2 != working_list.end(); ++it2)
        (*it2)->cal_idom();

/*
    it2 = working_list.begin();
    for(; it2 != working_list.end(); ++it2)
        printf("[debug] %llu %llu\n", *it2, (*it2)->idom);
*/
}

void ir::place_phi()
{
    if(cfg == NULL)
        return;
    cfg->build_dom_tree();
    cfg->cal_df();
    for(auto reg:prog->reg_list)
    {
        list<Basic_block*> working_list;
        for(auto ins:reg->get_def())
        {
            if(ins->func == this && ins->get_bb() != NULL && !ins->get_bb()->flag)
            {
                ins->get_bb()->flag = 1;
                working_list.push_back(ins->get_bb());
            }
        }

        while(!working_list.empty())
        {
            Basic_block *p = working_list.front();
            working_list.pop_front();
            for(auto bp:p->df)
            {
                Basic_block *np = (Basic_block*)bp;
                if(np->begin()->_export.find(reg) == np->begin()->_export.end())
                    continue;
                if(np->phi.find(reg) == np->phi.end())
                {
                    np->phi.insert(reg);
                    if(!np->flag)
                    {
                        np->flag = 1;
                        working_list.push_back(np);
                    }
                }
            }
            p->flag = 0;
        }
    }
}

void instruction::replace_reg(vir_reg* _old, vir_reg* _new) {}
void instruction::rename_reg(trie<int> *tb, prog_with_ir *prog) {}

void instruction::rename_single_reg(prog_with_ir *prog, trie<int> *tb, vir_reg *&op, bool def)
{
    if(op->is_imme())
        return;
    int val = 0;
    if(def)
    {
        trie<int> *p = prog->rename_table.front();
        if(p->is_defined(op->get_name()))
            val = p->find(op->get_name());
    }
    else
    {
        if(tb->is_defined(op->get_name()))
            val = tb->find(op->get_name());
    }
    if(def)
    {
        trie<int> *p = prog->rename_table.front();
        ++val;
        tb->insert(op->get_name(), val, true);
        p->insert(op->get_name(), val, true);
        string tmp = op->get_name();
        op->remove_def(this);
        op = prog->build_reg(tmp + "[" + to_string(val) + "]");
        op->set_def(this);
    }
    else
    {
        string tmp = op->get_name();
        op->remove_use(this);
        op = prog->find_reg(tmp + "[" + to_string(val) + "]");
        op->set_use(this);
    }
}

void ir_ins_glb::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op == _old)
    {
        op = _new;
        _new->is_str = _old->is_str;
        _new->is_global = 1;
    }
}

void ir_ins_glb::rename_reg(trie<int>*, prog_with_ir *prog)
{
    op = prog->build_reg(op->get_name() + "[0]");
    op->is_global = 1;
    op->is_str = is_string;
}

void ir_ins_mov::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_def(this);
        op0 = _new;
        op0->set_def(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
}

void ir_ins_mov::rename_reg(trie<int> *tb, prog_with_ir *prog)
{
    rename_single_reg(prog, tb, op1);
    rename_single_reg(prog, tb, op0, 1);
}


void ir_ins_load::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_def(this);
        op0 = _new;
        op0->set_def(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
    if(base == _old)
    {
        base->remove_use(this);
        base = _new;
        base->set_use(this);
    }
    if(index == _old)
    {
        index->remove_use(this);
        index = _new;
        index->set_use(this);
    }
}

void ir_ins_load::rename_reg(trie<int> *tb, prog_with_ir *prog)
{
    rename_single_reg(prog, tb, op1);
    rename_single_reg(prog, tb, op0, 1);
}

void ir_ins_store::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_use(this);
        op0 = _new;
        op0->set_use(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
    if(base == _old)
    {
        base->remove_use(this);
        base = _new;
        base->set_use(this);
    }
    if(index == _old)
    {
        index->remove_use(this);
        index = _new;
        index->set_use(this);
    }
}

void ir_ins_store::rename_reg(trie<int> *tb, prog_with_ir *prog)
{
    rename_single_reg(prog, tb, op1);
    rename_single_reg(prog, tb, op0);
}

void ir_ins_thr_op::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_def(this);
        op0 = _new;
        op0->set_def(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
    if(op2 == _old)
    {
        op2->remove_use(this);
        op2 = _new;
        op2->set_use(this);
    }
}

void ir_ins_thr_op::rename_reg(trie<int> *tb, prog_with_ir *prog)
{
    rename_single_reg(prog, tb, op2);
    rename_single_reg(prog, tb, op1);
    rename_single_reg(prog, tb, op0, 1);
}

void ir_ins_call::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(re == _old)
    {
        re->remove_def(this);
        re = _new;
        re->set_def(this);
    }
    list<vir_reg*>::iterator it = para_list.begin();
    for(;it != para_list.end(); ++it)
    {
        if(*it == _old)
        {
            _old->remove_use(this);
            it = para_list.erase(it);
            para_list.insert(it, _new);
            _new->set_use(this);
            --it;
        }
    }
}

void ir_ins_call::rename_reg(trie<int> *tb, prog_with_ir *prog)
{
    list<vir_reg*>::iterator it = para_list.begin();
    for(; it != para_list.end(); ++it)
    {
        vir_reg *op = *it;
        rename_single_reg(prog, tb, op);
        it = para_list.erase(it);
        para_list.insert(it, op);
        --it;
    }
    if(re != NULL)
        rename_single_reg(prog, tb, re, 1);
}

void ir_ins_not::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_def(this);
        op0 = _new;
        op0->set_def(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
}

void ir_ins_not::rename_reg(trie<int> *tb, prog_with_ir *prog)
{
    rename_single_reg(prog, tb, op1);
    rename_single_reg(prog, tb, op0, 1);
}

void ir_ins_neg::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op0 == _old)
    {
        op0->remove_def(this);
        op0 = _new;
        op0->set_def(this);
    }
    if(op1 == _old)
    {
        op1->remove_use(this);
        op1 = _new;
        op1->set_use(this);
    }
}

void ir_ins_neg::rename_reg(trie<int> *tb, prog_with_ir *prog)
{
    rename_single_reg(prog, tb, op1);
    rename_single_reg(prog, tb, op0, 1);
}

void ir_ins_cjmp::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op == _old)
    {
        op->remove_use(this);
        op = _new;
        op->set_use(this);
    }
}

void ir_ins_cjmp::rename_reg(trie<int> *tb, prog_with_ir *prog)
{
    rename_single_reg(prog, tb, op);
}

void ir_ins_ret::replace_reg(vir_reg *_old, vir_reg *_new)
{
    if(op == _old)
    {
        op->remove_use(this);
        op = _new;
        op->set_use(this);
    }
}

void ir_ins_ret::rename_reg(trie<int> *tb, prog_with_ir *prog)
{
    if(op != NULL)
        rename_single_reg(prog, tb, op);
}

void Basic_block::renaming(prog_with_ir *prog, trie<int> *base)
{
    rename_table = new trie<int>(*base);
    prog->rename_table.push_back(rename_table);
    trie<int> *new_name = prog->rename_table.front();
    set<vir_reg*> rename_phi;
    for(auto reg:phi)
    {
        int val = 0;
        if(new_name->is_defined(reg->get_name()))
            val = new_name->find(reg->get_name());
        ++val;
        rename_table->insert(reg->get_name(), val, 1);
        new_name->insert(reg->get_name(), val, 1);
        vir_reg *phi_reg = prog->build_reg(reg->get_name() + "[" + to_string(val) + "]");
//        phi_reg->phi_v = true;
        rename_phi.insert(phi_reg);
    }
    phi = rename_phi;

    instruction* ins = begin();
    while(ins != end())
    {
        ins->rename_reg(rename_table, prog);
        ins = (instruction*)(*(ins->nxt.begin()));
    }
    ins->rename_reg(rename_table, prog);

    for(auto nb:son)
        ((Basic_block*)nb)->renaming(prog, rename_table);
}

void ir::renaming()
{
    if(cfg == NULL)
        return;

    prog->rename_table.push_back(new trie<int>(65));
    prog->rename_table.back()->bind_trans_func(trans);

    list<vir_reg*> t_para_list;
    for(auto reg:para_list)
    {
        t_para_list.push_back(prog->build_reg(reg->get_name() + "[0]"));
        prog->rename_table.back()->insert(reg->get_name(), 0, 1);
    }
    para_list = t_para_list;
    cfg->renaming(prog, prog->rename_table.back());
}

void prog_with_ir::convert_to_ssa()
{
    for(auto func:func_list)
    {
        if(func->label != "[static]" && func->label != "main")
            continue;
        func->build_ins_flow_graph();
        func->liveness_analysis();
        func->build_cfg();
        func->place_phi();
        func->renaming();
    }
    for(auto func:func_list)
    {
        if(func->label == "[static]" || func->label == "main")
            continue;
        func->build_ins_flow_graph();
        func->liveness_analysis();
        func->build_cfg();
        func->place_phi();
        func->renaming();
    }
}

void ir::iterator::cut()
{
    list<instruction*>::iterator tmp = p;
    ++p;
    ins_list->erase(tmp);
}

void ir::iterator::append(instruction *ins)
{
    ++p;
    ins_list->insert(p, ins);
    ins->it = p;
    --(ins->it);
}

void ir::iterator::insert(instruction *ins)
{
    ins_list->insert(p, ins);
    ins->it = p;
    --(ins->it);
}

void prog_with_ir::move_init_ins()
{
    ir *_static = get_first_func();
    ir *_main = func_index.find("main");
    ir::iterator it_main = _main->begin();
    ir::iterator it_static = _static->begin();
    while(it_static != _static->end())
    {
        if((*it_static)->type != IR_INS_GLB)
        {
            it_main.insert(*it_static);
            it_static.cut();
        }
        else
        {
            ++it_static;
        }
    }
}

void ir::clean_ins_flag()
{
    for(auto ins:ins_list)
        ins->flag = false;
}

void prog_with_ir::opt_inline(int level)
{
    int inline_cnt = 0;
    int inline_siz = 200;
    int inline_s = 0;
    while(level--)
    {
        for(auto func:func_list)
            func->clean_ins_flag();
        list<ir::iterator> inline_list;
        for(auto func:func_list)
        {
            ir::iterator it = func->begin();
            while(it != func->end())
            {
                if((*it)->type == IR_INS_CALL)
                {
                    ir_ins_call *bak = ((ir_ins_call*)(*it));
                    string s = bak->get_name();
                    if(func_index.is_defined(s))
                    {
                        ir *callee = func_index.find(s);
                        int siz = 0;
                        for(auto ins:callee->ins_list)
                            if(!ins->flag)
                                ++siz;
                        if(siz < inline_siz && inline_s < 5000)
                        {
                            inline_list.push_back(it);

                            ++inline_cnt;

//                            string exit_label = "[inline_exit][" + to_string(name_cnt++) + "]";

                            list<vir_reg*>::iterator caller_para = bak->para_list.begin();
                            list<vir_reg*>::iterator callee_para = callee->para_list.begin();
                            for(;caller_para != bak->para_list.end(); ++caller_para, ++callee_para)
                            {
                                vir_reg *nreg = find_reg((*callee_para)->get_name() + "][" + to_string(inline_cnt) + "]");
                                instruction *ins = new ir_ins_mov(nreg, *caller_para);
                                ins->flag = true;
                                it.insert(ins);
                            }

                            for(auto ins:callee->ins_list)
                            {
                                if(ins->flag)
                                    continue;
                                ++inline_s;
                                if(ins->type == IR_INS_RET)
                                {
                                    instruction *nins;
                                    if(bak->re != NULL && ((ir_ins_ret*)ins)->op != NULL)
                                    {
                                        vir_reg *nreg = ((ir_ins_ret*)ins)->op;
                                        if(!nreg->is_global)
                                            nreg = find_reg(nreg->get_name() + "][" + to_string(inline_cnt) + "]");
                                        nins = new ir_ins_mov(bak->re, nreg);
                                        nins->flag = true;
                                        it.insert(nins);
                                    }
                                    /*
                                    nins = new ir_ins_jmp(exit_label);
                                    nins->flag = true;
                                    it.append(nins);
                                    */
                                    continue;
                                }
                                instruction *nins = ins->clone(this, inline_cnt);
                                nins->flag = true;
                                it.insert(nins);
                            }
                            /*
                            instruction *nins = new instruction;
                            nins->label = exit_label;
                            nins->flag = true;
                            it.append(nins);
                            */
                        }
                    }
                }
                ++it;
            }
        }
        for(auto it:inline_list)
        {
            delete *it;
            it.cut();
        }
    }
}

void prog_with_ir::mark_phi_v()
{
    for(auto func: func_list)
        func->mark_phi_v();
}

void ir::mark_phi_v()
{
    cfg->mark_phi_v(prog);
}

void Basic_block::mark_phi_v(prog_with_ir *prog)
{
    instruction *ins = _begin;
    for(auto _pre: pre)
    {
        Basic_block *p = (Basic_block*)_pre;
        for(auto reg: phi)
        {
            string s = cut_reg_name(reg->get_name());
            int val = 0;
            if(p->rename_table->is_defined(s))
                val = p->rename_table->find(s);
            if(prog->reg_index.is_defined(s + "[" + to_string(val) + "]"))
            {
                vir_reg *old_reg = prog->reg_index.find(s + "[" + to_string(val) + "]");
                old_reg->phi_v = true;
            }
        }
    }
    for(auto _son: son)
        ((Basic_block*)_son)->mark_phi_v(prog);
}

void instruction::self_remove()
{
    if(bb->begin() == this)
    {
        ++it;
        bb->set_begin(*it);
        --it;
    }
    if(bb->end() == this)
    {
        --it;
        bb->set_end(*it);
        ++it;
    }
    if(type == IR_INS_NULL || type == IR_INS_JMP)
        throw("Invalid remove");

    list<instruction*>::iterator _pre, _nxt;
    int cnt = 0;

    if(it != func->ins_list.begin())
    {
        _pre = it;
        --_pre;
        ++cnt;
    }

    if(it != func->ins_list.end())
    {
        _nxt = it;
        ++_nxt;
        ++cnt;
    }

    if(cnt == 2)
    {
        (*_pre)->add_out(*_nxt);
    }

    set<flow_node*>::iterator _p, _n;
    for(_p = pre.begin(); _p != pre.end();)
    {
        instruction *p = (instruction*)*_p;
        ++_p;
        p->erase_out(this);
    }
    for(_n = nxt.begin(); _n != nxt.end();)
    {
        instruction *n = (instruction*)*_n;
        ++_n;
        erase_out(n);
    }

    func->ins_list.erase(it);

    delete this;
}

void ir::tmp_func_label()
{
    list<instruction*>::iterator it = ins_list.begin();
    for(; it != ins_list.end(); ++it)
    {
        (*it)->func = this;
        (*it)->it = it;
    }
}

void ir::init_pcg()
{
    for(auto ins: ins_list)
    {
        if(ins->type == IR_INS_CALL)
        {
            ir_ins_call *top_ins = (ir_ins_call*)ins;
            if(prog->func_index.is_defined(top_ins->name))
            {
                ir *p = prog->func_index.find(top_ins->name);
                if (p != this)
                    add_out(p);
            }
        }
    }
}

void ir::replace_global()
{
    for(auto reg: global_set)
    {
        vir_reg *new_reg = prog->build_reg("[t][" + to_string(name_cnt++) + "]");
        ins_list.push_front(new ir_ins_load(new_reg, reg));
        if(label != "main")
            ins_list.push_back(new ir_ins_store(reg, new_reg));

        set<instruction*> use_ins = reg->get_use();
        for(auto ins: use_ins)
        {
            if(ins->func != this)
                continue;
            if(ins->type == IR_INS_LOAD)
            {
                ir_ins_load *top_ins = (ir_ins_load*)ins;
                ins_list.insert(top_ins->it, new ir_ins_mov(top_ins->op0, new_reg));
                ins_list.erase(top_ins->it);
                delete top_ins;
                continue;
            }
            if(ins->type == IR_INS_STORE)
            {
                ir_ins_store *top_ins = (ir_ins_store*)ins;
                ins_list.insert(top_ins->it, new ir_ins_mov(new_reg, top_ins->op1));
                ins_list.erase(top_ins->it);
                delete top_ins;
                continue;
            }
        }

        list<instruction*>::iterator it = ins_list.begin();
        for(; it != ins_list.end(); ++it)
        {
            instruction *ins = *it;
            if(ins->type == IR_INS_CALL)
            {
                ir_ins_call *top_ins = (ir_ins_call*)ins;
                if(prog->func_index.is_defined(top_ins->name))
                {
                    list<instruction*>::iterator it2 = it;
                    ++it2;
                    ir *call_func = prog->func_index.find(top_ins->name);
                    if(call_func->global_set.count(reg))
                    {
                        ins_list.insert(it, new ir_ins_store(reg, new_reg));
                        ins_list.insert(it2, new ir_ins_load(new_reg, reg));
                    }
                }
            }
        }
    }
}

void prog_with_ir::replace_global()
{
    for(auto func: func_list)
    {
        func->tmp_func_label();
        func->init_pcg();
    }
    for(auto reg: reg_list)
    {
        if(!reg->is_global)
            continue;
        if(reg == buff)
            continue;
        if(reg->is_str)
            continue;
        const set<instruction*> &use_set = reg->get_use();
        for(auto ins: use_set)
        {
            if(ins->type == IR_INS_LOAD || ins->type == IR_INS_STORE)
                ins->func->global_set.insert(reg);
        }
    }
    list<ir*> working_list;
    for(auto func: func_list)
    {
        working_list.push_back(func);
        func->flag = 1;
    }
    while(!working_list.empty())
    {
        ir *now = working_list.front();
        working_list.pop_front();
        for(auto _pre: now->pre)
        {
            ir* pre = (ir*)_pre;
            for(auto reg: now->global_set)
            {
                if(pre->global_set.count(reg) == 0)
                {
                    pre->global_set.insert(reg);
                    if(!pre->flag)
                    {
                        pre->flag = 1;
                        working_list.push_back(pre);
                    }
                }
            }
        }
        now->flag = 0;
    }
    for(auto func: func_list)
        func->replace_global();
}

void prog_with_ir::liveness_analysis()
{
    for(auto func: func_list)
    {
        func->liveness_analysis();
    }
}

//--------------ins_simplification---------------------

void prog_with_ir::ins_simplification()
{
    for(auto func: func_list)
        func->ins_simplification();
}

void ir::ins_simplification()
{
    int cnt = 0;
    while(gen_lea());
    useless_lea();
    cjmp2jmp();
    cjmp2jcc();
}

void ir::cjmp2jmp()
{
    list<instruction*>::iterator it = ins_list.begin();
    for(; it != ins_list.end(); )
    {
        instruction *ins = *it;
        ++it;
        if(ins->get_bb() == NULL)
            continue;
        if(ins->type == IR_INS_CJMP)
        {
            ir_ins_cjmp *top_ins = (ir_ins_cjmp*)ins;
            if(top_ins->op->is_imme())
            {
                if(top_ins->op->get_v() == top_ins->taken)
                {
                    ir_ins_jmp *jmp_ins = new ir_ins_jmp(top_ins->label);
                    insert_ins_in_bb(it, jmp_ins);
                    jmp_ins->set_bb(top_ins->get_bb());
                    jmp_ins->get_bb()->set_end(jmp_ins);
                }
                top_ins->self_remove();
            }
        }
    }
}

void ir::cjmp2jcc()
{
    list<instruction*>::iterator it = ins_list.begin();
    for(; it != ins_list.end();)
    {
        instruction *ins = *it;
        ++it;
        if(ins->get_bb() == NULL)
            continue;

        if(ins->type == IR_INS_EQU)
        {
            ir_ins_equ *top_ins = (ir_ins_equ*)ins;
            if(top_ins->op1->is_imme() && top_ins->op1->get_v() == 0)
            {
                instruction *_dependence_ins = NULL;
                if(top_ins->op2->get_def().size() == 1 && top_ins->op2->get_use_size() == 1)
                    _dependence_ins = *(top_ins->op2->get_def().begin());
                else
                    continue;

                instruction *new_ins = NULL;

                switch (_dependence_ins->type)
                {
                case IR_INS_LST:
                {
                    ir_ins_lst *dependence_ins = (ir_ins_lst*)_dependence_ins;
                    new_ins = new ir_ins_let(top_ins->op0, dependence_ins->op2, dependence_ins->op1);
                    dependence_ins->self_remove();
                    break;
                }
                case IR_INS_LET:
                {
                    ir_ins_let *dependence_ins = (ir_ins_let*)_dependence_ins;
                    new_ins = new ir_ins_lst(top_ins->op0, dependence_ins->op2, dependence_ins->op1);
                    dependence_ins->self_remove();
                    break;
                }
                case IR_INS_EQU:
                {
                    ir_ins_equ *dependence_ins = (ir_ins_equ*)_dependence_ins;
                    new_ins = new ir_ins_neq(top_ins->op0, dependence_ins->op1, dependence_ins->op2);
                    dependence_ins->self_remove();
                    break;
                }
                case IR_INS_NEQ:
                {
                    ir_ins_neq *dependence_ins = (ir_ins_neq*)_dependence_ins;
                    new_ins = new ir_ins_equ(top_ins->op0, dependence_ins->op1, dependence_ins->op2);
                    dependence_ins->self_remove();
                    break;
                }
                default:
                    break;
                }

                if(new_ins != NULL)
                {
                    insert_ins_in_bb(it, new_ins);
                    top_ins->self_remove();
                }
            }
            continue;
        }

        if(ins->type == IR_INS_NEQ)
        {
            ir_ins_neq *top_ins = (ir_ins_neq*)ins;
            if(top_ins->op1->is_imme() && top_ins->op1->get_v() == 0)
            {
                instruction *_dependence_ins = NULL;
                if(top_ins->op2->get_def().size() == 1 && top_ins->op2->get_use_size() == 1)
                    _dependence_ins = *(top_ins->op2->get_def().begin());
                else
                    continue;

                instruction *new_ins = NULL;

                switch (_dependence_ins->type)
                {
                case IR_INS_LST:
                {
                    ir_ins_lst *dependence_ins = (ir_ins_lst*)_dependence_ins;
                    new_ins = new ir_ins_lst(top_ins->op0, dependence_ins->op1, dependence_ins->op2);
                    dependence_ins->self_remove();
                    break;
                }
                case IR_INS_LET:
                {
                    ir_ins_let *dependence_ins = (ir_ins_let*)_dependence_ins;
                    new_ins = new ir_ins_let(top_ins->op0, dependence_ins->op1, dependence_ins->op2);
                    dependence_ins->self_remove();
                    break;
                }
                case IR_INS_EQU:
                {
                    ir_ins_equ *dependence_ins = (ir_ins_equ*)_dependence_ins;
                    new_ins = new ir_ins_equ(top_ins->op0, dependence_ins->op1, dependence_ins->op2);
                    dependence_ins->self_remove();
                    break;
                }
                case IR_INS_NEQ:
                {
                    ir_ins_neq *dependence_ins = (ir_ins_neq*)_dependence_ins;
                    new_ins = new ir_ins_neq(top_ins->op0, dependence_ins->op1, dependence_ins->op2);
                    dependence_ins->self_remove();
                    break;
                }
                default:
                    break;
                }

                if(new_ins != NULL)
                {
                    insert_ins_in_bb(it, new_ins);
                    top_ins->self_remove();
                }
            }
            continue;
        }

        if(ins->type == IR_INS_CJMP)
        {
            ir_ins_cjmp *top_ins = (ir_ins_cjmp*)ins;
            instruction *_dependence_ins = NULL;
            if(top_ins->op->get_def().size() == 1 && top_ins->op->get_use_size() == 1)
                _dependence_ins = *(top_ins->op->get_def().begin());
            else
                continue;
            
            instruction *new_ins = NULL;
            
            switch(_dependence_ins->type)
            {
            case IR_INS_EQU:
            {
                ir_ins_equ *dependence_ins = (ir_ins_equ*)_dependence_ins;
                if(top_ins->taken)
                    new_ins = new ir_ins_jequ(dependence_ins->op1, dependence_ins->op2, top_ins->label);
                else
                    new_ins = new ir_ins_jneq(dependence_ins->op1, dependence_ins->op2, top_ins->label);
                dependence_ins->self_remove();
                break;
            }
            case IR_INS_NEQ:
            {
                ir_ins_neq *dependence_ins = (ir_ins_neq*)_dependence_ins;
                if(top_ins->taken)
                    new_ins = new ir_ins_jneq(dependence_ins->op1, dependence_ins->op2, top_ins->label);
                else
                    new_ins = new ir_ins_jequ(dependence_ins->op1, dependence_ins->op2, top_ins->label);
                dependence_ins->self_remove();
                break;
            }
            case IR_INS_LST:
            {
                ir_ins_lst *dependence_ins = (ir_ins_lst*)_dependence_ins;
                if(top_ins->taken)
                    new_ins = new ir_ins_jlst(dependence_ins->op1, dependence_ins->op2, top_ins->label);
                else
                    new_ins = new ir_ins_jlet(dependence_ins->op2, dependence_ins->op1, top_ins->label);
                dependence_ins->self_remove();
                break;
            }
            case IR_INS_LET:
            {
                ir_ins_let *dependence_ins = (ir_ins_let*)_dependence_ins;
                if(top_ins->taken)
                    new_ins = new ir_ins_jlet(dependence_ins->op1, dependence_ins->op2, top_ins->label);
                else
                    new_ins = new ir_ins_jlst(dependence_ins->op2, dependence_ins->op1, top_ins->label);
                dependence_ins->self_remove();
                break;
            }
            default:
                break;
            }
            if(new_ins != NULL)
            {
                insert_ins_in_bb(it, new_ins);
                new_ins->set_bb(top_ins->get_bb());
                new_ins->get_bb()->set_end(new_ins);
                top_ins->self_remove();
            }
        }
    }
}

void ir::useless_lea()
{
    list<instruction*>::iterator it = ins_list.begin();
    for(; it != ins_list.end(); )
    {
        instruction *ins = *it;
        list<instruction*>::iterator it2 = it;
        ++it;
        if(ins->type == IR_INS_LEA)
        {
            ir_ins_lea *top_ins = (ir_ins_lea*)ins;
            if(top_ins->target->get_use_size() == 1 && top_ins->target->get_use().size() == 1)
            {
                instruction *use_ins = *(top_ins->target->get_use().begin());
                if(use_ins->type == IR_INS_LOAD && ((ir_ins_load*)use_ins)->op1 == top_ins->target)
                {
                    ((ir_ins_load*)use_ins)->merge_lea(top_ins);
                    continue;
                }
                if(use_ins->type == IR_INS_STORE && ((ir_ins_store*)use_ins)->op0 == top_ins->target)
                {
                    ((ir_ins_store*)use_ins)->merge_lea(top_ins);
                    continue;
                }
            }
            if(top_ins->offset == 0 && top_ins->scale == 1 && top_ins->index != NULL && top_ins->base != NULL)
            {
                instruction *nins = new ir_ins_add(top_ins->target, top_ins->base, top_ins->index);
                insert_ins_in_bb(it2, nins);
                ins->self_remove();
                continue;
            }
            if(top_ins->offset != 0 && top_ins->index == NULL)
            {
                instruction *nins;
                if(top_ins->offset > 0)
                    nins = new ir_ins_add(top_ins->target, top_ins->base, new vir_reg(top_ins->offset));
                else
                    nins = new ir_ins_sub(top_ins->target, top_ins->base, new vir_reg(-top_ins->offset));
                insert_ins_in_bb(it2, nins);
                ins->self_remove();
                continue;
            }
            if(top_ins->offset == 0 && top_ins->index == NULL)
            {
                instruction *nins = new ir_ins_mov(top_ins->target, top_ins->base);
                insert_ins_in_bb(it2, nins);
                ins->self_remove();
                continue;
            }
            /*
            if(top_ins->offset == 0 && top_ins->base == NULL)
            {
                int v = 0;
                switch(top_ins->scale)
                {
                    case 1:
                        v = 0;
                        break;
                    case 2:
                        v = 1;
                        break;
                    case 4:
                        v = 2;
                        break;
                    case 8:
                        v = 3;
                        break;
                    default:
                        break;
                }
                if(v)
                {
                    instruction *nins = new ir_ins_shl(top_ins->target, top_ins->index, new vir_reg(v));
                    insert_ins_in_bb(it2, nins);
                }
                ins->self_remove();
                continue;
            }
            */
        }
    }
}


bool ir::gen_lea()
{
    bool do_something = false;

    list<instruction*>::iterator it = ins_list.begin();
    for(; it != ins_list.end();)
    {
        instruction *ins = *it;
        ++it;
        if(ins->get_bb() == NULL)
            continue;

        if(ins->type == IR_INS_ADD)
        {
            ir_ins_add *top_ins = (ir_ins_add*)ins;
            if(top_ins->op1->is_imme())
                swap(top_ins->op1, top_ins->op2);
            ir_ins_lea *lea_ins = new ir_ins_lea(top_ins->op0);
            lea_ins->set_base(top_ins->op1);
            if(top_ins->op2->is_imme())
            {
                lea_ins->offset = top_ins->op2->get_v();
            }
            else
            {
                lea_ins->set_index(top_ins->op2);
                lea_ins->scale = 1;
            }
            insert_ins_in_bb(it, lea_ins);
            top_ins->self_remove();
            do_something = true;
            continue;
        }

        if(ins->type == IR_INS_MUL)
        {
            ir_ins_mul *top_ins = (ir_ins_mul*)ins;
            if(top_ins->op1->is_imme())
                swap(top_ins->op1, top_ins->op2);
            if(!top_ins->op2->is_imme())
                continue;
            switch (top_ins->op2->get_v())
            {
            case 1:
            case 2:
            case 4:
            case 8:
            {
                ir_ins_lea *lea_ins = new ir_ins_lea(top_ins->op0);
                lea_ins->set_index(top_ins->op1);
                lea_ins->scale = top_ins->op2->get_v();
                insert_ins_in_bb(it, lea_ins);
                top_ins->self_remove();
                do_something = true;
                continue;
                break;
            }

            default:
                break;
            }
        }

        if(ins->type == IR_INS_SUB)
        {
            ir_ins_sub *top_ins = (ir_ins_sub*)ins;
            if(!top_ins->op2->is_imme())
                continue;

            ir_ins_lea *lea_ins = new ir_ins_lea(top_ins->op0);
            lea_ins->set_base(top_ins->op1);
            if(top_ins->op2->is_imme())
            {
                lea_ins->offset = -top_ins->op2->get_v();
            }
            insert_ins_in_bb(it, lea_ins);
            top_ins->self_remove();
            do_something = true;
            continue;
        }

        if(ins->type == IR_INS_LEA)
        {
            ir_ins_lea *top_ins = (ir_ins_lea*)ins;
            ir_ins_lea *lea_ins = new ir_ins_lea(top_ins->target);

            lea_ins->offset = top_ins->offset;

            bool suc_flag = false;

            instruction *_left_ins = NULL, *_right_ins = NULL;
            ir_ins_lea *left_ins = NULL, *right_ins = NULL;

            map<vir_reg*, int> cnt_map;

            if(top_ins->base != NULL)
            {
                _left_ins = *(top_ins->base->get_def().begin());
                if(top_ins->base->get_def().size() == 1)
                {
                    if(_left_ins->type == IR_INS_LEA && ((ir_ins_lea*)_left_ins)->target->get_use_size() == 1)
                    {
                        left_ins = (ir_ins_lea*)_left_ins;
                        lea_ins->offset += left_ins->offset;
                    }
                }
                else
                {
                    if(cnt_map.count(top_ins->base) == 0)
                        cnt_map[top_ins->base] = 1;
                    else
                        cnt_map[top_ins->base] = cnt_map[top_ins->base] + 1;
                }
                
            }

            if(top_ins->index != NULL)
            {
                _right_ins = *(top_ins->index->get_def().begin());
                if(top_ins->index->get_def().size() == 1)
                {
                    if(_right_ins->type == IR_INS_LEA && ((ir_ins_lea*)_right_ins)->target->get_use_size() == 1)
                    {
                        right_ins = (ir_ins_lea*)_right_ins;
                        lea_ins->offset += right_ins->offset * top_ins->scale;
                    }
                }
                else
                {
                    if(cnt_map.count(top_ins->index) == 0)
                        cnt_map[top_ins->index] = top_ins->scale;
                    else
                        cnt_map[top_ins->index] = cnt_map[top_ins->index] + top_ins->scale;
                }
            }

            vir_reg *v0 = NULL, *v1 = NULL;

            if(left_ins != NULL)
            {
                if(left_ins->base != NULL)
                {
                    if(cnt_map.count(left_ins->base) == 0)
                        cnt_map[left_ins->base] = 1;
                    else
                        cnt_map[left_ins->base] = cnt_map[left_ins->base] + 1;
                }
                if(left_ins->index != NULL)
                {
                    if(cnt_map.count(left_ins->index) == 0)
                        cnt_map[left_ins->index] = left_ins->scale;
                    else
                        cnt_map[left_ins->index] = cnt_map[left_ins->index] + left_ins->scale;
                }
            }
            else if(_left_ins != NULL)
            {
                if(cnt_map.count(top_ins->base) == 0)
                    cnt_map[top_ins->base] = 1;
                else
                    cnt_map[top_ins->base] = cnt_map[top_ins->base] + 1;
            }

            if(right_ins != NULL)
            {
                if(right_ins->base != NULL)
                {
                    if(cnt_map.count(right_ins->base) == 0)
                        cnt_map[right_ins->base] = top_ins->scale;
                    else
                        cnt_map[right_ins->base] = cnt_map[right_ins->base] + top_ins->scale;
                }
                if(right_ins->index != NULL)
                {
                    if(cnt_map.count(right_ins->index) == 0)
                        cnt_map[right_ins->index] = right_ins->scale * top_ins->scale;
                    else
                        cnt_map[right_ins->index] = cnt_map[right_ins->index] + right_ins->scale * top_ins->scale;
                }
            }
            else if(_right_ins != NULL)
            {
                if(cnt_map.count(top_ins->index) == 0)
                    cnt_map[top_ins->index] = top_ins->scale;
                else
                    cnt_map[top_ins->index] = cnt_map[top_ins->index] + top_ins->scale;
            }

            suc_flag = true;
            for(auto it: cnt_map)
            {
                if(it.second == 1)
                {
                    if(v0 == NULL)
                        v0 = it.first;
                    else if(v1 == NULL)
                        v1 = it.first;
                    else
                        suc_flag = false;
                }
                else
                {
                    if(v1 == NULL)
                        v1 = it.first;
                    else
                        suc_flag = false;
                }
            }

            if(v0 == top_ins->base && v1 == top_ins->index || v1 == top_ins->base && v0 == top_ins->index)
                suc_flag = false;

            int cnt = cnt_map[v1];
            if(cnt != 1 && cnt != 2 && cnt != 4 && cnt != 8)
                suc_flag = false;
            
            if(suc_flag)
            {
                if(v0 != NULL)
                    lea_ins->set_base(v0);
                if(v1 != NULL)
                    lea_ins->set_index(v1);
                lea_ins->scale = cnt;
                insert_ins_in_bb(it, lea_ins);

                if(left_ins)
                    left_ins->self_remove();
                if(right_ins)
                    right_ins->self_remove();

                top_ins->self_remove();

                do_something = true;
                continue;
            }
            else
                delete lea_ins;
        }
    }
    return do_something;
}
//-----------------------------------------------------


//--------------constant_pro-------------------------------

void prog_with_ir::constant_pro()
{
    for(auto func: func_list)
        func->constant_pro();
}

void ir::constant_pro()
{
    list<vir_reg*> working_list;
    list<instruction*>::iterator it;
    for(it = ins_list.begin(); it != ins_list.end();)
    {
        instruction *ins = (*it);
        ++it;
        if(ins->get_bb() == NULL)
            continue;
        vir_reg *re = ins->constant_pro();
        if(re != NULL)
            working_list.push_back(re);
    }
    while(!working_list.empty())
    {
        vir_reg *now = working_list.front();
        working_list.pop_front();
        for(auto ins: now->get_use())
        {
            vir_reg *re = ins->constant_pro();
            if(re != NULL)
                working_list.push_back(re);
        }
    }
}

//----------------------------------------------------

//----------------copy_pro----------------------------
void prog_with_ir::copy_pro()
{
    for(auto func: func_list)
        func->copy_pro();
}

void ir::copy_pro()
{
    list<instruction*>::iterator it;
    for(it = ins_list.begin(); it != ins_list.end();)
    {
        instruction *ins = (*it);
        ++it;
        if(ins->get_bb() == NULL)
            continue;
        ins->copy_pro();
    }
}
//----------------------------------------------------

//----------------------gvn---------------------------
void prog_with_ir::gvn()
{
    for(auto func: func_list)
        func->gvn();

    while(!vn_table_list.empty())
    {
        trie<vir_reg*> *now = vn_table_list.back();
        vn_table_list.pop_back();
        delete now;
    }
}

void ir::gvn()
{
    if(cfg == NULL)
        return;
    prog->vn_table_list.push_back(new trie<vir_reg*>(65));
    prog->vn_table_list.back()->bind_trans_func(trans);
    cfg->gvn(prog, prog->vn_table_list.back());
}

void Basic_block::gvn(prog_with_ir *prog, trie<vir_reg*> *meta_data)
{
    vn_table = new trie<vir_reg*>(*meta_data);
    prog->vn_table_list.push_back(vn_table);

    instruction* ins = begin();
    while(ins != end())
    {
        instruction *nins = (instruction*)(*(ins->nxt.begin()));
        ins->gvn(vn_table);
        ins = nins;
    }
    ins->gvn(vn_table);

    for(auto _s: son)
    {
        Basic_block *s = (Basic_block*)_s;
        s->gvn(prog, vn_table);
    }
}
//----------------------------------------------------

void prog_with_ir::ssa_deconstruct()
{
    for(auto func: func_list)
        func->ssa_deconstruct();
}

void ir::ssa_deconstruct()
{
    for(auto ins: ins_list)
    {
        if(ins->get_bb() == NULL)
            continue;
        if(ins->get_bb()->begin() != ins)
            continue;
        Basic_block *new_bb = ins->get_bb();
        for(auto _old_bb: new_bb->pre)
        {
            Basic_block *old_bb = (Basic_block*)_old_bb;
            for(auto reg: new_bb->phi)
            {
                string s = cut_reg_name(reg->get_name());
                int val = 0;
                if(old_bb->rename_table->is_defined(s))
                    val = old_bb->rename_table->find(s);
                if(prog->reg_index.is_defined(s + "[" + to_string(val) + "]"))
                {
                    vir_reg *old_reg = prog->reg_index.find(s + "[" + to_string(val) + "]");
                    old_reg = old_reg->get_phi_coalesce_reg();
                    instruction *nins = new ir_ins_mov(reg, old_reg);
                    insert_ins_in_bb(old_bb->end()->it, nins);
                    if(old_bb->begin() == old_bb->end())
                        old_bb->set_begin(nins);
                }
            }
        }
    }
}

/*
void ir::ssa_deconstruct()
{
    list<vir_reg*> working_list;
    for(auto ins: ins_list)
    {
        if(ins->get_bb() == NULL)
            continue;
        if(ins->get_bb()->begin() != ins)
            continue;
        Basic_block *new_bb = ins->get_bb();
        for(auto _old_bb: new_bb->pre)
        {
            Basic_block *old_bb = (Basic_block*)_old_bb;
            for(auto reg: new_bb->phi)
            {
                string s = cut_reg_name(reg->get_name());
                int val = 0;
                if(old_bb->rename_table->is_defined(s))
                    val = old_bb->rename_table->find(s);
                if(prog->reg_index.is_defined(s + "[" + to_string(val) + "]"))
                {
                    vir_reg *old_reg = prog->reg_index.find(s + "[" + to_string(val) + "]");
                    working_list.push_back(old_reg);
                    if(reg->get_phi_coalesce_reg() != old_reg->get_phi_coalesce_reg())
                    {
                        working_list.push_back(old_reg->get_phi_coalesce_reg());
                        old_reg->get_phi_coalesce_reg()->phi_coalesce = reg;
                    }
                }
            }
        }
    }
    for(auto reg: working_list)
    {
        reg->replace(reg->get_phi_coalesce_reg());
    }
}
*/

void prog_with_ir::add_constraint()
{
    pre_color_reg["res0"] = pre_color_reg["rax"] = new vir_reg("rax", RAX);
    pre_color_reg["arg1"] = pre_color_reg["rdi"] = new vir_reg("rdi", RDI);
    pre_color_reg["arg2"] = pre_color_reg["rsi"] = new vir_reg("rsi", RSI);
    pre_color_reg["arg3"] = pre_color_reg["rdx"] = new vir_reg("rdx", RDX);
    pre_color_reg["arg4"] = pre_color_reg["rcx"] = new vir_reg("rcx", RCX);
    pre_color_reg["arg5"] = pre_color_reg["r8"] = new vir_reg("r8", R8);
    pre_color_reg["arg6"] = pre_color_reg["r9"] = new vir_reg("r9", R9);
    pre_color_reg["ler7"] = pre_color_reg["r10"] = new vir_reg("r10", R10);
    pre_color_reg["ler8"] = pre_color_reg["r11"] = new vir_reg("r11", R11);
    pre_color_reg["lee9"] = pre_color_reg["rbx"] = new vir_reg("rbx", RBX);
    pre_color_reg["lee10"] = pre_color_reg["rbp"] = new vir_reg("rbp", RBX);
    pre_color_reg["lee11"] = pre_color_reg["r12"] = new vir_reg("r12", R12);
    pre_color_reg["lee12"] = pre_color_reg["r13"] = new vir_reg("r13", R13);
    pre_color_reg["lee13"] = pre_color_reg["r14"] = new vir_reg("r14", R14);
    pre_color_reg["lee14"] = pre_color_reg["r15"] = new vir_reg("r15", R15);
    pre_color_reg["rsp"] = new vir_reg("rsp", RSP);
    reg_list.push_back(pre_color_reg["res0"]);
    reg_list.push_back(pre_color_reg["arg1"]);
    reg_list.push_back(pre_color_reg["arg2"]);
    reg_list.push_back(pre_color_reg["arg3"]);
    reg_list.push_back(pre_color_reg["arg4"]);
    reg_list.push_back(pre_color_reg["arg5"]);
    reg_list.push_back(pre_color_reg["arg6"]);
    reg_list.push_back(pre_color_reg["ler7"]);
    reg_list.push_back(pre_color_reg["ler8"]);
    reg_list.push_back(pre_color_reg["lee9"]);
    reg_list.push_back(pre_color_reg["lee10"]);
    reg_list.push_back(pre_color_reg["lee11"]);
    reg_list.push_back(pre_color_reg["lee12"]);
    reg_list.push_back(pre_color_reg["lee13"]);
    reg_list.push_back(pre_color_reg["lee14"]);
    for(auto func: func_list)
        func->add_constraint(pre_color_reg);
}

void ir::add_constraint(map<string, vir_reg*> &pre_color_reg)
{
    list<instruction*>::iterator it;
    it = ins_list.begin();
    if(!_sim)
    {
        ++it;
        {
            int cnt = 0;
            for(auto para: para_list)
            {
                ++cnt;
                if(cnt <= 6)
                {
                    instruction *nins = new ir_ins_mov(para, pre_color_reg["arg" + to_string(cnt)]);
                    insert_ins_in_bb(it, nins);
                }
                else
                {
                    instruction *nins = new ir_ins_load(para, new vir_reg(cnt - 6));
                    insert_ins_in_bb(it, nins);
                }
                
            }
        }
    }
    for(it = ins_list.begin(); it != ins_list.end();)
    {
        instruction *ins = (*it);
        ++it;
        if(ins->get_bb() == NULL)
            continue;
        switch (ins->type)
        {
        case IR_INS_DIV:
        {
            ir_ins_div *top_ins = (ir_ins_div*)ins;
            instruction *nins;
            if(top_ins->op2->is_imme() && is_power2(top_ins->op2->get_v()))
            {
                int v = power2(top_ins->op2->get_v());
                if(v)
                {
                    nins = new ir_ins_shr(top_ins->op0, top_ins->op1, new vir_reg(v));
                    insert_ins_in_bb(it, nins);
                }
                top_ins->self_remove();
                break;
            }
            nins = new ir_ins_mov(pre_color_reg["rax"], top_ins->op1);
            insert_ins_in_bb(it, nins);
            vir_reg *divisor = top_ins->op2;
            if(divisor->is_imme())
            {
                divisor = prog->build_reg("[t][" + to_string(name_cnt++) + "]");
                nins = new ir_ins_mov(divisor, new vir_reg(top_ins->op2->get_v()));
                insert_ins_in_bb(it, nins);
            }
            nins = new ir_ins_div(pre_color_reg["rax"], pre_color_reg["rax"], divisor);
            insert_ins_in_bb(it, nins);
//            nins = new ir_ins_mov(pre_color_reg["rdx"], new vir_reg(0));
//            insert_ins_in_bb(it, nins);
            nins = new ir_ins_mov(top_ins->op0, pre_color_reg["rax"], true);
            insert_ins_in_bb(it, nins);
            top_ins->self_remove();
            break;
        }
        case IR_INS_MOD:
        {
            ir_ins_mod *top_ins = (ir_ins_mod*)ins;
            instruction *nins;
            nins = new ir_ins_mov(pre_color_reg["rax"], top_ins->op1);
            insert_ins_in_bb(it, nins);
            vir_reg *divisor = top_ins->op2;
            if(divisor->is_imme())
            {
                divisor = prog->build_reg("[t][" + to_string(name_cnt++) + "]");
                nins = new ir_ins_mov(divisor, new vir_reg(top_ins->op2->get_v()));
                insert_ins_in_bb(it, nins);
            }
            nins = new ir_ins_mod(pre_color_reg["rax"], pre_color_reg["rax"], divisor);
            insert_ins_in_bb(it, nins);
//            nins = new ir_ins_mov(pre_color_reg["rax"], new vir_reg(0));
//            insert_ins_in_bb(it, nins);
            nins = new ir_ins_mov(top_ins->op0, pre_color_reg["rdx"], true);
            insert_ins_in_bb(it, nins);
            top_ins->self_remove();
            break;
        }
        case IR_INS_SHL:
        {
            ir_ins_shl *top_ins = (ir_ins_shl*)ins;
            instruction *nins;
            nins = new ir_ins_mov(pre_color_reg["rcx"], top_ins->op2);
            insert_ins_in_bb(it, nins);
            nins = new ir_ins_shl(top_ins->op0, top_ins->op1, pre_color_reg["rcx"]);
            insert_ins_in_bb(it, nins);
            top_ins->self_remove();
            break;
        }
        case IR_INS_SHR:
        {
            ir_ins_shr *top_ins = (ir_ins_shr*)ins;
            instruction *nins;
            nins = new ir_ins_mov(pre_color_reg["rcx"], top_ins->op2);
            insert_ins_in_bb(it, nins);
            nins = new ir_ins_shr(top_ins->op0, top_ins->op1, pre_color_reg["rcx"]);
            insert_ins_in_bb(it, nins);
            top_ins->self_remove();
            break;
        }
        case IR_INS_CALL:
        {
            --it;

            ir_ins_call *top_ins = (ir_ins_call*)ins;
            instruction *nins;

            const list<vir_reg*> para_list = top_ins->get_para_list();
            list<vir_reg*>::const_iterator para_ite = para_list.cbegin();
            if(!_sim)
            {
                if(para_list.size() > 6)
                {
                    for(int i = 0; i < 6; ++i, ++para_ite);
                    int cnt = 1;
                    list<instruction*>::iterator it2 = it;
                    for(; para_ite != para_list.cend(); ++para_ite)
                    {
                        vir_reg *reg = *para_ite;
                        nins = new ir_ins_store(new vir_reg(cnt++), reg);
                        insert_ins_in_bb(it2, nins);
                        --it2;
                    }
                    if(para_list.size() % 2)
                    {
                        nins = new ir_ins_sub(prog->pre_color_reg["rsp"], prog->pre_color_reg["rsp"], new vir_reg(8));
                        insert_ins_in_bb(it2, nins);
                    }
                }
            }
            para_ite = para_list.cbegin();
            for(int i = 1; i <= 6 && para_ite != para_list.cend(); ++i)
            {
                vir_reg *reg = *para_ite;
                ++para_ite;
                nins = new ir_ins_mov(pre_color_reg["arg" + to_string(i)], reg);
                insert_ins_in_bb(it, nins);
                top_ins->replace_reg(reg, pre_color_reg["arg" + to_string(i)]);
            }
            vir_reg *def_reg = top_ins->get_def();
            ++it;
            if (def_reg != NULL)
            {
                nins = new ir_ins_mov(def_reg, pre_color_reg["res0"]);
                insert_ins_in_bb(it, nins);
                top_ins->replace_reg(def_reg, pre_color_reg["res0"]);
            }
            if(!_sim)
            {
                if(para_list.size() > 6)
                {
                    int k = para_list.size() - 6;
                    if(k % 2)
                        ++k;
                    nins = new ir_ins_add(prog->pre_color_reg["rsp"], prog->pre_color_reg["rsp"], new vir_reg(8 * k));
                    insert_ins_in_bb(it, nins);
                }
            }
            break;
        }
        case IR_INS_RET:
        {
            if(!_sim)
            {
                ir_ins_ret *top_ins = (ir_ins_ret*)ins;
                instruction *nins = new ir_ins_mov(pre_color_reg["res0"], top_ins->op);
                insert_ins_in_bb(it, nins);
                top_ins->self_remove();
            }
            break;
        }
        case IR_INS_STORE:
        {
            --it;
            ir_ins_store *top_ins = (ir_ins_store*)ins;
            if(top_ins->op1->is_global)
            {
                vir_reg *tmp_reg = prog->build_reg("[t][" + to_string(name_cnt++) + "]");
                instruction *nins = new ir_ins_mov(tmp_reg, top_ins->op1);
                insert_ins_in_bb(it, nins);
                top_ins->replace_reg(top_ins->op1, tmp_reg);
            }
            ++it;
            break;
        }
        default:
            break;
        }
    }
    instruction *nins = new ir_ins_mov(pre_color_reg["rax"], pre_color_reg["rax"]);
    list<instruction*>::iterator it2 = ins_list.end();
    --it2;
    insert_ins_in_bb(it2, nins);
    convert_to_2op();
}

string ir_ins_jmp::get_label()
{
    return label;
}

void ir::useless_jmp()
{
    set<string> valid_label;
    list<instruction*>::reverse_iterator it = ins_list.rbegin();
    for(; it != ins_list.rend(); ++it)
    {
        if((*it)->label.length())
        {
            valid_label.insert((*it)->label);
        }
        if((*it)->type == IR_INS_JMP)
        {
            ir_ins_jmp *top_ins = (ir_ins_jmp*)(*it);
            if(valid_label.count(top_ins->get_label()))
                top_ins->bb = NULL;
        }
        if((*it)->type != IR_INS_NULL && (*it)->bb != NULL)
            valid_label.clear();
    }
}

void prog_with_ir::useless_jmp()
{
    for(auto func: func_list)
        func->useless_jmp();
}

void ir::eval_cost()
{
    double cnt = 1.0;
    for(auto ins: ins_list)
    {
        if(ins->get_bb() == NULL)
            continue;
        if(ins->label.length())
        {
            string s = ins->label;

            if(s.substr(0, 11) == "[for_start]")
                cnt *= 10;
            if(s.substr(0, 10) == "[for_exit]")
                cnt /= 10;
            if(s.substr(0, 13) == "[while_start]")
                cnt *= 10;
            if(s.substr(0, 12) == "[while_exit]")
                cnt /= 10;
            if(s.substr(0, 10) == "[if_start]")
                cnt /= 2;
            if(s.substr(0, 8) == "[if_end]")
                cnt *= 2;
        }
        ins->eval_cost(cnt);
    }
}

void prog_with_ir::eval_cost()
{
    for(auto func: func_list)
        func->eval_cost();
}

void Basic_block::init_inter_graph(graph_coloring &_allocator)
{
    instruction *ins = end();
    set<vir_reg*> live_out, def_reg;
    for(auto _nxt: ins->nxt)
    {
        instruction *nxt = (instruction*)_nxt;
        if(nxt->get_bb() == NULL)
            continue;
        for(auto reg: ins->_export)
        {
            live_out.insert(reg);
        }
    }
    bool tmp_flag = 0;
    do
    {
        if(tmp_flag)
            ins = (instruction*)(*(ins->pre.begin()));
        tmp_flag = 1;
        if(ins->type == IR_INS_MOV 
        && !((ir_ins_mov*)ins)->is_sx 
        && ((ir_ins_mov*)ins)->op1->is_interable())
//        && !((ir_ins_mov*)ins)->op1->phi_v
//        && !((ir_ins_mov*)ins)->op0->phi_v)
        {
            ir_ins_mov *mov_ins = (ir_ins_mov*)ins;
            live_out.erase(mov_ins->get_use().front());
            _allocator.moveList[mov_ins->get_def()].insert(mov_ins);
            _allocator.moveList[mov_ins->get_use().front()].insert(mov_ins);
            _allocator.worklistMoves.insert(mov_ins);
        }

        def_reg.clear();
        if(ins->get_def() != NULL)
        {
            def_reg.insert(ins->get_def());
        }
        list<vir_reg*> use_reg = ins->get_use();


        if(ins->func != NULL)
        {
            prog_with_ir *prog = ins->func->prog;
            def_reg.erase(prog->pre_color_reg["rsp"]);
            list<vir_reg*>::iterator it = use_reg.begin();
            for(; it != use_reg.end(); )
            {
                list<vir_reg*>::iterator it2 = it++;
                if((*it2) == prog->pre_color_reg["rsp"])
                    use_reg.erase(it2);
            }
        }

        if(ins->type == IR_INS_DIV || ins->type == IR_INS_MOD)
        {
            prog_with_ir *prog = ins->func->prog;
            def_reg.insert(prog->pre_color_reg["rax"]);
            def_reg.insert(prog->pre_color_reg["rdx"]);
            for(auto reg: use_reg)
            {
                _allocator.AddEdge(prog->pre_color_reg["rax"], reg);
                _allocator.AddEdge(prog->pre_color_reg["rdx"], reg);
            }
        }

        if(ins->type == IR_INS_CALL)
        {
            prog_with_ir *prog = ins->func->prog;
            for(int i = 1; i <= 6; ++i)
                def_reg.insert(prog->pre_color_reg["arg" + to_string(i)]);
            def_reg.insert(prog->pre_color_reg["ler" + to_string(7)]);
            def_reg.insert(prog->pre_color_reg["ler" + to_string(8)]);
            def_reg.insert(prog->pre_color_reg["res0"]);
        }

        for(auto reg: def_reg)
            live_out.insert(reg);
        for(auto reg: def_reg)
            for(auto l_reg: live_out)
                _allocator.AddEdge(l_reg, reg);
        for(auto reg: def_reg)
            live_out.erase(reg);
        for(auto reg: use_reg)
        {
            if(!reg->is_interable())
                continue;
            if(reg->is_imme())
                continue;
            live_out.insert(reg);
        }
    } while (ins != begin());
    for(auto _bb: son)
    {
        Basic_block *bb = (Basic_block*)_bb;
        bb->init_inter_graph(_allocator);
    }
}

void ir::rewrite_program(graph_coloring &_allocator)
{
    while(!_allocator.spilledNodes.empty())
    {
        vir_reg *n = *(_allocator.spilledNodes.begin());
        printf("%s\n", n->get_name().c_str());
        _allocator.spilledNodes.erase(n);
        n->color = NONE;
        n->frame_add = frame_cnt++;

        printf("%s\n", n->get_name().c_str());

        set<instruction*> use_ins = n->get_use();
        for(auto ins: use_ins)
        {
            if(ins->type == IR_INS_CALL)
                continue;
            if(ins->type == IR_INS_MOV)
            {
                ir_ins_mov *mov_ins = (ir_ins_mov*)ins;
                if(mov_ins->op0 == mov_ins->op1)
                {
                    ins->self_remove();
                    continue;
                }
                instruction *nins = new ir_ins_load(mov_ins->op0, n);
                insert_ins_in_bb(ins->it, nins);
                ins->self_remove();
                continue;
            }
            vir_reg *tmp_reg = prog->build_reg("[t][" + to_string(name_cnt++) + "]");
            tmp_reg->eval = 1e16;
            instruction *nins = new ir_ins_load(tmp_reg, n);
            insert_ins_in_bb(ins->it, nins);
            if(ins->get_def() == n)
            {
                list<instruction*>::iterator it = ins->it;
                ++it;
                instruction *nins = new ir_ins_store(n, tmp_reg);
                insert_ins_in_bb(it, nins);
            }
            ins->replace_reg(n, tmp_reg);
        }

        set<instruction*> def_ins = n->get_def();
        for(auto ins: def_ins)
        {
            if(ins->type == IR_INS_CALL)
                continue;
            if(ins->type == IR_INS_MOV && !((ir_ins_mov*)ins)->op1->is_global)
            {
                ir_ins_mov *mov_ins = (ir_ins_mov*)ins;
                instruction *nins = new ir_ins_store(n, mov_ins->op1);
                insert_ins_in_bb(ins->it, nins);
                ins->self_remove();
                continue;
            }
            vir_reg *tmp_reg = prog->build_reg("[t][" + to_string(name_cnt++) + "]");
            tmp_reg->eval = 1e16;
            ins->replace_reg(n, tmp_reg);
            instruction *nins = new ir_ins_store(n, tmp_reg);
            list<instruction*>::iterator it = ins->it;
            ++it;
            insert_ins_in_bb(it, nins);
        }
    }
}

void ir::allocate()
{
    bool ok = false;
    do
    {
        printf("%s\n", label.c_str());
        graph_coloring _allotator;
        for(auto reg: prog->reg_list)
        {
            if(reg->pre_color)
            {
                _allotator.add_precolored_node(reg);
                continue;
            }
            if(reg->is_global)
                continue;
            if(reg->is_imme())
                continue;
            if(reg->get_def().size())
                if((*(reg->get_def().begin()))->func != this)
                    continue;
            if(reg->get_use().size())
                if((*(reg->get_use().begin()))->func != this)
                    continue;
            if(reg->get_def().size() == 0 && reg->get_use().size() == 0)
                continue;
            _allotator.add_initial_node(reg);
        }
        clean_flow_info();
        rebuild_ins_flow_graph();
        liveness_analysis();
        cfg->init_inter_graph(_allotator);
        _allotator.run();
        if(_allotator.spilledNodes.empty())
            ok = true;
        else
        {
            rewrite_program(_allotator);
        }
    }while(!ok);
}

void prog_with_ir::allocate()
{
    for(auto func: func_list)
        func->allocate();
}
