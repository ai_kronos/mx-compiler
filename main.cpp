#include<cstdio>
#include<cstdlib>
#include<fstream>
#include"parser.hpp"
#include"ir.h"
#include"RegTab.hpp"
#include"scope.h"
#include"ast.h"
#include"frontend.h"

using namespace std;

extern FILE *yyin;

RegTab ast;
ast_node *ast_root;
prog_with_ir lir;
int name_cnt;
void *mem;
int _debug = 0;
bool x86_reg_out = false;
bool _sim = false;

int main(int argc, char** argv)
{
    mem = malloc(100000);
    if (argc > 1)
    {
        for (int i = 1; i < argc; ++i)
        {
            yyin = fopen(argv[i], "r");
            yy::parser parser;
            try
            {
                parser.parse();
                printf("parse suc\n");
                semantic();
                printf("semmantic suc\n");
                ast_root->init_string(lir);
                ast_root->build_ir(lir, lir.get_first_func());
                lir.add_builtin_func();
                lir.remove_useless_call();
                lir.opt_inline(5);
                lir.replace_global();
                lir.move_init_ins();
                lir.init_label_set();
                printf("ir suc\n");

                lir.convert_to_ssa();
                /*
                printf("---------------ssa---------------\n");
                lir.print();
                printf("---------------------------------\n\n");
                */
                lir.mark_phi_v();
                lir.constant_pro();
                /*
                freopen("out1.txt", "w", stdout);
                printf("--------------no-color--------------\n");
                lir.print();
                printf("---------------------------------\n\n");
                fclose(stdout);
                */
                lir.copy_pro();
                lir.gvn();
                lir.copy_pro();
                /*
                freopen("out2.txt", "w", stdout);
                printf("----------------ir----------------\n"); 
                lir.print(); 
                printf("----------------------------------\n\n"); 
                fclose(stdout);
                */
                lir.ins_simplification();
                lir.add_constraint();
                lir.ssa_deconstruct();
                /*
                freopen("out2.txt", "w", stdout);
                printf("--------------deconstruct-----------\n");
                lir.print();
                printf("---------------------------------\n\n");
                fclose(stdout);
                */
                /*
                printf("------------ssa-sim-----------------\n");
                lir.sim();
                printf("------------------------------------\n\n");
                */
                lir.eval_cost();
                lir.allocate();

                x86_reg_out = true;
                /*
                freopen("out3.txt", "w", stdout);
                printf("-----------------color--------------\n");
                lir.print();
                printf("---------------------------------\n\n");
                fclose(stdout);
                */

                lir.useless_jmp();

                freopen("out3.asm", "w", stdout);
                lir.print_x86();
                fclose(stdout);

//                fclose(stdout);
            }
            catch(const char *s)
            {
                printf("%s\n", s);
                return 1;
            }
        }
    }
    return 0;
}