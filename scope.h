#ifndef _scope_h
#define _scope_h

#include"RegTab.hpp"
#include"type.h"
#include"trie.hpp"
#include"enum.h"

class scope: public BaseClass
{
  public:
    static string trans(const string&);

  private:
    trie<type_struct> *ttrie;
    trie<bool> *ntrie;
    trie<scope*> *strie;
    trie<string> *fun_trie;

  public:
    scope();
    scope(const scope &, bool = true);
    trie<type_struct> &get_tTrie();
    trie<scope*> &get_sTrie();
    trie<bool> &get_nTrie();
    trie<string> &get_fun_trie();
    virtual ~scope();
};

#endif