#include "allocation.h"
#include "phyreg_enum.h"

void graph_coloring::add_precolored_node(vir_reg *u)//checked
{
    precolored.insert(u);
    degree[u] = 0;
}

void graph_coloring::add_initial_node(vir_reg *u)//checked
{
    initial.insert(u);
    degree[u] = 0;
}

void graph_coloring::AddEdge(vir_reg *u, vir_reg *v)//checked
{
    if(adjSet.count(make_pair(u, v)) == 0 && u != v)
    {
        adjSet.insert(make_pair(u, v));
        adjSet.insert(make_pair(v, u));
        if(precolored.count(u) == 0)
        {
            adjList[u].insert(v);
            degree[u] = degree[u] + 1;
        }
        if(precolored.count(v) == 0)
        {
            adjList[v].insert(u);
            degree[v] = degree[v] + 1;
        }
    }
}

void graph_coloring::MakeWorklist()//checked
{
    set<vir_reg*>::iterator it = initial.begin();
    for(; it != initial.end(); )
    {
        vir_reg *n = *it;
        it = initial.erase(it);
        if(degree[n] >= K)
            spillWorklist.insert(n);
        else if(MoveRelated(n))
            freezeWorklist.insert(n);
        else
            simplifyWorklist.insert(n);
    }
}

set<vir_reg*> graph_coloring::Adjacent(vir_reg *n)//checked
{
    set<vir_reg*> ret;
    for(auto reg: adjList[n])
    {
        if(selectStack_set.count(reg))
            continue;
        if(coalescedNodes.count(reg))
            continue;
        ret.insert(reg);
    }
    return ret;
}

set<ir_ins_mov*> graph_coloring::NodeMoves(vir_reg *n)//checked
{
    set<ir_ins_mov*> ret = moveList[n];
    set<ir_ins_mov*>::iterator it = ret.begin();
    for(; it != ret.end(); )
    {
        if(activeMoves.count(*it) == 0 && worklistMoves.count(*it) == 0)
            it = ret.erase(it);
        else
            ++it;
    }
    return ret;
}

bool graph_coloring::MoveRelated(vir_reg *n)//checked
{
    return !NodeMoves(n).empty();
}

void graph_coloring::Simplify()//checked
{
//    printf("Simplify\n");
    set<vir_reg*>::iterator it = simplifyWorklist.begin();
    vir_reg *n = *it;
    it = simplifyWorklist.erase(it);
    selectStack.push_back(n);
    selectStack_set.insert(n);
    for(auto reg: adjList[n])
    {
        if(selectStack_set.count(reg))
            continue;
        if(coalescedNodes.count(reg))
            continue;
        DecrementDegree(reg);
    }
}

void graph_coloring::DecrementDegree(vir_reg *m)//checked
{
    int d = degree[m];
    degree[m] = d - 1;
    if(d == K)
    {
        set<vir_reg*> adjacent = Adjacent(m);
        adjacent.insert(m);
        EnableMoves(adjacent);
        spillWorklist.erase(m);
        if(MoveRelated(m))
            freezeWorklist.insert(m);
        else
            simplifyWorklist.insert(m);
    }
}

void graph_coloring::EnableMoves(set<vir_reg*> nodes)//checked
{
    for(auto n: nodes)
    {
        set<ir_ins_mov*> nodemoves = NodeMoves(n);
        for(auto m: nodemoves)
        {
            if(activeMoves.count(m))
            {
                activeMoves.erase(m);
                worklistMoves.insert(m);
            }
        }
    }
}

void graph_coloring::AddWorkList(vir_reg *u)//checked
{
    if(precolored.count(u) == 0 && !MoveRelated(u) && degree[u] < K)
    {
        freezeWorklist.erase(u);
        simplifyWorklist.insert(u);
    }
}

bool graph_coloring::OK(vir_reg *t, vir_reg *r)//checked. but why?
{
    return degree[t] < K || precolored.count(t) || adjSet.count(make_pair(t, r));
}

/*
bool graph_coloring::Conservative(set<vir_reg*> nodes)//checked
{
    int k = 0;
    for(auto n: nodes)
        if(degree[n] >= K)
            ++k;
    return k < K;
}
*/

bool graph_coloring::judge(vir_reg *u, vir_reg *v)
{
    set<vir_reg*> adj = Adjacent(v);
    for(auto t: adj)
        if(!OK(t, u))
            return false;
    return true;
}

void graph_coloring::Coalesce()//except condition
{
//    printf("Coalesce\n");
    ir_ins_mov *m = *(worklistMoves.begin());
    vir_reg *x = GetAlias(m->op0);
    vir_reg *y = GetAlias(m->op1);
    vir_reg *u, *v;
    if(precolored.count(y))
    {
        u = y;
        v = x;
    }
    else
    {
        u = x;
        v = y;
    }
    worklistMoves.erase(m);

    if(u == v)
    {
        coalescedMoves.insert(m);
        AddWorkList(u);
    }
    else if(precolored.count(v) || adjSet.count(make_pair(u, v)))
    {
        constrainedMoves.insert(m);
        AddWorkList(u);
        AddWorkList(v);
    }
    else
    {
        if(precolored.count(u) && judge(u, v))
        {
            coalescedMoves.insert(m);
            Combine(u, v);
            AddWorkList(u);
        } 
        else
        {
            if(degree[u] > 30 || degree[v] > 30)
            {
                activeMoves.insert(m);
            }
            else
            {
                int k = 0;
                for(auto reg: adjList[u])
                {
                    if(selectStack_set.count(reg))
                        continue;
                    if(coalescedNodes.count(reg))
                        continue;
                    if(degree[reg] >= K)
                        ++k;
                }
                for(auto reg: adjList[v])
                {
                    if(selectStack_set.count(reg))
                        continue;
                    if(coalescedNodes.count(reg))
                        continue;
                    if(adjList[u].count(reg))
                        continue;
                    if(degree[reg] >= K)
                        ++k;
                }
                if(precolored.count(u) == 0 && k < K)
                {
                    coalescedMoves.insert(m);
                    Combine(u, v);
                    AddWorkList(u);
                }
                else
                {
                    activeMoves.insert(m);
                }
            }
        }
    }
    
}

void graph_coloring::Combine(vir_reg *u, vir_reg *v)//checked
{
    if(freezeWorklist.count(v))
        freezeWorklist.erase(v);
    else
        spillWorklist.erase(v);

    coalescedNodes.insert(v);
    alias[v] = u;
    set<ir_ins_mov*> movelist = moveList[v];
    for(auto reg: movelist)
        moveList[u].insert(reg);
    movelist.clear();
    set<vir_reg*> tmp;
    tmp.insert(v);
    EnableMoves(tmp);
    tmp = Adjacent(v);
    for(auto t: tmp)
    {
        AddEdge(t, u);
        DecrementDegree(t);
    }
    if(degree[u] >= K && freezeWorklist.count(u))
    {
        freezeWorklist.erase(u);
        spillWorklist.insert(u);
    }
}

vir_reg *graph_coloring::GetAlias(vir_reg *n)
{
    if(coalescedNodes.count(n))
        return GetAlias(alias[n]);
    else
        return n;
}

void graph_coloring::Freeze()
{
//    printf("Freeze\n");
    vir_reg *u = *(freezeWorklist.begin());
    freezeWorklist.erase(u);
    simplifyWorklist.insert(u);
    FreezeMoves(u);
}

void graph_coloring::FreezeMoves(vir_reg *u)
{
    set<ir_ins_mov*> nodemoves = NodeMoves(u);
    vir_reg *v;
    for(auto m: nodemoves)
    {
        if(GetAlias(m->op1) == GetAlias(u))
            v = GetAlias(m->op0);
        else
            v = GetAlias(m->op1);
        activeMoves.erase(m);
        frozenMoves.insert(m);
        if(freezeWorklist.count(v) && NodeMoves(v).empty())
        {
            freezeWorklist.erase(v);
            simplifyWorklist.insert(v);
        }
    }
}

void graph_coloring::SelectSpill()
{
//    printf("SelectSpill\n");
    vir_reg *spill_reg;
    double cost = 1e17;
    for(auto reg: spillWorklist)
    {
        if(reg->eval < cost)
        {
            cost = reg->eval;
            spill_reg = reg;
        }
    }
    spillWorklist.erase(spill_reg);
    simplifyWorklist.insert(spill_reg);
    FreezeMoves(spill_reg);
}

void graph_coloring::AssignColors()
{
    while(!selectStack.empty())
    {
        vir_reg *n = selectStack.back();
        selectStack.pop_back();
        selectStack_set.erase(n);
        set<phyreg> okColors;
        for(int i = 1; i <= 15; ++i)
            okColors.insert((phyreg)i);
        set<vir_reg*> adj = adjList[n];
        for(auto w: adj)
        {
            if(precolored.count(GetAlias(w)) || coloredNodes.count(GetAlias(w)))
                okColors.erase(GetAlias(w)->color);
        }
        if(okColors.empty())
        {
            spilledNodes.insert(n);
        }
        else
        {
            coloredNodes.insert(n);
            n->color = *(okColors.begin());
        }
    }
    for(auto n: coalescedNodes)
    {
        n->color = GetAlias(n)->color;
    }
}

void graph_coloring::run()
{
    MakeWorklist();
    do
    {
        if(!simplifyWorklist.empty())
            Simplify();
        else if(!worklistMoves.empty())
            Coalesce();
        else if(!freezeWorklist.empty())
            Freeze();
        else if(!spillWorklist.empty())
            SelectSpill();
    } while (!(simplifyWorklist.empty() && worklistMoves.empty() && freezeWorklist.empty() && spillWorklist.empty()));
    AssignColors();
}

void graph_coloring::re_addin()
{
    for(auto reg: coloredNodes)
        initial.insert(reg);
    coloredNodes.clear();
    for(auto reg: coalescedNodes)
        initial.insert(reg);
    initial.clear();
}