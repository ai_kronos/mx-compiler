#ifndef _vir_reg_h
#define _vir_reg_h

#include<string>
#include<set>
#include"phyreg_enum.h"

using namespace std;

class instruction;
class prog_with_ir;

class vir_reg
{
  private:
    string name;
    int imm;
    bool is_imm;
    long long val;
    set<instruction*> use, def;
    bool del_flag;

  public:
    int is_global;
    double eval = 0;
    vir_reg *phi_coalesce = this;
    bool phi_v = false;
    bool pre_color;
    bool is_str = false;
    phyreg color; 
    int frame_add = -1;
    bool _atomic = false;

  public:
    vir_reg() = delete;
    vir_reg(const string);
    vir_reg(const string, phyreg);
    vir_reg(const int);
    string get_name(bool = false);
    long long get_v();
    void set_v(long long);
    void set_use(instruction*);
    void set_def(instruction*);
    void remove_use(instruction*);
    void remove_def(instruction*);
    int get_use_size();
    vir_reg *clone(prog_with_ir*, int);
    bool is_imme();
    const set<instruction*> &get_use();
    const set<instruction*> &get_def();
    void replace(vir_reg*);
    vir_reg *get_phi_coalesce_reg();

    void set_imme();
    bool is_interable();
};

#endif