#include"scope.h"

string scope::trans(const string &str)
{
    string s;
    int siz = str.length();
    for(int i = 0; i < siz; ++i)
    {
        if('0' <= str[i] && str[i] <= '9')
            s.push_back(str[i] - 48);
        else if('a' <= str[i] && str[i] <= 'z')
            s.push_back(str[i] - 87);
        else if('A' <= str[i] && str[i] <= 'Z')
            s.push_back(str[i] - 29);
        else if(str[i] == '[')
            s.push_back(62);
        else if(str[i] == ']')
            s.push_back(63);
        else
            s.push_back(64);
    }
    return s;
}

scope::scope()
{
    strie = new trie<scope*>(65);
    ttrie = new trie<type_struct>(65);
    ntrie = new trie<bool>(65);
    fun_trie = new trie<string>(65);
    strie->bind_trans_func(trans);
    ttrie->bind_trans_func(trans);
    ntrie->bind_trans_func(trans);
    fun_trie->bind_trans_func(trans);
}

scope::~scope()
{
    delete strie;
    delete ttrie;
    delete ntrie;
}

scope::scope(const scope &ob, bool new_sp)
{
    strie = ob.strie;
    ttrie = new trie<type_struct>(*ob.ttrie);
    if(new_sp)
    {
        ntrie = new trie<bool>(*ob.ntrie);
        fun_trie = new trie<string>(*ob.fun_trie);
    }
    else
    {
        ntrie = ob.ntrie;
        fun_trie = ob.fun_trie;
    }
    
}

trie<type_struct> &scope::get_tTrie()
{
    return *ttrie;
}

trie<scope*> &scope::get_sTrie()
{
    return *strie;
}

trie<bool> &scope::get_nTrie()
{
    return *ntrie;
}

trie<string> &scope::get_fun_trie()
{
    return *fun_trie;
}