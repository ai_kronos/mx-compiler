#include "ir.h"

extern int name_cnt;

void prog_with_ir::add_builtin_func()
{
    add_strcat();
    add_strlst();
    add_strlet();
    add_strequ();
    add_strneq();

    ir *func = get_first_func();

    string s = "[t][" + to_string(name_cnt++) + "]";
    string_format = build_reg(s);
    instruction *ins = new ir_ins_glb(string_format, "%s");
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    int_format = build_reg(s);
    ins = new ir_ins_glb(int_format, "%lld");
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    buff = build_reg(s);
    ins = new ir_ins_glb(buff);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *buff_size = build_reg(s);
    ins = new ir_ins_mov(buff_size, new vir_reg(256));
    func->append_inst(ins);
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *tmp = build_reg(s);
    ins = new ir_ins_call("]malloc", tmp);
    func->append_inst(ins);
    ((ir_ins_call*)ins)->append_para(buff_size);
    ins = new ir_ins_store(buff, tmp);
    func->append_inst(ins);

    add_getInt();
    add_getString();
    add_print();
    add_println();
    add_toString();

    add_arr_size();

    add_str_length();
    add_str_substring();
    add_str_parseInt();
    add_str_ord();
}

void prog_with_ir::add_strcat()
{
    build_func("]]strcat");
    ir* func = get_last_func();
    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in0 = build_reg(s);
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in1 = build_reg(s);
    func->append_para(in0);
    func->append_para(in1);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len0_p = build_reg(s);
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len1_p = build_reg(s);

    instruction *ins = new ir_ins_sub(len0_p, in0, new vir_reg(8));
    func->append_inst(ins);
    ins = new ir_ins_sub(len1_p, in1, new vir_reg(8));
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len0 = build_reg(s);
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len1 = build_reg(s);

    ins = new ir_ins_load(len0, len0_p);
    func->append_inst(ins);
    ins = new ir_ins_load(len1, len1_p);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len = build_reg(s);
    ins = new ir_ins_add(len, len0, len1);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *malloc_len = build_reg(s);
    ins = new ir_ins_add(malloc_len, len, new vir_reg(9));
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *malloc_add = build_reg(s);
    ins = new ir_ins_call("]malloc", malloc_add);
    ((ir_ins_call*)ins)->append_para(malloc_len);
    func->append_inst(ins);

    ins = new ir_ins_store(malloc_add, len);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *str_add = build_reg(s);
    ins = new ir_ins_add(str_add, malloc_add, new vir_reg(8));
    func->append_inst(ins);

    ins = new ir_ins_call("]strcpy");
    ((ir_ins_call*)ins)->append_para(str_add);
    ((ir_ins_call*)ins)->append_para(in0);
    func->append_inst(ins);

    ins = new ir_ins_call("]strcat");
    ((ir_ins_call*)ins)->append_para(str_add);
    ((ir_ins_call*)ins)->append_para(in1);
    func->append_inst(ins);

    ins = new ir_ins_ret(str_add);
    func->append_inst(ins);
}

void prog_with_ir::add_strlst()
{
    build_func("]]strlst");
    ir* func = get_last_func();
    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in0 = build_reg(s);
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in1 = build_reg(s);
    func->append_para(in0);
    func->append_para(in1);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *result = build_reg(s);
    instruction *ins = new ir_ins_call("]strcmp", result);
    ((ir_ins_call*)ins)->append_para(in0);
    ((ir_ins_call*)ins)->append_para(in1);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    ins = new ir_ins_lst(ret, result, new vir_reg(0));
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_strlet()
{
    build_func("]]strlet");
    ir* func = get_last_func();
    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in0 = build_reg(s);
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in1 = build_reg(s);
    func->append_para(in0);
    func->append_para(in1);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *result = build_reg(s);
    instruction *ins = new ir_ins_call("]strcmp", result);
    ((ir_ins_call*)ins)->append_para(in0);
    ((ir_ins_call*)ins)->append_para(in1);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    ins = new ir_ins_let(ret, result, new vir_reg(0));
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_strequ()
{
    build_func("]]strequ");
    ir* func = get_last_func();
    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in0 = build_reg(s);
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in1 = build_reg(s);
    func->append_para(in0);
    func->append_para(in1);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *result = build_reg(s);
    instruction *ins = new ir_ins_call("]strcmp", result);
    ((ir_ins_call*)ins)->append_para(in0);
    ((ir_ins_call*)ins)->append_para(in1);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    ins = new ir_ins_equ(ret, result, new vir_reg(0));
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_strneq()
{
    build_func("]]strneq");
    ir* func = get_last_func();
    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in0 = build_reg(s);
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in1 = build_reg(s);
    func->append_para(in0);
    func->append_para(in1);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *result = build_reg(s);
    instruction *ins = new ir_ins_call("]strcmp", result);
    ((ir_ins_call*)ins)->append_para(in0);
    ((ir_ins_call*)ins)->append_para(in1);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    ins = new ir_ins_neq(ret, result, new vir_reg(0));
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_getInt()
{
    build_func("getInt");
    ir *func = get_last_func();
    instruction *ins = new ir_ins_call("]scanf");
    ((ir_ins_call*)ins)->append_para(int_format);
    ((ir_ins_call*)ins)->append_para(buff);
    func->append_inst(ins);

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    ins = new ir_ins_load(ret, buff);
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_getString()
{
    build_func("getString");
    ir *func = get_last_func();
    instruction *ins = new ir_ins_call("]scanf");
    ((ir_ins_call*)ins)->append_para(string_format);
    ((ir_ins_call*)ins)->append_para(buff);
    func->append_inst(ins);

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len = build_reg(s);
    ins = new ir_ins_call("]strlen", len);
    ((ir_ins_call*)ins)->append_para(buff);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *malloc_len = build_reg(s);
    ins = new ir_ins_add(malloc_len, len, new vir_reg(9));
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *malloc_add = build_reg(s);
    ins = new ir_ins_call("]malloc", malloc_add);
    ((ir_ins_call*)ins)->append_para(malloc_len);
    func->append_inst(ins);

    ins = new ir_ins_store(malloc_add, len);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    ins = new ir_ins_add(ret, malloc_add, new vir_reg(8));
    func->append_inst(ins);

    ins = new ir_ins_call("]strcpy");
    ((ir_ins_call*)ins)->append_para(ret);
    ((ir_ins_call*)ins)->append_para(buff);
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_print()
{
    build_func("print");
    ir *func = get_last_func();

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in = build_reg(s);
    func->append_para(in);

    instruction *ins = new ir_ins_call("]printf");
    ((ir_ins_call*)ins)->append_para(string_format);
    ((ir_ins_call*)ins)->append_para(in);
    func->append_inst(ins);
}

void prog_with_ir::add_println()
{
    build_func("println");
    ir *func = get_last_func();

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in = build_reg(s);
    func->append_para(in);

    instruction *ins = new ir_ins_call("]puts");
    ((ir_ins_call*)ins)->append_para(in);
    func->append_inst(ins);
}

void prog_with_ir::add_toString()
{
    build_func("toString");
    ir *func = get_last_func();

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in = build_reg(s);
    func->append_para(in);
    
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *malloc_siz = build_reg(s);
    instruction *ins = new ir_ins_mov(malloc_siz, new vir_reg(19));
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *malloc_add = build_reg(s);
    ins = new ir_ins_call("]malloc", malloc_add);
    ((ir_ins_call*)ins)->append_para(malloc_siz);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    ins = new ir_ins_add(ret, malloc_add, new vir_reg(8));
    func->append_inst(ins);

    ins = new ir_ins_call("]sprintf");
    func->append_inst(ins);
    ((ir_ins_call*)ins)->append_para(ret);
    ((ir_ins_call*)ins)->append_para(int_format);
    ((ir_ins_call*)ins)->append_para(in);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len = build_reg(s);
    ins = new ir_ins_call("]strlen", len);
    ((ir_ins_call*)ins)->append_para(ret);
    func->append_inst(ins);

    ins = new ir_ins_store(malloc_add, len);
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_arr_size()
{
    build_func("[]size");
    ir *func = get_last_func();

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in = build_reg(s);
    func->append_para(in);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len_p = build_reg(s);
    instruction *ins = new ir_ins_sub(len_p, in, new vir_reg(8));
    func->append_inst(ins);
    
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);

    ins = new ir_ins_load(ret, len_p);
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_str_length()
{
    build_func("[string]length");
    ir *func = get_last_func();

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in = build_reg(s);
    func->append_para(in);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len_p = build_reg(s);
    instruction *ins = new ir_ins_sub(len_p, in, new vir_reg(8));
    func->append_inst(ins);
    
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);

    ins = new ir_ins_load(ret, len_p);
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_str_substring()
{
    build_func("[string]substring");
    ir *func = get_last_func();

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in = build_reg(s);
    func->append_para(in);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *left = build_reg(s);
    func->append_para(left);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *right = build_reg(s);
    func->append_para(right);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *len = build_reg(s);
    instruction *ins = new ir_ins_sub(len, right, left);
    func->append_inst(ins);

    ins = new ir_ins_add(len, len, new vir_reg(1));
    func->append_inst(ins);
    
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *malloc_len = build_reg(s);
    ins = new ir_ins_add(malloc_len, len, new vir_reg(9));
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *malloc_add = build_reg(s);
    ins = new ir_ins_call("]malloc", malloc_add);
    func->append_inst(ins);
    ((ir_ins_call*)ins)->append_para(malloc_len);

    ins = new ir_ins_store(malloc_add, len);
    func->append_inst(ins);
    
    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    ins = new ir_ins_add(ret, malloc_add, new vir_reg(8));
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *copy_base_add = build_reg(s);
    ins = new ir_ins_add(copy_base_add, in, left);
    func->append_inst(ins);

    ins = new ir_ins_call("]memcpy");
    func->append_inst(ins);
    ((ir_ins_call*)ins)->append_para(ret);
    ((ir_ins_call*)ins)->append_para(copy_base_add);
    ((ir_ins_call*)ins)->append_para(len);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_str_parseInt()
{
    build_func("[string]parseInt");
    ir *func = get_last_func();

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in = build_reg(s);
    func->append_para(in);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    instruction *ins = new ir_ins_call("]atoi", ret);
    ((ir_ins_call*)ins)->append_para(in);
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}

void prog_with_ir::add_str_ord()
{
    build_func("[string]ord");
    ir *func = get_last_func();

    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *in = build_reg(s);
    func->append_para(in);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *pos = build_reg(s);
    func->append_para(pos);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *add = build_reg(s);
    instruction *ins = new ir_ins_add(add, in, pos);
    func->append_inst(ins);

    s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *ret = build_reg(s);
    ins = new ir_ins_load(ret, add, true);
    func->append_inst(ins);

    ins = new ir_ins_ret(ret);
    func->append_inst(ins);
}