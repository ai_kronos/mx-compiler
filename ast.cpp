#include "RegTab.hpp"
#include "ast.h"
#include "tools.h"
#include<iostream>

using namespace std;

extern int name_cnt;

type_struct ast_node::re_type;
type_struct ast_node::building_class;
int ast_node::loop_cnt = 0;
bool ast_node::cons_valid = false;
bool ast_node::in_paralist = false;
bool ast_node::in_class = false;
int ast_node::in_func = 0;
int ast_node::var_cnt = 0;
vir_reg* ast_node::this_reg = NULL;
list<string> ast_node::entry_label;
list<string> ast_node::exit_label;
list<string> ast_node::set0;
list<string> ast_node::set1;
string ast_node::exit_block;

ast_node::ast_node()
{
    id = 0;
    valid = false;
    it = child.begin();
}

void ast_node::build()
{
    int siz = size();
    for(int i = 0; i < siz; ++i)
    {
        (operator[](i))->sp = sp;
        (operator[](i))->build();
    }
}

void ast_node::init_string(prog_with_ir &lir)
{
    int siz = size();
    for(int i = 0 ; i < siz; ++i)
        (operator[](i))->init_string(lir);
}

void ast_node::build_ir(prog_with_ir &lir, ir* func)
{
    int siz = size();
    for(int i = 0; i < siz; ++i)
        (operator[](i))->build_ir(lir, func);
}

void ast_node::type_check()
{
    int siz = size();
    for(int i = 0; i < siz; ++i)
        (operator[](i))->type_check();
}

ast_node::~ast_node() {};

size_t ast_node::size()
{
    return child.size();
}

void ast_node::appendChild(initializer_list<ast_node *> li)
{
    for (auto ptr(li.begin()); ptr != li.end(); ++ptr)
        child.push_back(*ptr);
}

void ast_node::merge(ast_node *p)
{
    p->valid = false;
    child.splice(child.end(), p->child);
}

ast_node *ast_node::operator[](size_t num)
{
    if (!valid)
    {
        if (num + num + 2 > child.size() + 1)
        {
            id = child.size();
            it = child.end();
        }
        else
        {
            id = 0;
            it = child.begin();
        }
        valid = true;
    }
    size_t sig(id > num ? -1 : 1);
    for (; id != num; id += sig)
    {
        if (id > num)
            --it;
        else
            ++it;
    }
    return *it;
}

node_PROGRAM::~node_PROGRAM() {};

node_type node_PROGRAM::getType()
{
    return PROGRAM;
}

node_FUNCDEF::~node_FUNCDEF() {};

void node_FUNCDEF::build()
{
    if(id == "this")
        throw("'this' cannot be the function name.");
    if (type_info.getStr() == "[]")
    {
        if(building_class.getClassname() != id)
        {
            throw("Invalid constructor function.");
        }
    }
    if (building_class.getClassname() == id)
    {
        if (type_info.getStr() != "[]")
            throw("return type of constructor in disable");
        type_info.init("void", "", true);
    }
    type_struct t;
    t.init_func(id, "");
    if(in_class)
        t.is_memb_func = 1;
    (sp->get_tTrie()).insert(id, t);
    (sp->get_nTrie()).insert(id, true);
    string s = "[" + to_string(CALL) + "]" + t.getStr();
    string para = "";
    node_PARADEF *p = (node_PARADEF*)operator[](0);
    int siz = p->size();
    for(int i = 0; i < siz; ++i)
    {
        s = s + ((node_VARDEF*)((*p)[i]))->type_info.getStr();
        para = para + ((node_VARDEF*)((*p)[i]))->type_info.getStr();
    }
    (sp->get_tTrie()).insert(s, type_info);
    (sp->get_fun_trie()).insert(t.getStr(), para);

    scope *tmp = ast.create<scope>(*sp);
    sp = tmp;
    ast_node::build();
}

node_type node_FUNCDEF::getType()
{
    return FUNCDEF;
}

void node_FUNCDEF::type_check()
{
    if(type_info.is_class() && !(sp->get_sTrie()).is_defined(type_info.getClassname()))
        throw("Function type is undefined.");
    re_type = type_info;
    ast_node::type_check();
}

void node_FUNCDEF::init(const string &t, const string &arr, const string &name, ast_node *para, ast_node *stat)
{
    type_info.init(t, arr, true);
    id = name;
    appendChild({para, stat});
}

void node_FUNCDEF::build_ir(prog_with_ir &lir, ir* func)
{
    string s = "[t][" + to_string(name_cnt++) + "]";
    this_reg = lir.build_reg(s);

    exit_block = "[exit_block][" + to_string(name_cnt++) + "]";

    if(!in_class)
        lir.build_func(id);
    else
    {
        lir.build_func("[" + building_class.getClassname() + "]" + id);
        lir.get_last_func()->append_para(this_reg);
    }

    ++in_func;
    ast_node::build_ir(lir, lir.get_last_func());
    --in_func;

    instruction *ins = new ir_ins_jmp(exit_block);
    lir.get_last_func()->append_inst(ins);

    ins = new instruction;
    ins->label = exit_block;
    lir.get_last_func()->append_inst(ins);
}

node_VARDEF::~node_VARDEF() {};

node_type node_VARDEF::getType()
{
    return VARDEF;
}

void node_VARDEF::init(const string &t, const string &arr, const string &name, ast_node* expr)
{
    type_info.init(t, arr, false);
    cout << "vardef: " << id << " " << type_info.getStr() << endl;
    id = name;
    if (expr != NULL)
        appendChild({expr});
}

void node_VARDEF::type_check()
{
    ast_node::type_check();
    string s = type_info.getStr();
    if(s.substr(0, 5) == "[null" || s.substr(0, 5) == "[void")
        throw("Invalid type.");
    if(type_info.is_class() && !(sp->get_sTrie()).is_defined(type_info.getClassname()))
    {
        throw("Variable type is undefined.");
    }
    int siz = size();
    if(siz != 0)
    {
        node_EXPR *p = (node_EXPR*)operator[](0);
        if(p->op_info == SPER)
            throw("Wrong initialized value.");
        cout << p->type_info.getStr() << ' ' << type_info.getStr() << endl;
        if(!(p->type_info == type_info))
        {
            if(p->type_info.getStr() == "[null]" && type_info.is_ref() && (type_info.getStr() != "[string]" || type_info.get_level()))
                return;
            cout << id << endl;
            throw("Unmatched initialized value.");
        }
    }
}

void node_VARDEF::build()
{
    ast_node::build();
    (sp->get_tTrie()).insert(id, type_info);
    (sp->get_nTrie()).insert(id, true);
    type_struct &t((sp->get_tTrie()).find(id));
    if(building_class.getStr() != "[]")
        t.no_in_class = var_cnt++;
}

void node_VARDEF::build_ir(prog_with_ir &lir, ir* func)
{
    if(in_class && !in_func && !in_paralist)
        return;

    string reg_name = id + "[" + to_string(name_cnt++) + "]";
    type_struct t((sp->get_tTrie()).find(id));
    t.set_reg(reg_name);
    vir_reg *r = lir.build_reg(reg_name);

    if(!in_func)
    {
        t.is_global = 1;
        instruction *ins(new ir_ins_glb(r));
        func->append_inst(ins);
    }
    sp->get_tTrie().insert(id, t, true);
    
    if(in_paralist)
        func->append_para(r);

    if(size())
    {
        ast_node::build_ir(lir, func);

        node_EXPR *expr((node_EXPR*)(operator[](0)));
        if(in_func)
        {
            instruction *ins(new ir_ins_mov(r, expr->load_reg(lir, func)));
            func->append_inst(ins);
        }
        else
        {
            instruction *ins(new ir_ins_store(r, expr->load_reg(lir, func)));
            func->append_inst(ins);
        }
        
    }
}

node_CLASSDEF::~node_CLASSDEF() {};

void node_CLASSDEF::build_ir(prog_with_ir &lir, ir* func)
{
    in_class = true;
    building_class = (sp->get_tTrie()).find(id);
    ast_node::build_ir(lir, func);
    in_class = false;
}

node_type node_CLASSDEF::getType()
{
    return CLASSDEF;
}

void node_CLASSDEF::init(string &name, ast_node *li)
{
    id = name;
    if (li != NULL)
        merge(li);
}

void node_CLASSDEF::build()
{
    int siz = size();
    type_struct t;
    t.init(id, "", false);
    building_class = t;
    var_cnt = 0;
    scope *tmp = ast.create<scope>(*sp);
    scope *bak_sp = sp;
    (sp->get_tTrie()).insert(id, t);
    (sp->get_nTrie()).insert(id, true);
    (sp->get_tTrie()).insert("[31][" + id + "]", t);
    (sp->get_sTrie()).insert(id, tmp);
    sp = tmp;
    in_class = 1;
    ast_node::build();
    in_class = 0;
    type_struct &ty((bak_sp->get_tTrie()).find(id));
    ty.no_in_class = var_cnt;
    (bak_sp->get_tTrie()).insert(id, ty, true);
    building_class.init("[]", "", false);
}

node_PARADEF::~node_PARADEF() {};

node_type node_PARADEF::getType()
{
    return PARADEF;
}

void node_PARADEF::build_ir(prog_with_ir &lir, ir* func)
{
    in_paralist = 1;
    ast_node::build_ir(lir, func);
    in_paralist = 0;
}

node_STATEMENT::~node_STATEMENT() {};

node_type node_STATEMENT::getType()
{
    return STATEMENT;
}

void node_STATEMENT::init(bool ns, ast_node *ob)
{
    new_scope = ns;
    if (ns)
        merge(ob);
    else
        appendChild({ob});
}

void node_STATEMENT::build()
{
    if(new_scope)
    {
        scope *tmp = ast.create<scope>(*sp);
        sp = tmp;
    }
    int siz = size();
    for(int i = 0; i < siz; ++i)
    {
        if((operator[](i))->getType() == VARDEF)
        {
            scope *tmp = ast.create<scope>(*sp, false);
            sp = tmp;
        }
        (operator[](i))->sp = sp;
        (operator[](i))->build();
    }
}

node_CLASSPARADEF::~node_CLASSPARADEF() {};

node_type node_CLASSPARADEF::getType()
{
    return CLASSPARADEF;
}

void node_CLASSPARADEF::init(ast_node *di, ast_node *li)
{
    appendChild({di});
    if (li != NULL)
        merge(li);
}

node_PARADEFLIST::~node_PARADEFLIST() {};

node_type node_PARADEFLIST::getType()
{
    return PARADEFLIST;
}

void node_PARADEFLIST::init(ast_node *di, ast_node *li)
{
    if (di != NULL)
        appendChild({di});
    if (li != NULL)
        merge(li);
}

node_IF::~node_IF() {};

node_type node_IF::getType()
{
    return IF;
}

void node_IF::build()
{
    sp = ast.create<scope>(*sp);
    ast_node::build();
}

void node_IF::type_check()
{
    ast_node::type_check();
    node_EXPR *p = (node_EXPR*)operator[](0);
    if(p->type_info.getStr() != "[bool]")
        throw("Condition in IF statement must be bool.");
}

void node_IF::build_ir(prog_with_ir &lir, ir* func)
{
    int t_siz = size();
    string label0 = "[if_iner][" + to_string(name_cnt++) + "]";
    string label1 = "[if_exit][" + to_string(name_cnt++) + "]";
    string label2 = "[if_start][" + to_string(name_cnt++) + "]";
    node_EXPR *p0((node_EXPR*)operator[](0));

    set0.push_back(label0);
    set1.push_back(label2);

    p0->build_ir(lir, func);

    set0.pop_back();
    set1.pop_back();

    instruction *ins;
    if(p0->op_info != LOR && p0->op_info != LAND && p0->op_info != DEN)
    {
        ins = new ir_ins_cjmp(label0, p0->load_reg(lir, func), false);
        func->append_inst(ins);
        ins = new ir_ins_jmp(label2);
        func->append_inst(ins);
    }

    ins = new instruction;
    ins->label = label2;
    func->append_inst(ins);
    ast_node *p1(operator[](1));
    p1->build_ir(lir, func);
    ins = new ir_ins_jmp(label1);
    func->append_inst(ins);

    ins = new instruction;
    ins->label = label0;
    func->append_inst(ins);
    if(t_siz == 3)
    {
        ast_node *p2(operator[](2));
        p2->build_ir(lir, func);
    }
    ins = new ir_ins_jmp(label1);
    func->append_inst(ins);

    ins = new instruction;
    ins->label = label1;
    func->append_inst(ins);
}

node_WHILE::~node_WHILE() {};

node_type node_WHILE::getType()
{
    return WHILE;
}

void node_WHILE::type_check()
{
    ++loop_cnt;
    ast_node::type_check();
    node_EXPR *p = (node_EXPR*)operator[](0);
    if(p->type_info.getStr() != "[bool]")
        throw("Condition in WHILE statement must be bool.");
    --loop_cnt;
}

void node_WHILE::build_ir(prog_with_ir &lir, ir* func)
{
    node_EXPR *p0 = (node_EXPR*)operator[](0);
    ast_node *p1 = operator[](1);
    string label0 = "[while_start][" + to_string(name_cnt++) + "]";
    string label1 = "[while_exit][" + to_string(name_cnt++) + "]";
    string label2 = "[while_body][" + to_string(name_cnt++) + "]";

    entry_label.push_back(label0);
    exit_label.push_back(label1);

    instruction *ins = new ir_ins_jmp(label0);
    func->append_inst(ins);

    ins = new instruction;
    ins->label = label0;
    func->append_inst(ins);

    set0.push_back(label1);
    set1.push_back(label2);

    p0->build_ir(lir, func);
    
    set0.pop_back();
    set1.pop_back();

    if(p0->op_info != LOR && p0->op_info != LAND && p0->op_info != DEN)
    {
        ins = new ir_ins_cjmp(label1, p0->load_reg(lir, func), false);
        func->append_inst(ins);
        ins = new ir_ins_jmp(label2);
        func->append_inst(ins);
    }

    ins = new instruction;
    ins->label = label2;
    func->append_inst(ins);

    p1->build_ir(lir, func);
    ins = new ir_ins_jmp(label0);
    func->append_inst(ins);

    ins = new instruction;
    ins->label = label1;
    func->append_inst(ins);

    entry_label.pop_back();
    exit_label.pop_back();
}

node_FOR::~node_FOR() {};

node_type node_FOR::getType()
{
    return FOR;
}

void node_FOR::build()
{
    scope *tmp = ast.create<scope>(*sp);
    sp = tmp;
    ast_node::build();
}

void node_FOR::type_check()
{
    ++loop_cnt;
    ast_node::type_check();
    node_EXPR *p = (node_EXPR*)operator[](1);
    string tmp = p->type_info.getStr();
    if(tmp != "[bool]" && tmp != "[null]" && tmp != "[]")
        throw("Condition in FOR statement must be bool or empty.");
    --loop_cnt;
}

void node_FOR::build_ir(prog_with_ir &lir, ir* func)
{
    node_EXPR *p0 = (node_EXPR*)operator[](0);
    node_EXPR *p1 = (node_EXPR*)operator[](1);
    node_EXPR *p2 = (node_EXPR*)operator[](2);
    ast_node *p3 = operator[](3);
    string label0 = "[for_start][" + to_string(name_cnt++) + "]";
    string label1 = "[for_exit][" + to_string(name_cnt++) + "]";
    string label2 = "[for_body][" + to_string(name_cnt++) + "]";
    string label3 = "[for_var][" + to_string(name_cnt++) + "]";

    p0->build_ir(lir, func);

    instruction *ins = new ir_ins_jmp(label0);
    func->append_inst(ins);

    entry_label.push_back(label3);
    exit_label.push_back(label1);

    ins = new instruction;
    ins->label = label0;
    func->append_inst(ins);

    set0.push_back(label1);
    set1.push_back(label2);

    p1->build_ir(lir, func);

    set0.pop_back();
    set1.pop_back();
    
    if(p1->op_info != LOR && p1->op_info != LAND && p0->op_info != DEN)
    {
        if(p1->reg_name.length())
        {
            ins = new ir_ins_cjmp(label1, p1->load_reg(lir, func), false);
            func->append_inst(ins);
            ins = new ir_ins_jmp(label2);
            func->append_inst(ins);
        }
    }
    ins = new instruction;
    ins->label = label2;
    func->append_inst(ins);

    p3->build_ir(lir, func);
    ins = new ir_ins_jmp(label3);
    func->append_inst(ins);

    ins = new instruction;
    ins->label = label3;
    func->append_inst(ins);
    p2->build_ir(lir, func);
    ins = new ir_ins_jmp(label0);
    func->append_inst(ins);

    ins = new instruction;
    ins->label = label1;
    func->append_inst(ins);

    entry_label.pop_back();
    exit_label.pop_back();
}

node_RETURN::~node_RETURN() {};

node_type node_RETURN::getType()
{
    return RETURN;
}

void node_RETURN::type_check()
{
    ast_node::type_check();
    int siz = size();
    type_struct tmp;
    if(siz == 0)
        tmp.init("void", "", true);
    else
        tmp = ((node_EXPR*)operator[](0))->type_info;
    if(!(tmp == re_type))
    {
        cout << tmp.getStr() << " " << re_type.getStr() << endl;
        if(!(tmp.getStr() == "[null]" && re_type.is_ref() && (re_type.getStr() != "string" || re_type.get_level() != 0)))
            throw("Unmatched return type.");
    }
}

void node_RETURN::build_ir(prog_with_ir &lir, ir* func)
{
    ast_node::build_ir(lir, func);
    instruction *ins;
    if(size())
    {
        ins = new ir_ins_ret(((node_EXPR*)operator[](0))->load_reg(lir, func));
        func->append_inst(ins);
    }
    ins = new ir_ins_jmp(exit_block);
    func->append_inst(ins);
}

node_CONTINUE::~node_CONTINUE() {};

node_type node_CONTINUE::getType()
{
    return CONTINUE;
}

void node_CONTINUE::type_check()
{
    if(loop_cnt == 0)
        throw("continue!!!");
}

void node_CONTINUE::build_ir(prog_with_ir &lir, ir* func)
{
    instruction *ins = new ir_ins_jmp(entry_label.back());
    func->append_inst(ins);
}

node_BREAK::~node_BREAK() {};

node_type node_BREAK::getType()
{
    return BREAK;
}

void node_BREAK::type_check()
{
    if(loop_cnt == 0)
        throw("Break!!!");
}

void node_BREAK::build_ir(prog_with_ir &lir, ir* func)
{
    instruction *ins = new ir_ins_jmp(exit_label.back());
    func->append_inst(ins);
}

type_struct &node_PARALIST::get_Typeinfo(int s)
{
    return ((node_EXPR*)operator[](s))->type_info;
}
type_struct &node_EXPR::get_Typeinfo(int s)
{
    return ((node_EXPR*)operator[](s))->type_info;
}


node_EXPR::~node_EXPR() {};

node_type node_EXPR::getType()
{
    return EXPR;
}

void node_EXPR::build()
{
    if(op_info == _ID)
    {
        if((*(string*)val) == "this")
        {
            type_info = building_class;
        }
        else if((sp->get_tTrie()).is_defined(*((string*)val)))
            type_info = (sp->get_tTrie()).find(*((string*)val));
    }
    ast_node::build();
}

bool node_EXPR::check_valid(string a, string b)
{
    while(a.length())
    {
        if(!assign_valid(cut_class_name(a), cut_class_name(b)))
            return false;
    }
    return true;
}

void node_EXPR::type_check()
{
    ast_node::type_check();
    scope *check_sp = sp;
    if(op_info == _ID)
    {
        if(!sp->get_tTrie().is_defined(*(string*)val))
        {
            printf("%s\n", (*((string*)val)).c_str());
            throw("Undefined id.");
        }
        type_struct t = sp->get_tTrie().find(*(string*)val);
        if(type_info.getStr() == "[]" || t.is_func())
        {
            if(!t.is_func())
                throw("This id is not a function name.");
            type_info = t;
        }
        if(type_info.is_class())
        {
            cout << type_info.getStr() << endl;
//            sp = sp->get_sTrie().find(type_info.getClassname());
        }
        return;
    }

    if(op_info == _INT || op_info == _BOOL || op_info == _STRING || op_info == _NULL || op_info == SPER)
        return;
    
    if(op_info == F_INC || op_info == F_DEC || op_info == E_INC || op_info == E_DEC)
    {
        if(get_Typeinfo(0).is_const)
            throw("const!!!");
    }
    
    if(op_info == NEW)
    {
        type_info = ((node_NEW*)operator[](0))->type_info;
        cout << "wtf: " << type_info.getStr() << endl;
        return;
    }
    
    if(op_info == ASIG)
    {
        type_info = get_Typeinfo(0);
        if(type_info.is_const)
            throw("const!!!");
        if(get_Typeinfo(1).getStr() == "[null]")
        {
            if(!(type_info.is_ref() && (type_info.getStr() != "[string]" || type_info.get_level())))
                throw("NULL");
            return;
        }
        cout << get_Typeinfo(0).getStr() << ' ' << get_Typeinfo(1).getStr() << endl;
        if(!(get_Typeinfo(0) == get_Typeinfo(1)))
            throw("Unequal type.");
        return;
    }

    if(op_info == SUBSCRIPT)
    {
        if(get_Typeinfo(1).getStr() != "[int]")
            throw("Subscript must be int.");
        type_info = get_Typeinfo(0);
//        if(type_info.is_const)
//            throw("const!!");
        if(type_info.get_level() <= 0)
            throw("Not an array.");
        type_info.dec_level();
        return;
    }

    if(op_info == EQU || op_info == NEQ)
    {
        type_struct a(get_Typeinfo(0)), b(get_Typeinfo(1));
        if(b.getStr() == "[null]")
            swap(a, b);
        if(a.getStr() == "[null]")
        {
            if(b.getStr() == "[null]" || b.is_ref())
            {
                type_info.init("bool", "", true);
                return;
            }
        }
    }
    
    string s = "[" + to_string(op_info) + "]";
    try
    {
    if(op_info == CALL)
    {
        s = s + get_Typeinfo(0).getStr();
        check_sp = ((node_EXPR*)operator[](0))->sp;
        node_PARALIST* p = (node_PARALIST*)operator[](1);
        int siz = p->size();
        type_struct t = (check_sp->get_tTrie()).find(get_Typeinfo(0).getClassname());
        if(siz == 0 && t.is_class() && !cons_valid)
        {
            cout << get_Typeinfo(0).getClassname() << ' ' << t.getStr() << endl;
            throw("cons is unenable.");
        }
        string para = "";
        for(int i = 0; i < siz; ++i)
        {
//            s = s + ((node_EXPR*)((*p)[i]))->type_info.getStr();
            para = para + ((node_EXPR*)((*p)[i]))->type_info.getStr();
        }
        string _para = check_sp->get_fun_trie().find(get_Typeinfo(0).getStr());
        if(check_valid(para, _para))
            s = s + _para;
    }
    else if(op_info == MEMB)
    {
        scope *tmp;
        s = *((string*)val);
        if(((node_EXPR*)operator[](0))->type_info.get_level() > 0)
        {
            tmp = arr_sub;
        }
        else if(!((node_EXPR*)operator[](0))->type_info.is_class())
            throw("WTF");
        else
            tmp = sp->get_sTrie().find(((node_EXPR*)operator[](0))->type_info.getClassname());
        if((tmp->get_tTrie()).is_defined(s, true))
            type_info = tmp->get_tTrie().find(s, true);
        else
            throw("No matched member.");
        sp = tmp;
        return;
    }
    else
    {
        int siz = size();
        for(int i = 0; i < siz; ++i)
            s = s + ((node_EXPR*)operator[](i))->type_info.getStr();
    }

    if(!check_sp->get_tTrie().is_defined(s))
    {
        cout << "call: " << s << endl;
        throw("No matched call.");
    }
    type_info = check_sp->get_tTrie().find(s);
    }
    catch(const char *st)
    {
        cout << op_info << endl;
        cout << s << endl;
        throw(st);
    }
    
}

void node_EXPR::init(op_type tp, ast_node* a, ast_node* b, const string &v)
{
    op_info = tp;
    is_pointer = false;
    switch (tp)
    {
        case SPER:
            if(((node_EXPR*)a)->op_info == SPER)
            {
                merge(a);
                appendChild({b});
            }
            else
            {
                appendChild({a, b});
            }
            break;
            
        case ASIG:
        case LOR:
        case LAND:
        case BOR:
        case BXOR:
        case BAND:
        case EQU:
        case NEQ:
        case LST:
        case BGT:
        case LET:
        case SHL:
        case SHR:
        case BET:
        case ADD:
        case SUB:
        case MUL:
        case DIV:
        case MOD:
            appendChild({a, b});
            break;
        case DEN:
        case F_SUB:
        case F_ADD:
        case NEG:
        case F_INC:
        case F_DEC:
        case E_INC:
        case E_DEC:
        case NEW:
            appendChild({a});
            type_info = ((node_NEW*)a)->type_info;
            break;
        case SUBSCRIPT:
        case CALL:
            appendChild({a, b});
            break;
        case MEMB:
            appendChild({a});
            val = new string(v);
            break;
        case _ID:
            val = new string(v);
            break;
        case _INT:
            type_info.init("int", "", true);
            val = new int(atoi(v.c_str()));
            break;
        case _BOOL:
            type_info.init("bool", "", true);
            val = new int;
            if(v == "true")
                *((int*)val) = true;
            else if(v == "false")
                *((int*)val) = false;
            else
                throw("bool must be true or false");
            break;
        case _STRING:
            type_info.init("string", "", true);
            val = new string(v);
            break;
        case _NULL:
            type_info.init("null", "", true);
            val = new int(0);
            break;
        default:
            break;
    }
}

void node_EXPR::init_string(prog_with_ir& lir)
{
    if(op_info == _STRING)
    {
        decode_esc(*(string*)val);
        string &s = *(string*)val;
        s = s.substr(1, s.length() - 2);
        int len = s.length();
        if(!lir.string_const.is_defined(s))
        {
            string tmp_reg_name = "[t][" + to_string(name_cnt++) + "]";
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            lir.string_const.insert(s, reg_name);
            vir_reg* t0(lir.build_reg(tmp_reg_name));
            vir_reg* t1(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_glb(t0));
            lir.get_first_func()->append_inst(ins);

            ins = new ir_ins_store(t0, new vir_reg(len));
            lir.get_first_func()->append_inst(ins);

            ins = new ir_ins_glb(t1, s);
            lir.get_first_func()->append_inst(ins);
        }
        else
        {
            reg_name = lir.string_const.find(s);
        }
        
    }
    else
    {
        ast_node::init_string(lir);
    }
}

vir_reg* node_EXPR::load_reg(prog_with_ir &lir, ir* func, bool p)
{
    if(!is_pointer || p)
        return lir.find_reg(reg_name);
    else
    {
        string tmp_name = "[t][" + to_string(name_cnt++) + "]";
        vir_reg *v(lir.find_reg(reg_name));
        vir_reg *tmp_reg(lir.build_reg(tmp_name));
        instruction *ins(new ir_ins_load(tmp_reg, v));
        func->append_inst(ins);
        return tmp_reg;
    }
}

void node_EXPR::stroe_reg(prog_with_ir &lir, ir* func, vir_reg* vr)
{
    vir_reg *v(lir.find_reg(reg_name));
    if(is_pointer)
    {
        instruction *ins(new ir_ins_store(v, vr));
        func->append_inst(ins);
    }
    else
    {
        instruction *ins(new ir_ins_mov(v, vr));
        func->append_inst(ins);
    }
}

void node_EXPR::build_ir(prog_with_ir &lir, ir* func)
{
    if (op_info != LOR && op_info != LAND && op_info != DEN)
        ast_node::build_ir(lir, func);

    switch (op_info)
    {
        case _ID:
        {
            if((*(string*)val) == "this")
            {
                reg_name = this_reg->get_name();
                is_pointer = false;
                break;
            }
            type_struct &t(sp->get_tTrie().find(*(string*)val));
            if(t.is_set_reg())
                reg_name = t.get_reg_name();
            else if(t.is_func())
            {
                reg_name = *(string*)val;
                if(t.is_memb_func)
                    reg_name = "[" + building_class.getClassname() + "]" + reg_name;
            }
            else
            {
                reg_name = "[t][" + to_string(name_cnt++) + "]";
                vir_reg *v(lir.build_reg(reg_name));
                instruction *ins(new ir_ins_add(v, this_reg, new vir_reg(t.no_in_class * 8)));
                func->append_inst(ins);
                is_pointer = 1;
            }

            if(t.is_global)
                is_pointer = true;
            break;
        }
        case _NULL:
        {
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_mov(t, new vir_reg(0)));
            func->append_inst(ins);
            break;
        }
        case _BOOL:
        case _INT:
        {
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_mov(t, new vir_reg(*((int*)val))));
            func->append_inst(ins);
            break;
        }
        case _STRING:
        {
            is_pointer = false;
            break;
        }
        case ASIG:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            p0->stroe_reg(lir, func, p1->load_reg(lir, func));
            break;
        }
        case LOR:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));

            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            string label1 = "[LOR]" + to_string(name_cnt++) + "]";
            string label0 = "[LOR]" + to_string(name_cnt++) + "]";
            string label2 = "[LOR]" + to_string(name_cnt++) + "]";
            string label3 = "[LOR]" + to_string(name_cnt++) + "]";

            if(set0.size() == 0)
            {
                set0.push_back(label2);
                set1.push_back(label1);
            }
            else
            {
                set0.push_back(label2);
                set1.push_back(set1.back());
            }

            p0->build_ir(lir, func);

            set0.pop_back();
            set1.pop_back();

            instruction *ins;

            if(p0->op_info != LOR && p0->op_info != LAND && p0->op_info != DEN)
            {
                string s = "[t][" + to_string(name_cnt++) + "]";
                vir_reg* t0(lir.build_reg(s));
                ins = new ir_ins_neq(t0, new vir_reg(0), p0->load_reg(lir, func));
                func->append_inst(ins);

                if(set0.size() == 0)
                {
                    ins = new ir_ins_cjmp(label1, t0, func);
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(label2);
                    func->append_inst(ins);
                }
                else
                {
                    ins = new ir_ins_cjmp(set1.back(), t0, func);
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(label2);
                    func->append_inst(ins);
                }
            }

            ins = new instruction;
            ins->label = label2;
            func->append_inst(ins);

            if(set0.size() == 0)
            {
                set0.push_back(label3);
                set1.push_back(label1);
            }
            else
            {
                set0.push_back(set0.back());
                set1.push_back(set1.back());
            }

            p1->build_ir(lir, func);

            set0.pop_back();
            set1.pop_back();

            if(p1->op_info != LOR && p1->op_info != LAND && p1->op_info != DEN)
            {
                string s = "[t][" + to_string(name_cnt++) + "]";
                vir_reg* t1(lir.build_reg(s));
                ins = new ir_ins_neq(t1, new vir_reg(0), p1->load_reg(lir, func));
                func->append_inst(ins);

                if(set0.size() == 0)
                {
                    ins = new ir_ins_cjmp(label1, t1, func);
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(label3);
                    func->append_inst(ins);
                }
                else
                {
                    ins = new ir_ins_cjmp(set1.back(), t1, func);
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(set0.back());
                    func->append_inst(ins);
                }
            }

            if(set0.size() == 0)
            {
                ins = new instruction;
                ins->label = label3;
                func->append_inst(ins);

                ins = new ir_ins_mov(t, new vir_reg(0));
                func->append_inst(ins);

                ins = new ir_ins_jmp(label0);
                func->append_inst(ins);

                ins = new instruction;
                ins->label = label1;
                func->append_inst(ins);

                ins = new ir_ins_mov(t, new vir_reg(1));
                func->append_inst(ins);

                ins = new ir_ins_jmp(label0);
                func->append_inst(ins);

                ins = new instruction;
                ins->label = label0;
                func->append_inst(ins);
            }
            break;
        }
        case BOR:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_or(t, p0->load_reg(lir, func), p1->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case LAND:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));

            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            string label1 = "[LAND]" + to_string(name_cnt++) + "]";
            string label0 = "[LAND]" + to_string(name_cnt++) + "]";
            string label2 = "[LAND]" + to_string(name_cnt++) + "]";
            string label3 = "[LAND]" + to_string(name_cnt++) + "]";

            if(set0.size() == 0)
            {
                set1.push_back(label2);
                set0.push_back(label1);
            }
            else
            {
                set1.push_back(label2);
                set0.push_back(set0.back());
            }

            p0->build_ir(lir, func);

            set0.pop_back();
            set1.pop_back();

            instruction *ins;

            if(p0->op_info != LOR && p0->op_info != LAND && p0->op_info != DEN)
            {
                string s = "[t][" + to_string(name_cnt++) + "]";
                vir_reg* t0(lir.build_reg(s));
                ins = new ir_ins_equ(t0, new vir_reg(0), p0->load_reg(lir, func));
                func->append_inst(ins);

                if(set0.size() == 0)
                {
                    ins = new ir_ins_cjmp(label1, t0, func);
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(label2);
                    func->append_inst(ins);
                }
                else
                {
                    ins = new ir_ins_cjmp(set0.back(), t0, func);
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(label2);
                    func->append_inst(ins);
                }
            }

            ins = new instruction;
            ins->label = label2;
            func->append_inst(ins);

            if(set0.size() == 0)
            {
                set0.push_back(label1);
                set1.push_back(label3);
            }
            else
            {
                set0.push_back(set0.back());
                set1.push_back(set1.back());
            }

            p1->build_ir(lir, func);

            set0.pop_back();
            set1.pop_back();

            if(p1->op_info != LOR && p1->op_info != LAND && p1->op_info != DEN)
            {
                string s = "[t][" + to_string(name_cnt++) + "]";
                vir_reg* t1(lir.build_reg(s));
                ins = new ir_ins_equ(t1, new vir_reg(0), p1->load_reg(lir, func));
                func->append_inst(ins);

                if(set0.size() == 0)
                {
                    ins = new ir_ins_cjmp(label1, t1, func);
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(label3);
                    func->append_inst(ins);
                }
                else
                {
                    ins = new ir_ins_cjmp(set0.back(), t1, func);
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(set1.back());
                    func->append_inst(ins);
                }
            }

            if(set0.size() == 0)
            {
                ins = new instruction;
                ins->label = label3;
                func->append_inst(ins);

                ins = new ir_ins_mov(t, new vir_reg(1));
                func->append_inst(ins);

                ins = new ir_ins_jmp(label0);
                func->append_inst(ins);

                ins = new instruction;
                ins->label = label1;
                func->append_inst(ins);

                ins = new ir_ins_mov(t, new vir_reg(0));
                func->append_inst(ins);

                ins = new ir_ins_jmp(label0);
                func->append_inst(ins);

                ins = new instruction;
                ins->label = label0;
                func->append_inst(ins);
            }
            break;
        }
        case BAND:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_and(t, p0->load_reg(lir, func), p1->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case BXOR:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_xor(t, p0->load_reg(lir, func), p1->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case EQU:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins;
            if(p0->type_info.getClassname() == "string")
            {
                ins = new ir_ins_call("]]strequ", t);
                ((ir_ins_call*)ins)->append_para(p0->load_reg(lir, func));
                ((ir_ins_call*)ins)->append_para(p1->load_reg(lir, func));
            }
            else
                ins = new ir_ins_equ(t, p0->load_reg(lir, func), p1->load_reg(lir, func));
            func->append_inst(ins);
            break;
        }
        case NEQ:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins;
            if(p0->type_info.getClassname() == "string")
            {
                ins = new ir_ins_call("]]strneq", t);
                ((ir_ins_call*)ins)->append_para(p0->load_reg(lir, func));
                ((ir_ins_call*)ins)->append_para(p1->load_reg(lir, func));
            }
            else
                ins = new ir_ins_neq(t, p0->load_reg(lir, func), p1->load_reg(lir, func));
            func->append_inst(ins);
            break;
        }
        case LST:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins;
            if(p0->type_info.getClassname() == "string")
            {
                ins = new ir_ins_call("]]strlst", t);
                ((ir_ins_call*)ins)->append_para(p0->load_reg(lir, func));
                ((ir_ins_call*)ins)->append_para(p1->load_reg(lir, func));
            }
            else
                ins = new ir_ins_lst(t, p0->load_reg(lir, func), p1->load_reg(lir, func));
            func->append_inst(ins);
            break;
        }
        case BGT:
        {
            node_EXPR *p0((node_EXPR*)operator[](1));
            node_EXPR *p1((node_EXPR*)operator[](0));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins;
            if(p0->type_info.getClassname() == "string")
            {
                ins = new ir_ins_call("]]strlst", t);
                ((ir_ins_call*)ins)->append_para(p0->load_reg(lir, func));
                ((ir_ins_call*)ins)->append_para(p1->load_reg(lir, func));
            }
            else
                ins = new ir_ins_lst(t, p0->load_reg(lir, func), p1->load_reg(lir, func));
            func->append_inst(ins);
            break;
        }
        case LET:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins;
            if(p0->type_info.getClassname() == "string")
            {
                ins = new ir_ins_call("]]strlet", t);
                ((ir_ins_call*)ins)->append_para(p0->load_reg(lir, func));
                ((ir_ins_call*)ins)->append_para(p1->load_reg(lir, func));
            }
            else
                ins = new ir_ins_let(t, p0->load_reg(lir, func), p1->load_reg(lir, func));
            func->append_inst(ins);
            break;
        }
        case BET:
        {
            node_EXPR *p0((node_EXPR*)operator[](1));
            node_EXPR *p1((node_EXPR*)operator[](0));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins;
            if(p0->type_info.getClassname() == "string")
            {
                ins = new ir_ins_call("]]strlet", t);
                ((ir_ins_call*)ins)->append_para(p0->load_reg(lir, func));
                ((ir_ins_call*)ins)->append_para(p1->load_reg(lir, func));
            }
            else
                ins = new ir_ins_let(t, p0->load_reg(lir, func), p1->load_reg(lir, func));
            func->append_inst(ins);
            break;
        }
        case SHL:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_shl(t, p0->load_reg(lir, func), p1->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case SHR:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_shr(t, p0->load_reg(lir, func), p1->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case ADD:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            if(type_info.getClassname() == "string")
            {
                reg_name = "[t][" + to_string(name_cnt++) + "]";
                vir_reg* vreg = lir.build_reg(reg_name);
                instruction *ins(new ir_ins_call("]]strcat", vreg));
                ((ir_ins_call*)ins)->append_para(p0->load_reg(lir, func));
                ((ir_ins_call*)ins)->append_para(p1->load_reg(lir, func));
                func->append_inst(ins);
            }
            else
            {
                reg_name = "[t][" + to_string(name_cnt++) + "]";
                vir_reg* vreg = lir.build_reg(reg_name);
                instruction *ins(new ir_ins_add(vreg, p0->load_reg(lir, func), p1->load_reg(lir, func)));
                func->append_inst(ins);
            }
            break;
        }
        case SUB:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_sub(t, p0->load_reg(lir, func), p1->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case MUL:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_mul(t, p0->load_reg(lir, func), p1->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case DIV:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_div(t, p0->load_reg(lir, func), p1->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case MOD:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_mod(t, p0->load_reg(lir, func), p1->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case DEN:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            vir_reg *t = NULL;
            
            string label0 = "[den_false][" + to_string(name_cnt++) + "]";
            string label1 = "[den_true][" + to_string(name_cnt++) + "]";
            string label2 = "[den_exit][" + to_string(name_cnt++) + "]";

            reg_name = "[t][" + to_string(name_cnt++) + "]";
            t = lir.build_reg(reg_name);

            if(set0.size())
            {
                string tmp = set0.back();
                set0.push_back(set1.back());
                set1.push_back(tmp);
            }
            else
            {
                set0.push_back(label1);
                set1.push_back(label0);
            }

            p0->build_ir(lir, func);

            set0.pop_back();
            set1.pop_back();

            if(p0->op_info != LOR && p0->op_info != LAND && p0->op_info != DEN)
            {
                if(set0.size() == 0)
                {
                    instruction *ins(new ir_ins_cjmp(label0, p0->load_reg(lir, func)));
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(label1);
                    func->append_inst(ins);
                }
                else
                {
                    instruction *ins(new ir_ins_cjmp(set0.back(), p0->load_reg(lir, func)));
                    func->append_inst(ins);
                    ins = new ir_ins_jmp(set1.back());
                    func->append_inst(ins);
                }
            }

            if(set0.size() == 0)
            {
                instruction *ins;

                ins = new instruction;
                ins->label = label0;
                func->append_inst(ins);

                ins = new ir_ins_mov(t, new vir_reg(0));
                func->append_inst(ins);
                ins = new ir_ins_jmp(label2);
                func->append_inst(ins);

                ins = new instruction;
                ins->label = label1;
                func->append_inst(ins);

                ins = new ir_ins_mov(t, new vir_reg(1));
                func->append_inst(ins);
                ins = new ir_ins_jmp(label2);
                func->append_inst(ins);

                ins = new instruction;
                ins->label = label2;
                func->append_inst(ins);
            }
            break;
        }
        case NEG:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_not(t, p0->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case F_DEC:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));

            string s = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* tmp(lir.build_reg(s));
            instruction *ins = new ir_ins_sub(tmp, p0->load_reg(lir, func), new vir_reg(1));
            func->append_inst(ins);

            p0->stroe_reg(lir, func, tmp);
            reg_name = p0->reg_name;
            is_pointer = p0->is_pointer;
            break;
        }
        case F_INC:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));

            string s = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* tmp(lir.build_reg(s));
            instruction *ins = new ir_ins_add(tmp, p0->load_reg(lir, func), new vir_reg(1));
            func->append_inst(ins);

            p0->stroe_reg(lir, func, tmp);
            reg_name = p0->reg_name;
            is_pointer = p0->is_pointer;
            break;
        }
        case F_ADD:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            reg_name = p0->reg_name;
            is_pointer = p0->is_pointer;
            break;
        }
        case F_SUB:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            instruction *ins(new ir_ins_neg(t, p0->load_reg(lir, func)));
            func->append_inst(ins);
            break;
        }
        case E_INC:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));

            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* tmp(lir.build_reg(reg_name));
            instruction *ins = new ir_ins_mov(tmp, p0->load_reg(lir, func));
            func->append_inst(ins);

            string s = "[t][" + to_string(name_cnt++) + "]";
            tmp = lir.build_reg(s);
            ins = new ir_ins_add(tmp, p0->load_reg(lir, func), new vir_reg(1));
            func->append_inst(ins);

            p0->stroe_reg(lir, func, tmp);
            break;
        }
        case E_DEC:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));

            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* tmp(lir.build_reg(reg_name));
            instruction *ins = new ir_ins_mov(tmp, p0->load_reg(lir, func));
            func->append_inst(ins);

            string s = "[t][" + to_string(name_cnt++) + "]";
            tmp = lir.build_reg(s);
            ins = new ir_ins_sub(tmp, p0->load_reg(lir, func), new vir_reg(1));
            func->append_inst(ins);

            p0->stroe_reg(lir, func, tmp);
            break;
        }
        case SUBSCRIPT:
        {
            is_pointer = true;
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            int scale = 8;
            string s = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* tmp(lir.build_reg(s));
            instruction *ins(new ir_ins_mul(tmp, p1->load_reg(lir, func), new vir_reg(scale)));
            func->append_inst(ins);
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t2(lir.build_reg(reg_name));
            ins = new ir_ins_add(t2, p0->load_reg(lir, func), tmp);
            func->append_inst(ins);
            /*
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg* t(lir.build_reg(reg_name));
            ins = new ir_ins_load(t, t2);
            func->append_inst(ins);
            */
            break;
        }
        case CALL:
        {
            node_EXPR *p0((node_EXPR*)operator[](0));
            node_EXPR *p1((node_EXPR*)operator[](1));
            reg_name = "[t][" + to_string(name_cnt++) + "]";
            vir_reg *re = lir.build_reg(reg_name);
            ir_ins_call *ins(new ir_ins_call(p0->reg_name, re));
            if(p0->reg_name[0] == '[')
            {
                if(p0->op_info == MEMB)
                    ins->append_para(((node_EXPR*)((*p0)[0]))->load_reg(lir, func));
                else
                    ins->append_para(this_reg);
            }
            int siz = p1->size();
            for(int i = 0; i < siz; ++i)
            {
                node_EXPR *p((node_EXPR*)((*p1)[i]));
                ins->append_para(p->load_reg(lir, func));
            }
            func->append_inst(ins);
            break;
        }
        case MEMB:
        {
            is_pointer = true;
            node_EXPR *p0((node_EXPR*)operator[](0));
            int scale = 8;
            type_struct &ty = (sp->get_tTrie()).find(*(string*)val);
            if(ty.is_func())
            {
                reg_name = "[" + p0->type_info.getClassname() + "]" + *(string*)val;
            }
            else
            {
                reg_name = "[t][" + to_string(name_cnt++) + "]";
                vir_reg* tmp(lir.build_reg(reg_name));
                instruction *ins(new ir_ins_add(tmp, p0->load_reg(lir, func), new vir_reg(scale * ty.no_in_class)));
                func->append_inst(ins);
                is_pointer = true;
            }
            break;
        }
        case NEW:
        {
            node_NEW *p0((node_NEW*)operator[](0));
            reg_name = p0->reg_name;
            break;
        }
        default:
            break;
    }
}

node_PARALIST::~node_PARALIST() {};

node_type node_PARALIST::getType()
{
    return PARALIST;
}

node_CREATOR::~node_CREATOR() {};

node_type node_CREATOR::getType()
{
    return CREATOR;
}

void node_CREATOR::type_check()
{
    ast_node::type_check();
    int siz = size();
    for(int i = 0; i < siz; ++i)
    {
        node_EXPR *p = (node_EXPR*)operator[](i);
        if(p->type_info.getStr() != "int")
            throw("Subscript must be int.");
    }
}

node_NEW::~node_NEW() {};

node_type node_NEW::getType()
{
    return n_NEW;
}

void node_NEW::init(const string &t, const string &arr, ast_node *det, ast_node *par)
{
    int del = 0;
    if(det != NULL)
    {
        merge(det);
        del = size();
    }
    if(par != NULL)
    {
        cons = true;
        appendChild({par});
    }
    type_info.init(t, arr, true, del);
}

void node_NEW::type_check()
{
    ast_node::type_check();
    string s = type_info.getStr().substr(0, 5);
    if(s == "[null" || s == "[void")
        throw("Invalid type.");
    if(cons)
    {
        sp = (sp->get_sTrie()).find(type_info.getClassname());
        string s = "[31][" + type_info.getClassname() + "]";
        node_PARALIST *p = (node_PARALIST*)operator[](0);
        int siz = p->size();
        for(int i = 0; i < siz; ++i)
            s = s + (p->get_Typeinfo(i)).getStr();
        sp->get_tTrie().find(s);
    }
}

vir_reg *node_NEW::malloc_loop(prog_with_ir &lir, ir* func, int ind)
{
    string s = "[t][" + to_string(name_cnt++) + "]";
    vir_reg *r = lir.build_reg(s);
    printf("[debug] %d %d\n", ind, t_siz);
    if(ind == t_siz)//tbc: need to fix
    {
        s = "[t][" + to_string(name_cnt++) + "]";
        vir_reg *tmp = lir.build_reg(s);

        ir_ins_call *ins(new ir_ins_call("]malloc", tmp));
        func->append_inst(ins);
        ins->append_para(new vir_reg(len * 8 + 8));

        instruction *inst = new ir_ins_store(tmp, new vir_reg(len));
        func->append_inst(inst);

        inst = new ir_ins_add(r, tmp, new vir_reg(8));
        func->append_inst(inst);
        return r;
    }
    else
    {
        node_EXPR *p((node_EXPR*)operator[](ind));
        s = "[t][" + to_string(name_cnt++) + "]";

        vir_reg *cal_size = lir.build_reg(s);
        instruction *c0(new ir_ins_mul(cal_size, p->load_reg(lir, func),new vir_reg(8)));
        instruction *inc(new ir_ins_add(cal_size, cal_size, new vir_reg(8)));
        func->append_inst(c0);
        func->append_inst(inc);

        s = "[t][" + to_string(name_cnt++) + "]";
        vir_reg *tmp = lir.build_reg(s);

        ir_ins_call *ins(new ir_ins_call("]malloc", tmp));
        func->append_inst(ins);
        ins->append_para(cal_size);

        instruction *st_len(new ir_ins_store(tmp, p->load_reg(lir, func)));
        func->append_inst(st_len);

        instruction *cal_r(new ir_ins_add(r, tmp, new vir_reg(8)));
        func->append_inst(cal_r);

        if(ind < t_siz - 1)
        {
            s = "[t][" + to_string(name_cnt++) + "]";
            vir_reg *_i(lir.build_reg(s));
            instruction *cc(new ir_ins_mov(_i, p->load_reg(lir, func)));
            func->append_inst(cc);

            s = "[t][" + to_string(name_cnt++) + "]";
            vir_reg *ite(lir.build_reg(s));
            instruction *c1(new ir_ins_mov(ite, r));
            func->append_inst(c1);

            string loop_name = "[malloc_loop][" + to_string(name_cnt++) + "]";

            instruction *k(new ir_ins_jmp(loop_name));
            func->append_inst(k);

            instruction *c2(new instruction);
            c2->label = loop_name;
            func->append_inst(c2);

            vir_reg *r0(malloc_loop(lir, func, ind + 1));
            instruction *c3(new ir_ins_store(ite, r0));
            func->append_inst(c3);

            instruction *c4(new ir_ins_add(ite, ite, new vir_reg(8)));
            func->append_inst(c4);

            instruction *c5(new ir_ins_sub(_i, _i, new vir_reg(1)));
            func->append_inst(c5);

            s = "[t][" + to_string(name_cnt++) + "]";
            vir_reg *cond(lir.build_reg(s));
            instruction *c6(new ir_ins_neq(cond, _i, new vir_reg(0)));
            func->append_inst(c6);

            instruction *c7(new ir_ins_cjmp(loop_name, cond, func));
            func->append_inst(c7);

            string fall = "[malloc_loop][" + to_string(name_cnt++) + "]";
            instruction *c8(new ir_ins_jmp(fall));
            func->append_inst(c8);

            instruction *c9(new instruction);
            c9->label = fall;
            func->append_inst(c9);
        }
        
        return r;
    }
    
}

void node_NEW::build_ir(prog_with_ir &lir, ir* func)
{
    ast_node::build_ir(lir, func);
    if(type_info.get_level() == 0)
    {
        if(type_info.is_class())
        {
            type_struct &tp = sp->get_tTrie().find(type_info.getClassname());
            len = tp.no_in_class;
        }
        else
        {
            len = 1;
        }
    }
    else
        len = 1;
    t_siz = size();
    if(cons || type_info.get_level() == 0)
    {
        reg_name = "[t][" + to_string(name_cnt++) + "]";
        vir_reg *r = lir.build_reg(reg_name);
        ir_ins_call *ins(new ir_ins_call("]malloc", r));
        func->append_inst(ins);
        ins->append_para(new vir_reg(len * 8));
        ins = new ir_ins_call("[" + type_info.getClassname() + "]" + type_info.getClassname());
        func->append_inst(ins);
        ins->append_para(r);
        for(int i = 0; i < t_siz; ++i)
            ins->append_para(((node_EXPR*)operator[](i))->load_reg(lir, func));
    }
    else
    {
        reg_name = "[t][" + to_string(name_cnt++) + "]";
        vir_reg *r = lir.build_reg(reg_name);
        instruction *ins(new ir_ins_mov(r, malloc_loop(lir, func, 0)));
        func->append_inst(ins);
    }
}