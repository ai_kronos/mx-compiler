#ifndef _graph_h
#define _graph_h

#include<set>
#include<list>

using namespace std;

class flow_node
{
  public:
    set<flow_node*> pre, nxt;
    bool flag = false;

    void add_out(flow_node *);
    void erase_out(flow_node *);
    void clean_edge();
};

class dom_node: public flow_node
{
  public:
    set<dom_node*> son;
    set<dom_node*> df;
    dom_node *idom = NULL;

    dom_node *u = this;
    dom_node *sdom = NULL;
    int time_stamp = -1;

    void build_dom_tree();
    void cal_df();
    void add_son(dom_node*);

  private:
    dom_node *fa = this, *val = this;
    list<dom_node*> cal_list;

    void dfs(int&, list<dom_node*>&);
    void cal_sdom();
    void cal_idom();
    dom_node* get_fa();
    void set_fa(dom_node*);
};


#endif