#ifndef _ir_enum_h
#define _ir_enum_h

enum ir_ins_type
{
    IR_INS_NULL,
    IR_INS_GLB,
    IR_INS_MOV,
    IR_INS_LOAD,
    IR_INS_STORE,
    IR_INS_OR,
    IR_INS_AND,
    IR_INS_XOR,
    IR_INS_EQU,
    IR_INS_NEQ,
    IR_INS_LST,
    IR_INS_LET,
    IR_INS_SHL,
    IR_INS_SHR,
    IR_INS_ADD,
    IR_INS_SUB,
    IR_INS_MUL,
    IR_INS_DIV,
    IR_INS_MOD,
    IR_INS_CALL,
    IR_INS_NOT,
    IR_INS_NEG,
    IR_INS_JMP,
    IR_INS_CJMP,
    IR_INS_RET,
    IR_INS_LEA,
    IR_INS_JEQU,
    IR_INS_JNEQ,
    IR_INS_JLST,
    IR_INS_JLET
};

#endif