#ifndef _front_end_h
#define _front_end_h

#include"RegTab.hpp"
#include"ast.h"
#include"scope.h"
#include"type.h"

extern RegTab ast;
extern ast_node *ast_root;

void frontend_init();
void semantic();

#endif