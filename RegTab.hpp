#ifndef _RegTab_hpp
#define _RegTab_hpp

#include<cstdlib>
#include<stack>
#include<initializer_list>

using namespace std;

/*
A base class for those classes which were at the risk of memory leak.
Just for the convenience of management.
*/
class BaseClass
{
  public:
    virtual ~BaseClass();
};
/*
Public:
  create() create(C &ob): Used for creating a pointer for class C.
  deleteALL(): Used for freeing the memory.

Used for trash recycling.
*/
class RegTab
{
  private:
    stack<BaseClass *> record;

  public:
    template <class C>
    C *create()
    {
        C *p = new C;
        record.push((BaseClass *)p);
        return p;
    }

    template <class C>
    C *create(const C &ob)
    {
        C *p = new C(ob);
        record.push((BaseClass *)p);
        return p;
    }

    template <class C>
    C *create(const C &ob, bool b)
    {
        C *p = new C(ob, b);
        record.push((BaseClass *)p);
        return p;
    }

    void deleteAll()
    {
        while (!record.empty())
        {
            delete record.top();
            record.pop();
        }
    }
};

extern RegTab ast;

#endif