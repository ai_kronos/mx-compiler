%language "c++"
%define api.value.type variant
%define api.token.constructor
%code requires {
    #include<string>
    #include"RegTab.hpp"
    #include"ast.h"

    extern ast_node* ast_root;
}
%code provides {
    #include"wrap.h"
}
%defines

%nonassoc vir
%nonassoc virc1
%nonassoc virc2

%token END_OF_FILE 0
//operator
%left <std::string> sper
%right <std::string> asig
%left <std::string> lor
%left <std::string> land
%left <std::string> bor
%left <std::string> bxor
%left <std::string> band
%left <std::string> equ neq
%left <std::string> lst bgt let bet
%left <std::string> shl shr
%left <std::string> add sub
%left <std::string> mul div mod
%right <std::string> den neg _new
%left <std::string> inc dec memb
%right <std::string> lrod lsqu
%left <std::string> rrod rsqu
%token <std::string> id inte str semi lbra rbra

%type <std::string> TYPEBASE
%type <ast_node*> PROGRAM FUNCDEF VARDEF CLASSDEF
%type <ast_node*> PARADEF STATEMENT PARADEFLIST PARALIST
%type <ast_node*> EXPR CLASSPARADEF STALIST
%type <ast_node*> FOR IF WHILE RETURN _SUBSCRIPT
%type <ast_node*> FOR_EXPR CREATOR _NEW

%token <std::string> creator
//controler keywords
%token <std::string> ctn brk ret whi
//command keywords
%token <std::string> _while _for _if
//const keywords
%token <std::string> _null _true _false
//type keywords
%token <std::string> _void _int _string _class _bool

%nonassoc <std::string> _then
%nonassoc <std::string> _else
%start PROGRAM

%%
PROGRAM:    FUNCDEF             {$$=ast_root=ast.create<node_PROGRAM>(); $$->appendChild({$1});}
        |   VARDEF semi         {$$=ast_root=ast.create<node_PROGRAM>(); $$->appendChild({$1});}
        |   CLASSDEF            {$$=ast_root=ast.create<node_PROGRAM>(); $$->appendChild({$1});}
        |   PROGRAM FUNCDEF     {$$=ast_root=$1; $$->appendChild({$2});}
        |   PROGRAM VARDEF semi {$$=ast_root=$1; $$->appendChild({$2});}
        |   PROGRAM CLASSDEF    {$$=ast_root=$1; $$->appendChild({$2});};

FUNCDEF:    TYPEBASE id PARADEF STATEMENT               {$$=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)$$)->init($1, "", $2, $3, $4);}
        |   id   id PARADEF  STATEMENT                  {$$=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)$$)->init($1, "", $2, $3, $4);}
        |   TYPEBASE creator id PARADEF STATEMENT       {$$=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)$$)->init($1, $2, $3, $4, $5);}
        |   id creator   id PARADEF STATEMENT           {$$=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)$$)->init($1, $2, $3, $4, $5);}
        |   id PARADEF  STATEMENT                       {$$=ast.create<node_FUNCDEF>(); ((node_FUNCDEF*)$$)->init("", "", $1, $2, $3);};

PARADEF:    lrod PARADEFLIST rrod       {$$=ast.create<node_PARADEF>(); $$->merge($2);};

PARADEFLIST:                            {$$=ast.create<node_PARADEFLIST>();}
        |   VARDEF                      {$$=ast.create<node_PARADEFLIST>(); ((node_PARADEFLIST*)$$)->init($1);}
        |   VARDEF sper PARADEFLIST     {$$=ast.create<node_PARADEFLIST>(); ((node_PARADEFLIST*)$$)->init($1, $3);};

STALIST:    STATEMENT           {$$=ast.create<node_STATEMENT>(); ((node_STATEMENT*)$$)->init(false, $1);}
        |   STALIST STATEMENT   {$$=$1; $$->appendChild({$2});};

STATEMENT:  EXPR semi           {$$=$1;}
        |   VARDEF semi         {$$=$1;}
        |   IF                  {$$=$1;}
        |   WHILE               {$$=$1;}
        |   FOR                 {$$=$1;}
        |   RETURN semi         {$$=$1;}
        |   ctn semi            {$$=ast.create<node_CONTINUE>();}
        |   brk semi            {$$=ast.create<node_BREAK>();}
        |   semi                {$$=ast.create<node_STATEMENT>();}
        |   lbra  rbra          {$$=ast.create<node_STATEMENT>();}
        |   lbra STALIST rbra   {$$=ast.create<node_STATEMENT>(); ((node_STATEMENT*)$$)->init(true, $2);};

RETURN:     ret EXPR            {$$=ast.create<node_RETURN>(); $$->appendChild({$2});}
        |   ret                 {$$=ast.create<node_RETURN>();};
WHILE:      _while lrod EXPR rrod STATEMENT                                     {$$=ast.create<node_WHILE>(); $$->appendChild({$3, $5});};
FOR:        _for lrod FOR_EXPR semi FOR_EXPR semi FOR_EXPR rrod STATEMENT       {$$=ast.create<node_FOR>(); $$->appendChild({$3, $5, $7, $9});};

IF:         _if lrod EXPR rrod STATEMENT %prec _then        {$$=ast.create<node_IF>(); $$->appendChild({$3, $5});}
        |   _if lrod EXPR rrod STATEMENT _else STATEMENT    {$$=ast.create<node_IF>(); $$->appendChild({$3, $5, $7});};

FOR_EXPR:   EXPR                    {$$=$1;}
        |                           {$$=ast.create<node_EXPR>();};

VARDEF:     TYPEBASE id                         {$$=ast.create<node_VARDEF>(); ((node_VARDEF*)$$)->init($1, "", $2, NULL);}
        |   TYPEBASE id asig EXPR               {$$=ast.create<node_VARDEF>(); ((node_VARDEF*)$$)->init($1, "", $2, $4);}
        |   TYPEBASE creator id                 {$$=ast.create<node_VARDEF>(); ((node_VARDEF*)$$)->init($1, $2, $3, NULL);}
        |   TYPEBASE creator id asig EXPR       {$$=ast.create<node_VARDEF>(); ((node_VARDEF*)$$)->init($1, $2, $3, $5);}
        |   id id asig EXPR                     {$$=ast.create<node_VARDEF>(); ((node_VARDEF*)$$)->init($1, "", $2, $4);}
        |   id creator id asig EXPR             {$$=ast.create<node_VARDEF>(); ((node_VARDEF*)$$)->init($1, $2, $3, $5);}
        |   id id                               {$$=ast.create<node_VARDEF>(); ((node_VARDEF*)$$)->init($1, "", $2, NULL);}
        |   id creator id                       {$$=ast.create<node_VARDEF>(); ((node_VARDEF*)$$)->init($1, $2, $3, NULL);};

CLASSDEF:   _class id lbra CLASSPARADEF rbra    {$$=ast.create<node_CLASSDEF>(); ((node_CLASSDEF*)$$)->init($2, $4);}
        |   _class id lbra rbra                 {$$=ast.create<node_CLASSDEF>(); ((node_CLASSDEF*)$$)->init($2, NULL);};

CLASSPARADEF:VARDEF semi                        {$$=ast.create<node_CLASSPARADEF>(); ((node_CLASSPARADEF*)$$)->init($1);}
        |   FUNCDEF                             {$$=ast.create<node_CLASSPARADEF>(); ((node_CLASSPARADEF*)$$)->init($1);}
        |   VARDEF semi CLASSPARADEF            {$$=ast.create<node_CLASSPARADEF>(); ((node_CLASSPARADEF*)$$)->init($1, $3);}
        |   FUNCDEF CLASSPARADEF                {$$=ast.create<node_CLASSPARADEF>(); ((node_CLASSPARADEF*)$$)->init($1, $2);};

TYPEBASE:   _void           {$$=$1;}
        |   _int            {$$=$1;}
        |   _string         {$$=$1;}
        |   _bool           {$$=$1;};

CREATOR:    lsqu EXPR rsqu              {$$=ast.create<node_CREATOR>(); $$->appendChild({$2});}
        |   CREATOR lsqu EXPR rsqu      {$$=$1; $$->appendChild({$3});};

EXPR:       id                  {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(_ID, NULL, NULL, $1);}
    |       inte                {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(_INT, NULL, NULL, $1);}
    |       _null               {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(_NULL, NULL, NULL, $1);}
    |       _true               {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(_BOOL, NULL, NULL, $1);}
    |       _false              {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(_BOOL, NULL, NULL, $1);}
    |       str                 {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(_STRING, NULL, NULL, $1);}
    |       EXPR sper EXPR      {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(SPER, $1, $3, "");}
    |       EXPR asig EXPR      {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(ASIG, $1, $3, "");}
    |       EXPR lor EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(LOR, $1, $3, "");}
    |       EXPR land EXPR      {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(LAND, $1, $3, "");}
    |       EXPR bor EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(BOR, $1, $3, "");}
    |       EXPR bxor EXPR      {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(BXOR, $1, $3, "");}
    |       EXPR band EXPR      {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(BAND, $1, $3, "");}
    |       EXPR equ EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(EQU, $1, $3, "");}
    |       EXPR neq EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(NEQ, $1, $3, "");}
    |       EXPR lst EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(LST, $1, $3, "");}
    |       EXPR bgt EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(BGT, $1, $3, "");}
    |       EXPR let EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(LET, $1, $3, "");}
    |       EXPR shl EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(SHL, $1, $3, "");}
    |       EXPR shr EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(SHR, $1, $3, "");}
    |       EXPR bet EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(BET, $1, $3, "");}
    |       EXPR add EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(ADD, $1, $3, "");}
    |       EXPR sub EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(SUB, $1, $3, "");}
    |       EXPR mul EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(MUL, $1, $3, "");}
    |       EXPR div EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(DIV, $1, $3, "");}
    |       EXPR mod EXPR       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(MOD, $1, $3, "");}
    |       den EXPR            {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(DEN, $2, NULL, "");}
    |       sub EXPR %prec den  {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(F_SUB, $2, NULL, "");}
    |       add EXPR %prec den  {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(F_ADD, $2, NULL, "");}
    |       neg EXPR            {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(NEG, $2, NULL, "");}
    |       inc EXPR %prec den  {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(F_INC, $2, NULL, "");}
    |       dec EXPR %prec den  {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(F_DEC, $2, NULL, "");}
    |       EXPR inc            {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(E_INC, $1, NULL, "");}
    |       EXPR dec            {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(E_DEC, $1, NULL, "");}
    |       EXPR _SUBSCRIPT     {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(SUBSCRIPT, $1, $2, "");}
    |       EXPR PARALIST       {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(CALL, $1, $2, "");}
    |       EXPR memb id        {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(MEMB, $1, NULL, $3);}
    |       lrod EXPR rrod      {$$=$2;}
    |       _NEW                {$$=ast.create<node_EXPR>(); ((node_EXPR*)$$)->init(NEW, $1, NULL, "");};

_NEW:       _new TYPEBASE                  %prec vir    {$$=ast.create<node_NEW>(); ((node_NEW*)$$)->init($2, "", NULL);}
    |       _new TYPEBASE CREATOR          %prec virc1  {$$=ast.create<node_NEW>(); ((node_NEW*)$$)->init($2, "", $3);}
    |       _new TYPEBASE CREATOR creator  %prec virc2  {$$=ast.create<node_NEW>(); ((node_NEW*)$$)->init($2, $4, $3);}
    |       _new id CREATOR                %prec virc1  {$$=ast.create<node_NEW>(); ((node_NEW*)$$)->init($2, "", $3);}
    |       _new id                        %prec vir    {$$=ast.create<node_NEW>(); ((node_NEW*)$$)->init($2, "", NULL);}
    |       _new id CREATOR creator        %prec virc2  {$$=ast.create<node_NEW>(); ((node_NEW*)$$)->init($2, $4, $3);}
    |       _new id PARALIST               %prec virc1  {$$=ast.create<node_NEW>(); ((node_NEW*)$$)->init($2, "", NULL, $3);};

_SUBSCRIPT: lsqu EXPR rsqu  {$$=$2;};

PARALIST:   lrod rrod       {$$=ast.create<node_PARALIST>();}
        |   lrod EXPR rrod  {
                $$=ast.create<node_PARALIST>();
                if(((node_EXPR*)$2)->op_info == SPER)
                        $$->merge($2);
                else
                        $$->appendChild({$2});
        };

%%
void yy::parser::error (const std::string& m)
{
        printf("error:%s\n", m.c_str());
        throw("parser failed.");
}