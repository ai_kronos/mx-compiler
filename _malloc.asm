global main
extern malloc

section .text

main:
	mov rdi, 2147483647
	call malloc
	ret
