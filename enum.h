#ifndef _enum_h
#define _enum_h

enum op_type
{
  SPER,//, rule based
  ASIG,//a=b rule based
  LOR,//bool || bool
  LAND,//bool && bool
  BOR,//int | int
  BXOR,//int ^ int
  BAND,//int & int
  EQU,// == rule based
  NEQ,// != rule based
  LST,// <
  BGT,// >
  LET,// <=
  SHL,// <<
  SHR,// >>
  BET,// >=
  ADD,// a + b
  SUB,// a - b
  MUL,// a * b
  DIV,// a / b
  MOD,// a % b
  DEN,// !bool
  F_SUB,// -a
  F_ADD,// +a
  NEG,// ~a
  F_INC,// ++a
  F_DEC,// --a
  SUBSCRIPT,// [a] rule based
  MEMB,// a.b rule based
  NEW,// 
  E_INC,// a++
  E_DEC,// b--
  CALL,
  _ID,
  _INT,
  _BOOL,
  _STRING,
  _NULL
};

enum node_type
{
  PROGRAM,
  FUNCDEF,
  n_null,
  n_int,
  n_bool,
  n_string,
  n_void,
  n_unknow,
  n_id,
  n_class,
  n_func,
  custom,
  VARDEF,
  CLASSDEF,
  PARADEF,
  STATEMENT,
  EXPR,
  CLASSPARADEF,
  PARADEFLIST,
  IF,
  WHILE,
  FOR,
  RETURN,
  CONTINUE,
  BREAK,
  LVALUE,
  PARALIST,
  TYPEBASE,
  CREATOR,
  n_NEW,
  PH
};

#endif