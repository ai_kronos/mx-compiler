#ifndef _tools_h
#define _tools_h

#include<string>
#include"phyreg_enum.h"

using namespace std;

bool is_power2(int);
int power2(int);
bool assign_valid(string, string);
string cut_class_name(string&);
string cut_reg_name(const string);
void decode_esc(string &);
string color2reg(phyreg);
string qword2byte(const string);
string qword2dword(const string);
string get_label_name(const string);

#endif