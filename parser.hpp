// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton interface for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.

/**
 ** \file parser.hpp
 ** Define the yy::parser class.
 */

// C++ LALR(1) parser skeleton written by Akim Demaille.

#ifndef YY_YY_PARSER_HPP_INCLUDED
# define YY_YY_PARSER_HPP_INCLUDED
// //                    "%code requires" blocks.
#line 4 "parser.yy" // lalr1.cc:377

    #include<string>
    #include"RegTab.hpp"
    #include"ast.h"

    extern ast_node* ast_root;

#line 52 "parser.hpp" // lalr1.cc:377


# include <cstdlib> // std::abort
# include <iostream>
# include <stdexcept>
# include <string>
# include <vector>
# include "stack.hh"


#ifndef YYASSERT
# include <cassert>
# define YYASSERT assert
#endif


#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif


namespace yy {
#line 129 "parser.hpp" // lalr1.cc:377



  /// A char[S] buffer to store and retrieve objects.
  ///
  /// Sort of a variant, but does not keep track of the nature
  /// of the stored data, since that knowledge is available
  /// via the current state.
  template <size_t S>
  struct variant
  {
    /// Type of *this.
    typedef variant<S> self_type;

    /// Empty construction.
    variant ()
    {}

    /// Construct and fill.
    template <typename T>
    variant (const T& t)
    {
      YYASSERT (sizeof (T) <= S);
      new (yyas_<T> ()) T (t);
    }

    /// Destruction, allowed only if empty.
    ~variant ()
    {}

    /// Instantiate an empty \a T in here.
    template <typename T>
    T&
    build ()
    {
      return *new (yyas_<T> ()) T;
    }

    /// Instantiate a \a T in here from \a t.
    template <typename T>
    T&
    build (const T& t)
    {
      return *new (yyas_<T> ()) T (t);
    }

    /// Accessor to a built \a T.
    template <typename T>
    T&
    as ()
    {
      return *yyas_<T> ();
    }

    /// Const accessor to a built \a T (for %printer).
    template <typename T>
    const T&
    as () const
    {
      return *yyas_<T> ();
    }

    /// Swap the content with \a other, of same type.
    ///
    /// Both variants must be built beforehand, because swapping the actual
    /// data requires reading it (with as()), and this is not possible on
    /// unconstructed variants: it would require some dynamic testing, which
    /// should not be the variant's responsability.
    /// Swapping between built and (possibly) non-built is done with
    /// variant::move ().
    template <typename T>
    void
    swap (self_type& other)
    {
      std::swap (as<T> (), other.as<T> ());
    }

    /// Move the content of \a other to this.
    ///
    /// Destroys \a other.
    template <typename T>
    void
    move (self_type& other)
    {
      build<T> ();
      swap<T> (other);
      other.destroy<T> ();
    }

    /// Copy the content of \a other to this.
    template <typename T>
    void
    copy (const self_type& other)
    {
      build<T> (other.as<T> ());
    }

    /// Destroy the stored \a T.
    template <typename T>
    void
    destroy ()
    {
      as<T> ().~T ();
    }

  private:
    /// Prohibit blind copies.
    self_type& operator=(const self_type&);
    variant (const self_type&);

    /// Accessor to raw memory as \a T.
    template <typename T>
    T*
    yyas_ ()
    {
      void *yyp = yybuffer_.yyraw;
      return static_cast<T*> (yyp);
     }

    /// Const accessor to raw memory as \a T.
    template <typename T>
    const T*
    yyas_ () const
    {
      const void *yyp = yybuffer_.yyraw;
      return static_cast<const T*> (yyp);
     }

    union
    {
      /// Strongest alignment constraints.
      long double yyalign_me;
      /// A buffer large enough to store any of the semantic values.
      char yyraw[S];
    } yybuffer_;
  };


  /// A Bison parser.
  class parser
  {
  public:
#ifndef YYSTYPE
    /// An auxiliary type to compute the largest semantic type.
    union union_type
    {
      // PROGRAM
      // FUNCDEF
      // PARADEF
      // PARADEFLIST
      // STALIST
      // STATEMENT
      // RETURN
      // WHILE
      // FOR
      // IF
      // FOR_EXPR
      // VARDEF
      // CLASSDEF
      // CLASSPARADEF
      // CREATOR
      // EXPR
      // _NEW
      // _SUBSCRIPT
      // PARALIST
      char dummy1[sizeof(ast_node*)];

      // sper
      // asig
      // lor
      // land
      // bor
      // bxor
      // band
      // equ
      // neq
      // lst
      // bgt
      // let
      // bet
      // shl
      // shr
      // add
      // sub
      // mul
      // div
      // mod
      // den
      // neg
      // _new
      // inc
      // dec
      // memb
      // lrod
      // lsqu
      // rrod
      // rsqu
      // id
      // inte
      // str
      // semi
      // lbra
      // rbra
      // creator
      // ctn
      // brk
      // ret
      // whi
      // _while
      // _for
      // _if
      // _null
      // _true
      // _false
      // _void
      // _int
      // _string
      // _class
      // _bool
      // _then
      // _else
      // TYPEBASE
      char dummy2[sizeof(std::string)];
};

    /// Symbol semantic values.
    typedef variant<sizeof(union_type)> semantic_type;
#else
    typedef YYSTYPE semantic_type;
#endif

    /// Syntax errors thrown from user actions.
    struct syntax_error : std::runtime_error
    {
      syntax_error (const std::string& m);
    };

    /// Tokens.
    struct token
    {
      enum yytokentype
      {
        END_OF_FILE = 0,
        vir = 258,
        virc1 = 259,
        virc2 = 260,
        sper = 261,
        asig = 262,
        lor = 263,
        land = 264,
        bor = 265,
        bxor = 266,
        band = 267,
        equ = 268,
        neq = 269,
        lst = 270,
        bgt = 271,
        let = 272,
        bet = 273,
        shl = 274,
        shr = 275,
        add = 276,
        sub = 277,
        mul = 278,
        div = 279,
        mod = 280,
        den = 281,
        neg = 282,
        _new = 283,
        inc = 284,
        dec = 285,
        memb = 286,
        lrod = 287,
        lsqu = 288,
        rrod = 289,
        rsqu = 290,
        id = 291,
        inte = 292,
        str = 293,
        semi = 294,
        lbra = 295,
        rbra = 296,
        creator = 297,
        ctn = 298,
        brk = 299,
        ret = 300,
        whi = 301,
        _while = 302,
        _for = 303,
        _if = 304,
        _null = 305,
        _true = 306,
        _false = 307,
        _void = 308,
        _int = 309,
        _string = 310,
        _class = 311,
        _bool = 312,
        _then = 313,
        _else = 314
      };
    };

    /// (External) token type, as returned by yylex.
    typedef token::yytokentype token_type;

    /// Symbol type: an internal symbol number.
    typedef int symbol_number_type;

    /// The symbol type number to denote an empty symbol.
    enum { empty_symbol = -2 };

    /// Internal symbol number for tokens (subsumed by symbol_number_type).
    typedef unsigned char token_number_type;

    /// A complete symbol.
    ///
    /// Expects its Base type to provide access to the symbol type
    /// via type_get().
    ///
    /// Provide access to semantic value.
    template <typename Base>
    struct basic_symbol : Base
    {
      /// Alias to Base.
      typedef Base super_type;

      /// Default constructor.
      basic_symbol ();

      /// Copy constructor.
      basic_symbol (const basic_symbol& other);

      /// Constructor for valueless symbols, and symbols from each type.

  basic_symbol (typename Base::kind_type t);

  basic_symbol (typename Base::kind_type t, const ast_node* v);

  basic_symbol (typename Base::kind_type t, const std::string v);


      /// Constructor for symbols with semantic value.
      basic_symbol (typename Base::kind_type t,
                    const semantic_type& v);

      /// Destroy the symbol.
      ~basic_symbol ();

      /// Destroy contents, and record that is empty.
      void clear ();

      /// Whether empty.
      bool empty () const;

      /// Destructive move, \a s is emptied into this.
      void move (basic_symbol& s);

      /// The semantic value.
      semantic_type value;

    private:
      /// Assignment operator.
      basic_symbol& operator= (const basic_symbol& other);
    };

    /// Type access provider for token (enum) based symbols.
    struct by_type
    {
      /// Default constructor.
      by_type ();

      /// Copy constructor.
      by_type (const by_type& other);

      /// The symbol type as needed by the constructor.
      typedef token_type kind_type;

      /// Constructor from (external) token numbers.
      by_type (kind_type t);

      /// Record that this symbol is empty.
      void clear ();

      /// Steal the symbol type from \a that.
      void move (by_type& that);

      /// The (internal) type number (corresponding to \a type).
      /// \a empty when empty.
      symbol_number_type type_get () const;

      /// The token.
      token_type token () const;

      /// The symbol type.
      /// \a empty_symbol when empty.
      /// An int, not token_number_type, to be able to store empty_symbol.
      int type;
    };

    /// "External" symbols: returned by the scanner.
    typedef basic_symbol<by_type> symbol_type;

    // Symbol constructors declarations.
    static inline
    symbol_type
    make_END_OF_FILE ();

    static inline
    symbol_type
    make_vir ();

    static inline
    symbol_type
    make_virc1 ();

    static inline
    symbol_type
    make_virc2 ();

    static inline
    symbol_type
    make_sper (const std::string& v);

    static inline
    symbol_type
    make_asig (const std::string& v);

    static inline
    symbol_type
    make_lor (const std::string& v);

    static inline
    symbol_type
    make_land (const std::string& v);

    static inline
    symbol_type
    make_bor (const std::string& v);

    static inline
    symbol_type
    make_bxor (const std::string& v);

    static inline
    symbol_type
    make_band (const std::string& v);

    static inline
    symbol_type
    make_equ (const std::string& v);

    static inline
    symbol_type
    make_neq (const std::string& v);

    static inline
    symbol_type
    make_lst (const std::string& v);

    static inline
    symbol_type
    make_bgt (const std::string& v);

    static inline
    symbol_type
    make_let (const std::string& v);

    static inline
    symbol_type
    make_bet (const std::string& v);

    static inline
    symbol_type
    make_shl (const std::string& v);

    static inline
    symbol_type
    make_shr (const std::string& v);

    static inline
    symbol_type
    make_add (const std::string& v);

    static inline
    symbol_type
    make_sub (const std::string& v);

    static inline
    symbol_type
    make_mul (const std::string& v);

    static inline
    symbol_type
    make_div (const std::string& v);

    static inline
    symbol_type
    make_mod (const std::string& v);

    static inline
    symbol_type
    make_den (const std::string& v);

    static inline
    symbol_type
    make_neg (const std::string& v);

    static inline
    symbol_type
    make__new (const std::string& v);

    static inline
    symbol_type
    make_inc (const std::string& v);

    static inline
    symbol_type
    make_dec (const std::string& v);

    static inline
    symbol_type
    make_memb (const std::string& v);

    static inline
    symbol_type
    make_lrod (const std::string& v);

    static inline
    symbol_type
    make_lsqu (const std::string& v);

    static inline
    symbol_type
    make_rrod (const std::string& v);

    static inline
    symbol_type
    make_rsqu (const std::string& v);

    static inline
    symbol_type
    make_id (const std::string& v);

    static inline
    symbol_type
    make_inte (const std::string& v);

    static inline
    symbol_type
    make_str (const std::string& v);

    static inline
    symbol_type
    make_semi (const std::string& v);

    static inline
    symbol_type
    make_lbra (const std::string& v);

    static inline
    symbol_type
    make_rbra (const std::string& v);

    static inline
    symbol_type
    make_creator (const std::string& v);

    static inline
    symbol_type
    make_ctn (const std::string& v);

    static inline
    symbol_type
    make_brk (const std::string& v);

    static inline
    symbol_type
    make_ret (const std::string& v);

    static inline
    symbol_type
    make_whi (const std::string& v);

    static inline
    symbol_type
    make__while (const std::string& v);

    static inline
    symbol_type
    make__for (const std::string& v);

    static inline
    symbol_type
    make__if (const std::string& v);

    static inline
    symbol_type
    make__null (const std::string& v);

    static inline
    symbol_type
    make__true (const std::string& v);

    static inline
    symbol_type
    make__false (const std::string& v);

    static inline
    symbol_type
    make__void (const std::string& v);

    static inline
    symbol_type
    make__int (const std::string& v);

    static inline
    symbol_type
    make__string (const std::string& v);

    static inline
    symbol_type
    make__class (const std::string& v);

    static inline
    symbol_type
    make__bool (const std::string& v);

    static inline
    symbol_type
    make__then (const std::string& v);

    static inline
    symbol_type
    make__else (const std::string& v);


    /// Build a parser object.
    parser ();
    virtual ~parser ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

#if YYDEBUG
    /// The current debugging stream.
    std::ostream& debug_stream () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);
#endif

    /// Report a syntax error.
    /// \param msg    a description of the syntax error.
    virtual void error (const std::string& msg);

    /// Report a syntax error.
    void error (const syntax_error& err);

  private:
    /// This class is not copyable.
    parser (const parser&);
    parser& operator= (const parser&);

    /// State numbers.
    typedef int state_type;

    /// Generate an error message.
    /// \param yystate   the state where the error occurred.
    /// \param yyla      the lookahead token.
    virtual std::string yysyntax_error_ (state_type yystate,
                                         const symbol_type& yyla) const;

    /// Compute post-reduction state.
    /// \param yystate   the current state
    /// \param yysym     the nonterminal to push on the stack
    state_type yy_lr_goto_state_ (state_type yystate, int yysym);

    /// Whether the given \c yypact_ value indicates a defaulted state.
    /// \param yyvalue   the value to check
    static bool yy_pact_value_is_default_ (int yyvalue);

    /// Whether the given \c yytable_ value indicates a syntax error.
    /// \param yyvalue   the value to check
    static bool yy_table_value_is_error_ (int yyvalue);

    static const short int yypact_ninf_;
    static const signed char yytable_ninf_;

    /// Convert a scanner token number \a t to a symbol number.
    static token_number_type yytranslate_ (token_type t);

    // Tables.
  // YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
  // STATE-NUM.
  static const short int yypact_[];

  // YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
  // Performed when YYTABLE does not specify something else to do.  Zero
  // means the default is an error.
  static const unsigned char yydefact_[];

  // YYPGOTO[NTERM-NUM].
  static const short int yypgoto_[];

  // YYDEFGOTO[NTERM-NUM].
  static const short int yydefgoto_[];

  // YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
  // positive, shift that token.  If negative, reduce the rule whose
  // number is the opposite.  If YYTABLE_NINF, syntax error.
  static const unsigned char yytable_[];

  static const short int yycheck_[];

  // YYSTOS[STATE-NUM] -- The (internal number of the) accessing
  // symbol of state STATE-NUM.
  static const unsigned char yystos_[];

  // YYR1[YYN] -- Symbol number of symbol that rule YYN derives.
  static const unsigned char yyr1_[];

  // YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.
  static const unsigned char yyr2_[];


#if YYDEBUG
    /// For a symbol, its name in clear.
    static const char* const yytname_[];

  // YYRLINE[YYN] -- Source line where rule number YYN was defined.
  static const unsigned char yyrline_[];
    /// Report on the debug stream that the rule \a r is going to be reduced.
    virtual void yy_reduce_print_ (int r);
    /// Print the state stack on the debug stream.
    virtual void yystack_print_ ();

    // Debugging.
    int yydebug_;
    std::ostream* yycdebug_;

    /// \brief Display a symbol type, value and location.
    /// \param yyo    The output stream.
    /// \param yysym  The symbol.
    template <typename Base>
    void yy_print_ (std::ostream& yyo, const basic_symbol<Base>& yysym) const;
#endif

    /// \brief Reclaim the memory associated to a symbol.
    /// \param yymsg     Why this token is reclaimed.
    ///                  If null, print nothing.
    /// \param yysym     The symbol.
    template <typename Base>
    void yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const;

  private:
    /// Type access provider for state based symbols.
    struct by_state
    {
      /// Default constructor.
      by_state ();

      /// The symbol type as needed by the constructor.
      typedef state_type kind_type;

      /// Constructor.
      by_state (kind_type s);

      /// Copy constructor.
      by_state (const by_state& other);

      /// Record that this symbol is empty.
      void clear ();

      /// Steal the symbol type from \a that.
      void move (by_state& that);

      /// The (internal) type number (corresponding to \a state).
      /// \a empty_symbol when empty.
      symbol_number_type type_get () const;

      /// The state number used to denote an empty symbol.
      enum { empty_state = -1 };

      /// The state.
      /// \a empty when empty.
      state_type state;
    };

    /// "Internal" symbol: element of the stack.
    struct stack_symbol_type : basic_symbol<by_state>
    {
      /// Superclass.
      typedef basic_symbol<by_state> super_type;
      /// Construct an empty symbol.
      stack_symbol_type ();
      /// Steal the contents from \a sym to build this.
      stack_symbol_type (state_type s, symbol_type& sym);
      /// Assignment, needed by push_back.
      stack_symbol_type& operator= (const stack_symbol_type& that);
    };

    /// Stack type.
    typedef stack<stack_symbol_type> stack_type;

    /// The stack.
    stack_type yystack_;

    /// Push a new state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the symbol
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, stack_symbol_type& s);

    /// Push a new look ahead token on the state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the state
    /// \param sym  the symbol (for its value and location).
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, state_type s, symbol_type& sym);

    /// Pop \a n symbols the three stacks.
    void yypop_ (unsigned int n = 1);

    /// Constants.
    enum
    {
      yyeof_ = 0,
      yylast_ = 785,     ///< Last index in yytable_.
      yynnts_ = 21,  ///< Number of nonterminal symbols.
      yyfinal_ = 17, ///< Termination state number.
      yyterror_ = 1,
      yyerrcode_ = 256,
      yyntokens_ = 60  ///< Number of tokens.
    };


  };

  // Symbol number corresponding to token number t.
  inline
  parser::token_number_type
  parser::yytranslate_ (token_type t)
  {
    static
    const token_number_type
    translate_table[] =
    {
     0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59
    };
    const unsigned int user_token_number_max_ = 314;
    const token_number_type undef_token_ = 2;

    if (static_cast<int>(t) <= yyeof_)
      return yyeof_;
    else if (static_cast<unsigned int> (t) <= user_token_number_max_)
      return translate_table[t];
    else
      return undef_token_;
  }

  inline
  parser::syntax_error::syntax_error (const std::string& m)
    : std::runtime_error (m)
  {}

  // basic_symbol.
  template <typename Base>
  inline
  parser::basic_symbol<Base>::basic_symbol ()
    : value ()
  {}

  template <typename Base>
  inline
  parser::basic_symbol<Base>::basic_symbol (const basic_symbol& other)
    : Base (other)
    , value ()
  {
      switch (other.type_get ())
    {
      case 61: // PROGRAM
      case 62: // FUNCDEF
      case 63: // PARADEF
      case 64: // PARADEFLIST
      case 65: // STALIST
      case 66: // STATEMENT
      case 67: // RETURN
      case 68: // WHILE
      case 69: // FOR
      case 70: // IF
      case 71: // FOR_EXPR
      case 72: // VARDEF
      case 73: // CLASSDEF
      case 74: // CLASSPARADEF
      case 76: // CREATOR
      case 77: // EXPR
      case 78: // _NEW
      case 79: // _SUBSCRIPT
      case 80: // PARALIST
        value.copy< ast_node* > (other.value);
        break;

      case 6: // sper
      case 7: // asig
      case 8: // lor
      case 9: // land
      case 10: // bor
      case 11: // bxor
      case 12: // band
      case 13: // equ
      case 14: // neq
      case 15: // lst
      case 16: // bgt
      case 17: // let
      case 18: // bet
      case 19: // shl
      case 20: // shr
      case 21: // add
      case 22: // sub
      case 23: // mul
      case 24: // div
      case 25: // mod
      case 26: // den
      case 27: // neg
      case 28: // _new
      case 29: // inc
      case 30: // dec
      case 31: // memb
      case 32: // lrod
      case 33: // lsqu
      case 34: // rrod
      case 35: // rsqu
      case 36: // id
      case 37: // inte
      case 38: // str
      case 39: // semi
      case 40: // lbra
      case 41: // rbra
      case 42: // creator
      case 43: // ctn
      case 44: // brk
      case 45: // ret
      case 46: // whi
      case 47: // _while
      case 48: // _for
      case 49: // _if
      case 50: // _null
      case 51: // _true
      case 52: // _false
      case 53: // _void
      case 54: // _int
      case 55: // _string
      case 56: // _class
      case 57: // _bool
      case 58: // _then
      case 59: // _else
      case 75: // TYPEBASE
        value.copy< std::string > (other.value);
        break;

      default:
        break;
    }

  }


  template <typename Base>
  inline
  parser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const semantic_type& v)
    : Base (t)
    , value ()
  {
    (void) v;
      switch (this->type_get ())
    {
      case 61: // PROGRAM
      case 62: // FUNCDEF
      case 63: // PARADEF
      case 64: // PARADEFLIST
      case 65: // STALIST
      case 66: // STATEMENT
      case 67: // RETURN
      case 68: // WHILE
      case 69: // FOR
      case 70: // IF
      case 71: // FOR_EXPR
      case 72: // VARDEF
      case 73: // CLASSDEF
      case 74: // CLASSPARADEF
      case 76: // CREATOR
      case 77: // EXPR
      case 78: // _NEW
      case 79: // _SUBSCRIPT
      case 80: // PARALIST
        value.copy< ast_node* > (v);
        break;

      case 6: // sper
      case 7: // asig
      case 8: // lor
      case 9: // land
      case 10: // bor
      case 11: // bxor
      case 12: // band
      case 13: // equ
      case 14: // neq
      case 15: // lst
      case 16: // bgt
      case 17: // let
      case 18: // bet
      case 19: // shl
      case 20: // shr
      case 21: // add
      case 22: // sub
      case 23: // mul
      case 24: // div
      case 25: // mod
      case 26: // den
      case 27: // neg
      case 28: // _new
      case 29: // inc
      case 30: // dec
      case 31: // memb
      case 32: // lrod
      case 33: // lsqu
      case 34: // rrod
      case 35: // rsqu
      case 36: // id
      case 37: // inte
      case 38: // str
      case 39: // semi
      case 40: // lbra
      case 41: // rbra
      case 42: // creator
      case 43: // ctn
      case 44: // brk
      case 45: // ret
      case 46: // whi
      case 47: // _while
      case 48: // _for
      case 49: // _if
      case 50: // _null
      case 51: // _true
      case 52: // _false
      case 53: // _void
      case 54: // _int
      case 55: // _string
      case 56: // _class
      case 57: // _bool
      case 58: // _then
      case 59: // _else
      case 75: // TYPEBASE
        value.copy< std::string > (v);
        break;

      default:
        break;
    }
}


  // Implementation of basic_symbol constructor for each type.

  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t)
    : Base (t)
    , value ()
  {}

  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const ast_node* v)
    : Base (t)
    , value (v)
  {}

  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const std::string v)
    : Base (t)
    , value (v)
  {}


  template <typename Base>
  inline
  parser::basic_symbol<Base>::~basic_symbol ()
  {
    clear ();
  }

  template <typename Base>
  inline
  void
  parser::basic_symbol<Base>::clear ()
  {
    // User destructor.
    symbol_number_type yytype = this->type_get ();
    basic_symbol<Base>& yysym = *this;
    (void) yysym;
    switch (yytype)
    {
   default:
      break;
    }

    // Type destructor.
    switch (yytype)
    {
      case 61: // PROGRAM
      case 62: // FUNCDEF
      case 63: // PARADEF
      case 64: // PARADEFLIST
      case 65: // STALIST
      case 66: // STATEMENT
      case 67: // RETURN
      case 68: // WHILE
      case 69: // FOR
      case 70: // IF
      case 71: // FOR_EXPR
      case 72: // VARDEF
      case 73: // CLASSDEF
      case 74: // CLASSPARADEF
      case 76: // CREATOR
      case 77: // EXPR
      case 78: // _NEW
      case 79: // _SUBSCRIPT
      case 80: // PARALIST
        value.template destroy< ast_node* > ();
        break;

      case 6: // sper
      case 7: // asig
      case 8: // lor
      case 9: // land
      case 10: // bor
      case 11: // bxor
      case 12: // band
      case 13: // equ
      case 14: // neq
      case 15: // lst
      case 16: // bgt
      case 17: // let
      case 18: // bet
      case 19: // shl
      case 20: // shr
      case 21: // add
      case 22: // sub
      case 23: // mul
      case 24: // div
      case 25: // mod
      case 26: // den
      case 27: // neg
      case 28: // _new
      case 29: // inc
      case 30: // dec
      case 31: // memb
      case 32: // lrod
      case 33: // lsqu
      case 34: // rrod
      case 35: // rsqu
      case 36: // id
      case 37: // inte
      case 38: // str
      case 39: // semi
      case 40: // lbra
      case 41: // rbra
      case 42: // creator
      case 43: // ctn
      case 44: // brk
      case 45: // ret
      case 46: // whi
      case 47: // _while
      case 48: // _for
      case 49: // _if
      case 50: // _null
      case 51: // _true
      case 52: // _false
      case 53: // _void
      case 54: // _int
      case 55: // _string
      case 56: // _class
      case 57: // _bool
      case 58: // _then
      case 59: // _else
      case 75: // TYPEBASE
        value.template destroy< std::string > ();
        break;

      default:
        break;
    }

    Base::clear ();
  }

  template <typename Base>
  inline
  bool
  parser::basic_symbol<Base>::empty () const
  {
    return Base::type_get () == empty_symbol;
  }

  template <typename Base>
  inline
  void
  parser::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move(s);
      switch (this->type_get ())
    {
      case 61: // PROGRAM
      case 62: // FUNCDEF
      case 63: // PARADEF
      case 64: // PARADEFLIST
      case 65: // STALIST
      case 66: // STATEMENT
      case 67: // RETURN
      case 68: // WHILE
      case 69: // FOR
      case 70: // IF
      case 71: // FOR_EXPR
      case 72: // VARDEF
      case 73: // CLASSDEF
      case 74: // CLASSPARADEF
      case 76: // CREATOR
      case 77: // EXPR
      case 78: // _NEW
      case 79: // _SUBSCRIPT
      case 80: // PARALIST
        value.move< ast_node* > (s.value);
        break;

      case 6: // sper
      case 7: // asig
      case 8: // lor
      case 9: // land
      case 10: // bor
      case 11: // bxor
      case 12: // band
      case 13: // equ
      case 14: // neq
      case 15: // lst
      case 16: // bgt
      case 17: // let
      case 18: // bet
      case 19: // shl
      case 20: // shr
      case 21: // add
      case 22: // sub
      case 23: // mul
      case 24: // div
      case 25: // mod
      case 26: // den
      case 27: // neg
      case 28: // _new
      case 29: // inc
      case 30: // dec
      case 31: // memb
      case 32: // lrod
      case 33: // lsqu
      case 34: // rrod
      case 35: // rsqu
      case 36: // id
      case 37: // inte
      case 38: // str
      case 39: // semi
      case 40: // lbra
      case 41: // rbra
      case 42: // creator
      case 43: // ctn
      case 44: // brk
      case 45: // ret
      case 46: // whi
      case 47: // _while
      case 48: // _for
      case 49: // _if
      case 50: // _null
      case 51: // _true
      case 52: // _false
      case 53: // _void
      case 54: // _int
      case 55: // _string
      case 56: // _class
      case 57: // _bool
      case 58: // _then
      case 59: // _else
      case 75: // TYPEBASE
        value.move< std::string > (s.value);
        break;

      default:
        break;
    }

  }

  // by_type.
  inline
  parser::by_type::by_type ()
    : type (empty_symbol)
  {}

  inline
  parser::by_type::by_type (const by_type& other)
    : type (other.type)
  {}

  inline
  parser::by_type::by_type (token_type t)
    : type (yytranslate_ (t))
  {}

  inline
  void
  parser::by_type::clear ()
  {
    type = empty_symbol;
  }

  inline
  void
  parser::by_type::move (by_type& that)
  {
    type = that.type;
    that.clear ();
  }

  inline
  int
  parser::by_type::type_get () const
  {
    return type;
  }

  inline
  parser::token_type
  parser::by_type::token () const
  {
    // YYTOKNUM[NUM] -- (External) token number corresponding to the
    // (internal) symbol number NUM (which must be that of a token).  */
    static
    const unsigned short int
    yytoken_number_[] =
    {
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314
    };
    return static_cast<token_type> (yytoken_number_[type]);
  }
  // Implementation of make_symbol for each symbol type.
  parser::symbol_type
  parser::make_END_OF_FILE ()
  {
    return symbol_type (token::END_OF_FILE);
  }

  parser::symbol_type
  parser::make_vir ()
  {
    return symbol_type (token::vir);
  }

  parser::symbol_type
  parser::make_virc1 ()
  {
    return symbol_type (token::virc1);
  }

  parser::symbol_type
  parser::make_virc2 ()
  {
    return symbol_type (token::virc2);
  }

  parser::symbol_type
  parser::make_sper (const std::string& v)
  {
    return symbol_type (token::sper, v);
  }

  parser::symbol_type
  parser::make_asig (const std::string& v)
  {
    return symbol_type (token::asig, v);
  }

  parser::symbol_type
  parser::make_lor (const std::string& v)
  {
    return symbol_type (token::lor, v);
  }

  parser::symbol_type
  parser::make_land (const std::string& v)
  {
    return symbol_type (token::land, v);
  }

  parser::symbol_type
  parser::make_bor (const std::string& v)
  {
    return symbol_type (token::bor, v);
  }

  parser::symbol_type
  parser::make_bxor (const std::string& v)
  {
    return symbol_type (token::bxor, v);
  }

  parser::symbol_type
  parser::make_band (const std::string& v)
  {
    return symbol_type (token::band, v);
  }

  parser::symbol_type
  parser::make_equ (const std::string& v)
  {
    return symbol_type (token::equ, v);
  }

  parser::symbol_type
  parser::make_neq (const std::string& v)
  {
    return symbol_type (token::neq, v);
  }

  parser::symbol_type
  parser::make_lst (const std::string& v)
  {
    return symbol_type (token::lst, v);
  }

  parser::symbol_type
  parser::make_bgt (const std::string& v)
  {
    return symbol_type (token::bgt, v);
  }

  parser::symbol_type
  parser::make_let (const std::string& v)
  {
    return symbol_type (token::let, v);
  }

  parser::symbol_type
  parser::make_bet (const std::string& v)
  {
    return symbol_type (token::bet, v);
  }

  parser::symbol_type
  parser::make_shl (const std::string& v)
  {
    return symbol_type (token::shl, v);
  }

  parser::symbol_type
  parser::make_shr (const std::string& v)
  {
    return symbol_type (token::shr, v);
  }

  parser::symbol_type
  parser::make_add (const std::string& v)
  {
    return symbol_type (token::add, v);
  }

  parser::symbol_type
  parser::make_sub (const std::string& v)
  {
    return symbol_type (token::sub, v);
  }

  parser::symbol_type
  parser::make_mul (const std::string& v)
  {
    return symbol_type (token::mul, v);
  }

  parser::symbol_type
  parser::make_div (const std::string& v)
  {
    return symbol_type (token::div, v);
  }

  parser::symbol_type
  parser::make_mod (const std::string& v)
  {
    return symbol_type (token::mod, v);
  }

  parser::symbol_type
  parser::make_den (const std::string& v)
  {
    return symbol_type (token::den, v);
  }

  parser::symbol_type
  parser::make_neg (const std::string& v)
  {
    return symbol_type (token::neg, v);
  }

  parser::symbol_type
  parser::make__new (const std::string& v)
  {
    return symbol_type (token::_new, v);
  }

  parser::symbol_type
  parser::make_inc (const std::string& v)
  {
    return symbol_type (token::inc, v);
  }

  parser::symbol_type
  parser::make_dec (const std::string& v)
  {
    return symbol_type (token::dec, v);
  }

  parser::symbol_type
  parser::make_memb (const std::string& v)
  {
    return symbol_type (token::memb, v);
  }

  parser::symbol_type
  parser::make_lrod (const std::string& v)
  {
    return symbol_type (token::lrod, v);
  }

  parser::symbol_type
  parser::make_lsqu (const std::string& v)
  {
    return symbol_type (token::lsqu, v);
  }

  parser::symbol_type
  parser::make_rrod (const std::string& v)
  {
    return symbol_type (token::rrod, v);
  }

  parser::symbol_type
  parser::make_rsqu (const std::string& v)
  {
    return symbol_type (token::rsqu, v);
  }

  parser::symbol_type
  parser::make_id (const std::string& v)
  {
    return symbol_type (token::id, v);
  }

  parser::symbol_type
  parser::make_inte (const std::string& v)
  {
    return symbol_type (token::inte, v);
  }

  parser::symbol_type
  parser::make_str (const std::string& v)
  {
    return symbol_type (token::str, v);
  }

  parser::symbol_type
  parser::make_semi (const std::string& v)
  {
    return symbol_type (token::semi, v);
  }

  parser::symbol_type
  parser::make_lbra (const std::string& v)
  {
    return symbol_type (token::lbra, v);
  }

  parser::symbol_type
  parser::make_rbra (const std::string& v)
  {
    return symbol_type (token::rbra, v);
  }

  parser::symbol_type
  parser::make_creator (const std::string& v)
  {
    return symbol_type (token::creator, v);
  }

  parser::symbol_type
  parser::make_ctn (const std::string& v)
  {
    return symbol_type (token::ctn, v);
  }

  parser::symbol_type
  parser::make_brk (const std::string& v)
  {
    return symbol_type (token::brk, v);
  }

  parser::symbol_type
  parser::make_ret (const std::string& v)
  {
    return symbol_type (token::ret, v);
  }

  parser::symbol_type
  parser::make_whi (const std::string& v)
  {
    return symbol_type (token::whi, v);
  }

  parser::symbol_type
  parser::make__while (const std::string& v)
  {
    return symbol_type (token::_while, v);
  }

  parser::symbol_type
  parser::make__for (const std::string& v)
  {
    return symbol_type (token::_for, v);
  }

  parser::symbol_type
  parser::make__if (const std::string& v)
  {
    return symbol_type (token::_if, v);
  }

  parser::symbol_type
  parser::make__null (const std::string& v)
  {
    return symbol_type (token::_null, v);
  }

  parser::symbol_type
  parser::make__true (const std::string& v)
  {
    return symbol_type (token::_true, v);
  }

  parser::symbol_type
  parser::make__false (const std::string& v)
  {
    return symbol_type (token::_false, v);
  }

  parser::symbol_type
  parser::make__void (const std::string& v)
  {
    return symbol_type (token::_void, v);
  }

  parser::symbol_type
  parser::make__int (const std::string& v)
  {
    return symbol_type (token::_int, v);
  }

  parser::symbol_type
  parser::make__string (const std::string& v)
  {
    return symbol_type (token::_string, v);
  }

  parser::symbol_type
  parser::make__class (const std::string& v)
  {
    return symbol_type (token::_class, v);
  }

  parser::symbol_type
  parser::make__bool (const std::string& v)
  {
    return symbol_type (token::_bool, v);
  }

  parser::symbol_type
  parser::make__then (const std::string& v)
  {
    return symbol_type (token::_then, v);
  }

  parser::symbol_type
  parser::make__else (const std::string& v)
  {
    return symbol_type (token::_else, v);
  }



} // yy
#line 1877 "parser.hpp" // lalr1.cc:377


// //                    "%code provides" blocks.
#line 11 "parser.yy" // lalr1.cc:377

    #include"wrap.h"

#line 1885 "parser.hpp" // lalr1.cc:377


#endif // !YY_YY_PARSER_HPP_INCLUDED
