#ifndef _ast_h
#define _ast_h

#include "RegTab.hpp"

#include<cstdlib>
#include<list>
#include<initializer_list>
#include<string>
#include<cstring>
#include"enum.h"
#include"type.h"
#include"scope.h"
#include"ir.h"

using namespace std;

extern RegTab ast;
extern scope *arr_sub;
extern void frontend_init();

/*
Public:
    ast_node(): Construct function.
    size_t size(): Get the number of children.
    void appendChild(initializer_list<ast_node*> li): Append a list of children to the end of this node.
    ast_node* operator [](size_t num): Get the num'th child of this node.(0 base)
    void merge(ast_node *p): Move all the children of p to the end of this node.
Virtual:
    virtual node_type getType(): Get the type of this node.


Abstract class for the ast_node.
It isn't responsible for memory releasing, since RegTab would take over this job.
*/
class ast_node: public BaseClass
{
    friend void frontend_init();
    friend void semantic();

  protected:
    static type_struct re_type;
    static type_struct building_class;
    static int loop_cnt;
    static bool cons_valid;
    static bool in_paralist;
    static bool in_class;
    static int in_func;
    static int var_cnt;
    static string exit_block;
    static vir_reg* this_reg;
    static list<string> entry_label;
    static list<string> exit_label;
    static list<string> set0;
    static list<string> set1;

  protected:
    list<ast_node*> child;
    list<ast_node*>::iterator it;
    size_t id;
    bool valid;

  public:
    scope *sp;

  public:
    ast_node();
    virtual ~ast_node();
    virtual node_type getType() = 0;
    virtual void build();
    virtual void type_check();
    virtual void init_string(prog_with_ir&);
    virtual void build_ir(prog_with_ir&, ir*);

  public:
    size_t size();
    void appendChild(initializer_list<ast_node*> li);
    void merge(ast_node *p);
    ast_node* operator [](size_t num);
};

//Specific ast_node.

class node_PROGRAM: public ast_node
{
  public:
    virtual ~node_PROGRAM();
    virtual node_type getType();
};

class node_FUNCDEF: public ast_node
{
  private:
    type_struct type_info;
    string id;
  public:
    virtual ~node_FUNCDEF();
    virtual node_type getType();
    virtual void build();
    virtual void type_check();
    virtual void build_ir(prog_with_ir&, ir*);
    void init(const string&, const string&, const string&, ast_node*, ast_node*);
};

class node_VARDEF: public ast_node
{
    friend class node_FUNCDEF;
  private:
    type_struct type_info;
    string id;
  public:
    virtual ~node_VARDEF();
    virtual node_type getType();
    virtual void build();
    virtual void type_check();
    virtual void build_ir(prog_with_ir&, ir*);
    void init(const string&, const string&, const string&, ast_node*);
};

class node_CLASSDEF: public ast_node
{
  private:
    string id;
  public:
    virtual ~node_CLASSDEF();
    virtual node_type getType();
    virtual void build();
    virtual void build_ir(prog_with_ir&, ir*);
    void init(string& , ast_node*);
};

class node_PARADEF: public ast_node
{
  public:
    virtual ~node_PARADEF();
    virtual node_type getType();
    virtual void build_ir(prog_with_ir&, ir*);
};

class node_STATEMENT: public ast_node
{
  friend class node_IF;
  private:
    bool new_scope = false;
  public:
    virtual ~node_STATEMENT();
    virtual node_type getType();
    virtual void build();
    void init(bool, ast_node*);
};

class node_EXPR: public ast_node
{
  private:
    type_struct &get_Typeinfo(int);
    bool check_valid(string, string);

  public:
    type_struct type_info;
    void *val;
    op_type op_info;
    string reg_name;
    bool is_pointer;
    virtual ~node_EXPR();
    virtual node_type getType();
    virtual void build();
    virtual void type_check();
    virtual void init_string(prog_with_ir&);
    virtual void build_ir(prog_with_ir&, ir*);
    void init(op_type, ast_node*, ast_node*, const string&);
    vir_reg* load_reg(prog_with_ir&, ir* func, bool = false);
    void stroe_reg(prog_with_ir&, ir*, vir_reg*);
};

class node_CLASSPARADEF: public ast_node
{
  public:
    virtual ~node_CLASSPARADEF();
    virtual node_type getType();
    void init(ast_node*, ast_node* = NULL);
};

class node_PARADEFLIST: public ast_node
{
  public:
    virtual ~node_PARADEFLIST();
    virtual node_type getType();
    void init(ast_node* = NULL, ast_node* = NULL);
};

class node_IF: public ast_node
{
  public:
    virtual ~node_IF();
    virtual node_type getType();
    virtual void type_check();
    virtual void build();
    virtual void build_ir(prog_with_ir&, ir*);
};

class node_WHILE: public ast_node
{
  public:
    virtual ~node_WHILE();
    virtual node_type getType();
    virtual void type_check();
    virtual void build_ir(prog_with_ir&, ir*);
};

class node_FOR: public ast_node
{
  public:
    virtual ~node_FOR();
    virtual node_type getType();
    virtual void build();
    virtual void type_check();
    virtual void build_ir(prog_with_ir&, ir*);
};

class node_RETURN: public ast_node
{
  public:
    virtual ~node_RETURN();
    virtual node_type getType();
    virtual void type_check();
    virtual void build_ir(prog_with_ir&, ir*);
};

class node_CONTINUE: public ast_node
{
  public:
    virtual ~node_CONTINUE();
    virtual node_type getType();
    virtual void type_check();
    virtual void build_ir(prog_with_ir&, ir*);
};

class node_BREAK: public ast_node
{
  public:
    virtual ~node_BREAK();
    virtual node_type getType();
    virtual void type_check();
    virtual void build_ir(prog_with_ir&, ir*);
};

class node_PARALIST: public ast_node
{
  public:
    type_struct &get_Typeinfo(int);
    virtual ~node_PARALIST();
    virtual node_type getType();
};

class node_NEW: public ast_node
{
  public:
    type_struct type_info;
    string reg_name;
    bool cons = false;
    virtual ~node_NEW();
    virtual node_type getType();
    virtual void type_check();
    virtual void build_ir(prog_with_ir&, ir*);
    void init(const string&, const string&, ast_node*, ast_node* = NULL);

  private:
    int len;
    int t_siz;
    vir_reg *malloc_loop(prog_with_ir &, ir *, int);
};

class node_CREATOR: public ast_node
{
  public:
    virtual ~node_CREATOR();
    virtual void type_check();
    virtual node_type getType();
};

#endif