#ifndef _type_h
#define _type_h

#include "enum.h"

#include<string>
#include<cstdlib>

using namespace std;

class type_struct
{
  private:
    string class_name;
    enum node_type type_info;
    size_t level;
    bool has_name;
    string reg_name;
  public:
    bool is_const;
    bool is_global;
    int no_in_class = 1;
    int is_memb_func;
    type_struct();
    void init(const string &, const string &, bool, int = 0);
    void init_func(const string &, const string &, bool = true, int = 0);
    bool operator ==(const type_struct &ob);
    void dec_level(int = 1);
    int get_level();
    bool is_class();
    bool is_func();
    bool is_ref();
    bool is_set_reg();
    void set_reg(const string);
    string get_reg_name();
    string getStr();
    string getClassname();
};

#endif